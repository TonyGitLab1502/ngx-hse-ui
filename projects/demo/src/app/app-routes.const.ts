import { Routes } from '@angular/router';
import { AppRoutesEnum } from '@shared';

export const appRoutes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: AppRoutesEnum.INTRO },
  {
    path: '',
    children: [
      {
        path: AppRoutesEnum.ALERT,
        loadComponent: () => import('./components/alert/alert.component').then((c) => c.AlertComponent),
      },
      {
        path: AppRoutesEnum.BUTTON,
        loadComponent: () => import('./components/button/button.component').then((c) => c.ButtonComponent),
      },
      {
        path: AppRoutesEnum.CHECKBOX,
        loadComponent: () => import('./components/checkbox/checkbox.component').then((c) => c.CheckboxComponent),
      },
      {
        path: AppRoutesEnum.COLLAPSE,
        loadComponent: () => import('./components/collapse/collapse.component').then((c) => c.CollapseComponent)
      },
      {
        path: AppRoutesEnum.DATEPICKER,
        loadComponent: () =>
          import('./components/date-picker/date-picker.component').then((c) => c.DatePickerComponent),
      },
      {
        path: AppRoutesEnum.FILE_INPUT,
        loadComponent: () =>
          import('./components/file-input/file-input.component').then((c) => c.FileInputComponent)
      },
      {
        path: AppRoutesEnum.HEADING,
        loadComponent: () => import('./heading/heading.component').then((c) => c.HeadingComponent),
      },
      {
        path: AppRoutesEnum.ICON,
        loadComponent: () => import('./components/icon/icon.component').then((c) => c.IconComponent),
      },
      {
        path: AppRoutesEnum.INPUT,
        loadComponent: () => import('./components/input/input.component').then((c) => c.InputComponent),
      },
      {
        path: AppRoutesEnum.INTRO,
        loadComponent: () => import('./intro/intro.component').then((c) => c.IntroComponent),
      },
      {
        path: AppRoutesEnum.LOADER,
        loadComponent: () => import('./components/loader/loader.component').then((c) => c.LoaderComponent)
      },
      {
        path: AppRoutesEnum.PAGINATION,
        loadComponent: () => import('./components/pagination/pagination.component').then((c) => c.PaginationComponent),
      },
      {
        path: AppRoutesEnum.PALETTE,
        loadComponent: () => import('./palette/palette.component').then((c) => c.PaletteComponent),
      },
      {
        path: AppRoutesEnum.RADIO_GROUP,
        loadComponent: () =>
          import('./components/radio-group/radio-group.component').then((c) => c.RadioGroupComponent),
      },
      {
        path: AppRoutesEnum.SELECT,
        loadComponent: () => import('./components/select/select.component').then((c) => c.SelectComponent),
      },
      {
        path: AppRoutesEnum.TABS,
        loadComponent: () => import('./components/tabs/tabs.component').then((c) => c.TabsComponent),
      },
      {
        path: AppRoutesEnum.TAGS,
        loadComponent: () => import('./components/tags/tags.component').then((c) => c.TagsComponent),
      },
      {
        path: AppRoutesEnum.TEXTAREA,
        loadComponent: () => import('./components/textarea/textarea.component').then((c) => c.TextareaComponent),
      },
      {
        path: AppRoutesEnum.TOAST,
        loadComponent: () => import('./components/toast/toast.component').then((c) => c.ToastComponent),
      },
      {
        path: AppRoutesEnum.TABLE,
        loadComponent: () =>
          import('./components/table/table.component').then((c) => c.TableComponent)
      }
    ],
  },
  { path: '**', redirectTo: AppRoutesEnum.INTRO },
];
