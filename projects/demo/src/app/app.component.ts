import { ChangeDetectionStrategy, Component, HostBinding } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HseAlertModule, HseLoaderModule, HseToastModule } from '@ngx-hse-ui';
import { LayoutComponent } from '@standaloneComponents/layout/layout.component';

@Component({
  selector: 'app-root',
  template: `
    <hse-loader></hse-loader>
    <hse-alerts></hse-alerts>
    <app-layout>
      <router-outlet></router-outlet>
    </app-layout>
    <hse-toast></hse-toast>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule, HseAlertModule, HseLoaderModule, HseToastModule, LayoutComponent, RouterModule],
})
export class AppComponent {
  @HostBinding('class.root') private readonly baseCss: boolean = true;
}
