import { ChangeDetectionStrategy, Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HseAlertService, HseAlertVariants, HseButtonModule } from '@ngx-hse-ui';
import { ComponentPreviewComponent, PageContentComponent, PageHeaderComponent } from '@shared/standalone-components';

@Component({
  selector: 'app-alert',
  standalone: true,
  imports: [
    CommonModule,
    ComponentPreviewComponent,
    HseButtonModule,
    PageContentComponent,
    PageHeaderComponent
  ],
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AlertComponent implements OnInit {
  @ViewChild('descriptionTmpl', {static: true})
  public descriptionTmpl!: TemplateRef<void>;
  
  public HseAlertVariants: typeof HseAlertVariants = HseAlertVariants;
  
  constructor(private readonly _alertService: HseAlertService) { }

  ngOnInit(): void {
  }
  
  showAlert(variant: HseAlertVariants = HseAlertVariants.INFO): void {
    this._alertService.addAlert({
      title: `Hello, I'm hse-alert component`,
      variant
    });
  }
  
  showAlertWithDescription(isTemplateDescription: boolean = false): void {
    this._alertService.addAlert({
      title: `Hello, I'm alert`,
      description: isTemplateDescription
        ? this.descriptionTmpl
        : `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium alias eius ipsa magnam nihil quae
           rerum, unde velit! Amet dolorem eaque, et facere modi, molestiae nihil obcaecati odit provident quam
           recusandae reiciendis sint sit temporibus vero? Facilis inventore ipsum nihil quas sit suscipit voluptas!
           Accusamus ad asperiores, aspernatur beatae commodi culpa cupiditate delectus dicta distinctio dolore, esse
           excepturi facilis inventore ipsa ipsam ipsum minima, necessitatibus neque obcaecati odit omnis possimus
           praesentium qui quisquam saepe ullam unde vitae. Asperiores beatae blanditiis delectus eaque fugiat fugit
           magnam minus, voluptate. Accusamus amet ea eius excepturi nihil officia perferendis praesentium quas. Ad
           alias mollitia porro quas quisquam! Ad aspernatur consectetur delectus id maxime mollitia quidem. Aperiam
           assumenda beatae, culpa debitis dolorem doloremque error ipsam molestias natus neque nesciunt, nulla, quam
           soluta voluptas!
         `,
    })
  }
  
  showAlertWithAnotherIcon(): void {
    this._alertService.addAlert({
      title: `Hello, I'm hse-alert component`,
      icon: 'check'
    })
  }
  
  showAlertWithActions(): void {
    this._alertService.addAlert({
      title: 'Alert with actions',
      description: 'We use essential cookies to make our site work. With your consent, we may also use non-essential cookies to improve user experience and analyze website traffic. By clicking “Accept,“ you agree to our website\'s cookie use as described in our Cookie Policy. You can change your cookie settings at any time by clicking “Preferences.”',
      actions: [
        { name: `Yes, I'm agree`, callback: this.onAgree.bind(this) },
        { name: `Cancel`, callback: this.onCancel.bind(this) },
      ]
    })
  }
  
  onAgree(): void {
    this._alertService.addAlert({
      title: `Yes, I'm agree`,
      variant: HseAlertVariants.SUCCESS
    });
  }
  
  onCancel(): void {
    this._alertService.addAlert({
      title: 'Cancel',
      variant: HseAlertVariants.ERROR
    });
  }
}
