import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HseButtonModule, HseButtonGroup, HseButtonGroupItem, HseUiSizes, HseUiVariants } from '@ngx-hse-ui';
import { ComponentPreviewComponent, PageContentComponent, PageHeaderComponent } from '@shared/standalone-components';

@Component({
  selector: 'app-button',
  standalone: true,
  imports: [CommonModule, ComponentPreviewComponent, HseButtonModule, PageContentComponent, PageHeaderComponent],
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ButtonComponent implements OnInit {
  sizes: HseUiSizes[] = [HseUiSizes.LARGE, HseUiSizes.MEDIUM, HseUiSizes.SMALL];
  variants: HseUiVariants[] = [HseUiVariants.PRIMARY, HseUiVariants.SECONDARY, HseUiVariants.TERTIARY];

  buttons: HseButtonGroup = [
    { label: 'Button #1', active: false, disabled: false, onClick: this.groupCallback1.bind(this) },
    { label: 'Button #2', active: false, disabled: true, onClick: this.groupCallback2.bind(this) },
    { label: 'Button #3', active: true, disabled: false, onClick: this.groupCallback3.bind(this) },
    { label: 'Button #4', active: true, disabled: true, onClick: this.groupCallback4.bind(this) },
  ];

  disabledButtons: HseButtonGroup = [
    { label: 'Button #1', active: false, disabled: true, onClick: this.groupCallback1.bind(this) },
    { label: 'Button #2', active: true, disabled: true, onClick: this.groupCallback2.bind(this) },
  ];

  constructor() {}

  ngOnInit(): void {}

  onButtonClick(): void {
    alert(`Hello, I'm hse-button component`);
  }

  groupCallback1(value: HseButtonGroupItem): void {
    console.log(value);
  }

  groupCallback2(value: HseButtonGroupItem): void {
    console.log(value);
  }

  groupCallback3(value: HseButtonGroupItem): void {
    console.log(value);
  }

  groupCallback4(value: HseButtonGroupItem): void {
    console.log(value);
  }
}
