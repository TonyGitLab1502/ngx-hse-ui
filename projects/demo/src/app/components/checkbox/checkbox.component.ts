import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HseCheckboxModule, HseUiSizes, HseUiVariants } from '@ngx-hse-ui';
import { ComponentPreviewComponent, PageContentComponent, PageHeaderComponent } from '@shared/standalone-components';

@Component({
  selector: 'app-checkbox',
  standalone: true,
  imports: [CommonModule, HseCheckboxModule, ComponentPreviewComponent, PageContentComponent, PageHeaderComponent],
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CheckboxComponent implements OnInit {
  sizes: HseUiSizes[] = [HseUiSizes.LARGE, HseUiSizes.MEDIUM, HseUiSizes.SMALL];
  variants: HseUiVariants[] = [HseUiVariants.PRIMARY, HseUiVariants.SECONDARY, HseUiVariants.TERTIARY];

  model1: boolean = false;
  model2: boolean = false;
  model3: boolean = true;
  model4: boolean = true;
  hasError: boolean = false;

  constructor() {}

  ngOnInit(): void {}
}
