import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HseCollapseModule, HseCollapsePanel } from '@ngx-hse-ui';
import { ComponentPreviewComponent, PageContentComponent, PageHeaderComponent } from '@shared/standalone-components';

@Component({
  selector: 'app-collapse',
  standalone: true,
  imports: [CommonModule, ComponentPreviewComponent, HseCollapseModule, PageContentComponent, PageHeaderComponent],
  templateUrl: './collapse.component.html',
  styleUrls: ['./collapse.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CollapseComponent implements OnInit {
  
  panels1: HseCollapsePanel[] = [
    {
      active: false,
      content: `
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi aut distinctio doloremque,
        dolores eius ex incidunt iusto maiores, minima minus, natus nostrum nulla officiis pariatur porro quae qui
        quia quidem quo rem sequi temporibus unde ut voluptates? Consequatur cum eaque error laboriosam molestiae
        neque possimus provident repellat similique, voluptas?
      `,
      label: 'Hse-collapse panel #1'
    },
    {
      active: true,
      content: `
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi aut distinctio doloremque,
        dolores eius ex incidunt iusto maiores, minima minus, natus nostrum nulla officiis pariatur porro quae qui
        quia quidem quo rem sequi temporibus unde ut voluptates? Consequatur cum eaque error laboriosam molestiae
        neque possimus provident repellat similique, voluptas?
      `,
      label: 'Hse-collapse panel #2'
    },
    {
      active: false,
      content: `
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi aut distinctio doloremque,
        dolores eius ex incidunt iusto maiores, minima minus, natus nostrum nulla officiis pariatur porro quae qui
        quia quidem quo rem sequi temporibus unde ut voluptates? Consequatur cum eaque error laboriosam molestiae
        neque possimus provident repellat similique, voluptas?
      `,
      label: 'Hse-collapse panel #3'
    },
    {
      active: false,
      content: `
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi aut distinctio doloremque,
        dolores eius ex incidunt iusto maiores, minima minus, natus nostrum nulla officiis pariatur porro quae qui
        quia quidem quo rem sequi temporibus unde ut voluptates? Consequatur cum eaque error laboriosam molestiae
        neque possimus provident repellat similique, voluptas?
      `,
      label: 'Hse-collapse panel #4'
    },
    {
      active: false,
      content: `
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi aut distinctio doloremque,
        dolores eius ex incidunt iusto maiores, minima minus, natus nostrum nulla officiis pariatur porro quae qui
        quia quidem quo rem sequi temporibus unde ut voluptates? Consequatur cum eaque error laboriosam molestiae
        neque possimus provident repellat similique, voluptas?
      `,
      label: 'Hse-collapse panel #5'
    }
  ];
  
  constructor() { }

  ngOnInit(): void {
  }

}
