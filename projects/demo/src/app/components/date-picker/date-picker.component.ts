import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import {
  HseButtonModule,
  HseButtonTypes,
  HseCheckboxModule,
  HseDateModel,
  HseDatePickerModule,
  HseDateSelectionModes,
  HseDirectionTypes,
  HseUiSizes,
} from '@ngx-hse-ui';
import { ComponentPreviewComponent, PageContentComponent, PageHeaderComponent } from '@shared/standalone-components';
import { DateTime } from 'luxon';

@Component({
  selector: 'app-datepicker',
  standalone: true,
  imports: [
    CommonModule,
    ComponentPreviewComponent,
    HseButtonModule,
    HseCheckboxModule,
    HseDatePickerModule,
    PageContentComponent,
    PageHeaderComponent,
    ReactiveFormsModule,
  ],
  templateUrl: './date-picker.component.html',
  styleUrls: ['./date-picker.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DatePickerComponent implements OnInit {
  get small(): HseUiSizes.SMALL {
    return HseUiSizes.SMALL;
  }

  get large(): HseUiSizes.LARGE {
    return HseUiSizes.LARGE;
  }

  get label(): string {
    return 'Label';
  }

  get rowDirection(): HseDirectionTypes.ROW {
    return HseDirectionTypes.ROW;
  }

  get columnDirection(): HseDirectionTypes.COLUMN {
    return HseDirectionTypes.COLUMN;
  }

  get datePlaceholder(): string {
    return 'dd.mm.yyyy';
  }

  get timePlaceholder(): string {
    return '00:00';
  }

  isRow: boolean = false;

  formGroup: FormGroup;
  type: HseButtonTypes = HseButtonTypes.SUBMIT;

  dateStart: DateTime = DateTime.now().minus({ day: 2 });
  dateEnd: DateTime = DateTime.now().plus({ day: 2 });
  timeRangeHasError: boolean = false;
  selectionMode: HseDateSelectionModes = HseDateSelectionModes.RANGE;

  timeRangeModel: HseDateModel | null = {
    dateStart: DateTime.fromObject({ day: 28, month: 6, hour: 10 }),
    dateEnd: DateTime.fromObject({ day: 28, month: 6, hour: 17, minute: 30 }),
  };

  datePickerModel: HseDateModel = {
    dateStart: DateTime.now(),
    dateEnd: null,
  };

  dateTimeModel: HseDateModel | null = {
    dateStart: DateTime.now(),
    dateEnd: null,
  };

  constructor(private readonly _formBuilder: FormBuilder) {
    this.formGroup = this._formBuilder.group({
      date: [null, Validators.required],
      time: [{ dateStart: DateTime.now().startOf('hour'), dateEnd: null }, Validators.required],
      dateRange: [null, Validators.required],
      timeRange: [null, Validators.required],
      dateTime: [{ dateStart: DateTime.now().startOf('hour').minus({ hour: 1 }), dateEnd: null }, Validators.required],
    });
  }

  ngOnInit(): void {}

  changeDate(date: HseDateModel | null) {
    console.log(date);
  }

  changeTime(time: any) {
    console.log(time?.timeStart?.toFormat('T'));
  }

  changeTimeRange(value: HseDateModel | null) {
    this.timeRangeModel = value;
    this.timeRangeHasError =
      (this.timeRangeModel?.dateStart as DateTime).hour < 9 || (this.timeRangeModel?.dateEnd as DateTime).hour > 18;
    console.log(this.timeRangeModel);
  }

  onSubmit() {
    console.log(this.formGroup);
  }

  changeDateTime(value: HseDateModel | null) {
    this.dateTimeModel = value;
    console.log(this.dateTimeModel);
  }
}
