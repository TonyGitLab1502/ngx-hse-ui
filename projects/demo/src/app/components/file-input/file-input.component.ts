import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComponentPreviewComponent, PageContentComponent, PageHeaderComponent } from '@shared/standalone-components';
import {FileInputItem, HseFileInputModule, HseUiSizes } from '@ngx-hse-ui';

@Component({
  selector: 'app-file-input',
  templateUrl: './file-input.component.html',
  styleUrls: ['./file-input.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [CommonModule, ComponentPreviewComponent, PageContentComponent, PageHeaderComponent, HseFileInputModule],
  standalone: true,
})
export class FileInputComponent implements OnInit {
  HseUiSizes = HseUiSizes;

  apiExample = `
    @Input() accept = '*'; - допустимые форматы файлов
    @Input() maxFilesCount = 1; - допустимое кол-во файлов
    @Input() disabled: boolean = false; - флаг блокировки
    @Input() hasError: boolean = false; - флаг наличия обшибки
    @Input() size: HseUiSizes = HseUiSizes.MEDIUM; - размер компонента
    @Input() hint: string = ''; - текстовое сообщение
    @Input() model: FileInputItem[] - выбранные файлы
  `;

  modelDescription = `
   interface FileInputItem {
      file: File, - файл
      size: string, - размер файла
      type: string, - формат файла
      name: string, - название файла
      url?: string, - ссылка на скачивание файла
      uploadProgress: number, - прогресс загрузки файла
      uploaded?: boolean, - флаг "файл загружен"
      uploading?: boolean, - флаг "файл загружается"
    }
  `;

  simpleExample = `
   <\hse-file-input [accept]="'.pdf'" [hint]="'Допустимый формат: .pdf'" (modelChange)="changeFile($event)"></hse-file-input>
  `;

  multipleExample = `
  <\hse-file-input [maxFilesCount]="10" (modelChange)="changeFile($event)"></hse-file-input>
  `;

  progressExample = `
  testFiles: FileInputItem[] = [
    {
      id: null,
      uploaded: false,
      uploadProgress: 30,
      uploading: true,
      name: 'test',
      size: '10KB',
      type: '.pdf'
    },
    {
      id: null,
      uploadProgress: 100,
      uploading: false,
      uploaded: true,
      name: 'test',
      size: '10KB',
      type: '.pdf'
    }
  ]

  ...

  <\hse-file-input [(model)]="testFiles" [maxFilesCount]="10" (modelChange)="changeFile($event)"></hse-file-input>
  `;

  testFiles: FileInputItem[] = [
    {
      id: null,
      uploaded: false,
      uploadProgress: 30,
      uploading: true,
      name: 'test',
      size: '10KB',
      type: '.pdf'
    },
    {
      id: null,
      uploadProgress: 100,
      uploading: false,
      uploaded: true,
      name: 'test',
      size: '10KB',
      type: '.pdf'
    }
  ];


  progressExample2 = `
  changeFile($event: FileInputItem[]) {
    $event.forEach((file)=> {
      file.uploading = true;

      let interval = setInterval(()=> {
        file.uploadProgress += 10;

        if(file.uploadProgress === 100) {
          file.uploading = false;
          file.uploaded = true;
          file.url = ''
          clearInterval(interval);
        }

        this.cdr.detectChanges();
      }, 100);
    })
  }
  `;

  constructor(private cdr: ChangeDetectorRef) {

  }

  ngOnInit(): void {}

  changeFile($event: FileInputItem[]) {
    $event.forEach((file)=> {
      file.uploading = true;

      let interval = setInterval(()=> {
        // @ts-ignore
        file.uploadProgress += 10;

        if(file.uploadProgress === 100) {
          file.uploading = false;
          file.uploaded = true;
          file.url = ''
          clearInterval(interval);
        }

        this.cdr.detectChanges();
      }, 100);
    })
  }
}
