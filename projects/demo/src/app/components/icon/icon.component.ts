import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HseIconModule, HseIconService, HseUiSizes } from '@ngx-hse-ui';
import { ComponentPreviewComponent, PageContentComponent, PageHeaderComponent } from '@shared/standalone-components';

@Component({
  selector: 'app-icon',
  templateUrl: './icon.component.html',
  styleUrls: ['./icon.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [CommonModule, ComponentPreviewComponent, HseIconModule, PageContentComponent, PageHeaderComponent],
  standalone: true,
})
export class IconComponent implements OnInit {
  public readonly icons: string[] = [];

  sizeSm: HseUiSizes = HseUiSizes.SMALL;
  sizeMd: HseUiSizes = HseUiSizes.MEDIUM;
  sizeLg: HseUiSizes = HseUiSizes.LARGE;

  iconSizes: any = [
    {
      name: HseUiSizes.LARGE,
      icons: Array.from(this._iconService.iconStore.keys()).slice(0, 10),
    },
    {
      name: HseUiSizes.MEDIUM,
      icons: Array.from(this._iconService.iconStore.keys()).slice(0, 10),
    },
    {
      name: HseUiSizes.SMALL,
      icons: Array.from(this._iconService.iconStore.keys()).slice(0, 10),
    },
  ];

  constructor(private readonly _iconService: HseIconService) {
    this.icons = Array.from(this._iconService.iconStore.keys());
  }

  ngOnInit(): void {}
}
