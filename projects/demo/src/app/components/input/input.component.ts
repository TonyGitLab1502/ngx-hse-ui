import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import {
  HseButtonModule,
  HseButtonTypes,
  HseCheckboxModule,
  HseInputModule,
  HseUiSizes,
  HseUiVariants,
} from '@ngx-hse-ui';
import { ComponentPreviewComponent, PageContentComponent, PageHeaderComponent } from '@shared/standalone-components';

@Component({
  selector: 'app-input',
  standalone: true,
  imports: [
    CommonModule,
    ComponentPreviewComponent,
    FormsModule,
    HseButtonModule,
    HseCheckboxModule,
    HseInputModule,
    PageContentComponent,
    PageHeaderComponent,
    ReactiveFormsModule,
  ],
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InputComponent implements OnInit {
  sizes: HseUiSizes[] = [HseUiSizes.LARGE, HseUiSizes.MEDIUM, HseUiSizes.SMALL];
  variants: HseUiVariants[] = [HseUiVariants.PRIMARY, HseUiVariants.SECONDARY, HseUiVariants.TERTIARY];

  model1: string = '';
  model2: number | null = null;
  model3: string = '';

  maskModel1: string = '';
  maskModel2: string = '';

  placeholder1: string = 'Text input placeholder...';
  placeholder2: string = 'Number input placeholder...';
  placeholder3: string = 'Password input placeholder...';

  hasError: boolean = false;

  type: HseButtonTypes = HseButtonTypes.SUBMIT;

  formGroup: FormGroup;

  constructor(private readonly _formBuilder: FormBuilder) {
    this.formGroup = this._formBuilder.group({
      firstName: ['', Validators.required],
      lastName: [''],
      age: [null, Validators.required],
      password: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    console.log(this.formGroup);
  }

  onChangeModel(value: string, input: any) {
    input.model = value;
    console.log(input);
  }

  onSubmit(): void {
    alert({ ...this.formGroup });
    console.log(this.formGroup);
  }
}
