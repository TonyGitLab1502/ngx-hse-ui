import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HseButtonModule, HseLoaderService } from '@ngx-hse-ui';
import { ComponentPreviewComponent, PageContentComponent, PageHeaderComponent } from '@shared/standalone-components';

@Component({
  selector: 'app-loader',
  standalone: true,
  imports: [CommonModule, ComponentPreviewComponent, HseButtonModule, PageContentComponent, PageHeaderComponent],
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoaderComponent implements OnInit {
  private readonly _loaderTimeout: number = 5000;

  constructor(private readonly _loaderService: HseLoaderService) { }

  ngOnInit(): void {
  }
  
  showLoader(): void {
    this._loaderService.show();
  
    setTimeout(() => {
      this._loaderService.hide();
    }, this._loaderTimeout);
  }

}
