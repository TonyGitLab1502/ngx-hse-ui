import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HsePaginationModule } from '@ngx-hse-ui';
import { ComponentPreviewComponent, PageContentComponent, PageHeaderComponent } from '@shared/standalone-components';

@Component({
  selector: 'app-pagination',
  standalone: true,
  imports: [CommonModule, ComponentPreviewComponent, HsePaginationModule, PageContentComponent, PageHeaderComponent],
  templateUrl: './pagination.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PaginationComponent implements OnInit {
  currentPage: number = 1;
  totalPages: number = 15;

  constructor() {}

  ngOnInit(): void {}

  changePage(page: number) {
    console.log(page);
  }
}
