import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HseCheckboxModule, HseDirectionTypes, HseRadioGroup, HseRadioGroupModule, HseUiSizes } from '@ngx-hse-ui';
import { ComponentPreviewComponent, PageContentComponent, PageHeaderComponent } from '@shared/standalone-components';

@Component({
  selector: 'app-radio-group',
  standalone: true,
  imports: [
    CommonModule,
    ComponentPreviewComponent,
    HseCheckboxModule,
    HseRadioGroupModule,
    PageContentComponent,
    PageHeaderComponent,
  ],
  templateUrl: './radio-group.component.html',
  styleUrls: ['./radio-group.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RadioGroupComponent implements OnInit {
  sizes: HseUiSizes[] = [HseUiSizes.LARGE, HseUiSizes.MEDIUM, HseUiSizes.SMALL];
  directions: HseDirectionTypes[] = [HseDirectionTypes.COLUMN, HseDirectionTypes.ROW];

  hasError: boolean = false;
  indeterminateModel: string = '';

  radioGroup1: HseRadioGroup<string> = [
    { label: 'Radio #1', value: 'Value 1', disabled: false },
    { label: 'Radio #2', value: 'Value 2', disabled: false },
    { label: 'Radio #3', value: 'Value 3', disabled: false },
  ];

  radioGroup2: HseRadioGroup<string> = [
    { label: 'Radio #1', value: 'Value 1', disabled: true },
    { label: 'Radio #2', value: 'Value 2', disabled: true },
    { label: 'Radio #3', value: 'Value 3', disabled: true },
  ];

  radioModel1: string = 'Value 1';
  radioModel2: string = 'Value 2';

  constructor() {}

  ngOnInit(): void {}

  changeRadio1(value: string): void {
    console.log(this.radioModel1, value);
  }
}
