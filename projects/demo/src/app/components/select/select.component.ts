import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  HseButtonModule,
  HseButtonTypes,
  HseCheckboxModule,
  HseSelectModule,
  HseSelectOption,
  HseUiSizes,
} from '@ngx-hse-ui';
import { ComponentPreviewComponent, PageContentComponent, PageHeaderComponent } from '@shared/standalone-components';

@Component({
  selector: 'app-select',
  standalone: true,
  imports: [
    CommonModule,
    ComponentPreviewComponent,
    FormsModule,
    HseButtonModule,
    HseCheckboxModule,
    HseSelectModule,
    PageContentComponent,
    PageHeaderComponent,
    ReactiveFormsModule,
  ],
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SelectComponent implements OnInit {
  hasError: boolean = false;

  placeholder: string = 'Choose option';
  options: HseSelectOption<number>[] = [
    { label: 'Option 1', value: 1 },
    { label: 'Option 2', value: 2 },
    { label: 'Option 3', value: 3 },
    { label: 'Option 4', value: 4 },
    { label: 'Option 5', value: 5 },
  ];

  optionsWithDisabledState: HseSelectOption<number>[] = [
    { label: 'Option 1', value: 1, disabled: true },
    { label: 'Option 2', value: 2 },
    { label: 'Option 3', value: 3, disabled: true },
    { label: 'Option 4', value: 4, disabled: true },
    { label: 'Option 5', value: 5 },
  ];

  selectModel1: HseSelectOption<number> | null = null;
  selectModel2: HseSelectOption<number> | null =
    this.options.find((option: HseSelectOption<number>) => option.value === 2) || null;
  multipleSelectModel1: Array<HseSelectOption<number>> = [];
  multipleSelectModel2: Array<HseSelectOption<number>> = [...this.options];

  selectSizeVariants: any = [
    {
      name: HseUiSizes.LARGE,
      selects: [{ model: null }, { model: [] }],
    },
    {
      name: HseUiSizes.MEDIUM,
      selects: [{ model: null }, { model: [] }],
    },
    {
      name: HseUiSizes.SMALL,
      selects: [{ model: null }, { model: [] }],
    },
  ];

  filteredOptions: HseSelectOption<number>[] = [];

  formGroup: FormGroup;
  type: HseButtonTypes = HseButtonTypes.SUBMIT;

  constructor(private readonly _formBuilder: FormBuilder) {
    this.formGroup = this._formBuilder.group({
      control1: [null],
      control2: [[]],
    });
  }

  ngOnInit(): void {
    this.filteredOptions = [...this.options];
  }

  onSearchOption(value: string): void {
    console.log(value);

    if (value) {
      const regExp: RegExp = new RegExp(value);

      this.filteredOptions = this.options.filter((option: HseSelectOption<number>) =>
        regExp.test(option.label as string)
      );
    } else {
      this.filteredOptions = [...this.options];
    }
  }

  onSubmit(): void {
    console.log(this.formGroup);
  }
}
