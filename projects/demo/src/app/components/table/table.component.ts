import { ChangeDetectionStrategy, Component, ElementRef, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ComponentPreviewComponent, PageContentComponent, PageHeaderComponent} from "@shared/standalone-components";
import { HseTableModule, HseTableConfig, HseButtonModule } from '@ngx-hse-ui';


enum TestTableColumnId {
  NAME = 'name',
  ID = 'id',
  DESCRIPTION = 'description',
  DESCRIPTION_ID = 'description-id',
  DESCRIPTION_VALUE = 'description-value',
  DESCRIPTION_NUMBER = 'description-number',
  DATE = 'date',
  LIST = 'list',
  SETTINGS = 'settings'
}

enum CompactTableColumnId {
  NAME = 'name',
  ID = 'id',
  DESCRIPTION = 'description',
  DESCRIPTION_2 = 'description_2',
  DATE = 'date',
  SETTINGS = 'settings',
}

@Component({
  selector: 'app-table',
  standalone: true,
  imports: [
    CommonModule,
    PageContentComponent,
    PageHeaderComponent,
    HseTableModule,
    HseButtonModule,
    ComponentPreviewComponent
  ],
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TableComponent implements OnInit {

  @ViewChild('buttonColumnTmpl', { static: true }) buttonColumnTmpl!: TemplateRef<any>;
  @ViewChild('filterTemplate', { static: true }) filterTemplate!: TemplateRef<any>;
  @ViewChild('simpleContainer', { static: true }) simpleContainer!: ElementRef<any>;
  @ViewChild('stickyHeaderContainer', { static: true }) stickyHeaderContainer!: ElementRef<any>;
  @ViewChild('shiftButtonsContainer', { static: true }) shiftButtonsContainer!: ElementRef<any>;
  @ViewChild('fixedColumnContainer', { static: true }) fixedColumnContainer!: ElementRef<any>;
  @ViewChild('nestedColumnsContainer', { static: true }) nestedColumnsContainer!: ElementRef<any>;
  @ViewChild('columnSettingsContainer', { static: true }) columnSettingsContainer!: ElementRef<any>;
  @ViewChild('customTemplateContainer', { static: true }) customTemplateContainer!: ElementRef<any>;
  @ViewChild('bigConfigContainer', { static: true }) bigConfigContainer!: ElementRef<any>;


  simpleConfig!: HseTableConfig<CompactTableColumnId>;

  stickyHeaderConfig!: HseTableConfig<CompactTableColumnId>;
  stickyHeaderExample = `
      const tableConfig: HseTableConfig\< TableColumns \> = {
        name: 'sticky-header-table',
        stickyHeader: {
          scrollableParent: this.stickyHeaderContainer.nativeElement
        },
        ...
      }
  `;

  shiftButtonsConfig!: HseTableConfig<CompactTableColumnId>;

  shiftButtonsExample = `
        const tableConfig: HseTableConfig\< TableColumns \> = {
        shiftButtons: true
        ...
      }
  `

  fixedColumnConfig!: HseTableConfig<CompactTableColumnId>;
  fixedColumnExample = `
      const tableConfig: HseTableConfig\< TableColumns \> = {
        shiftButtons: true,
        columns: [
        {
          id: CompactTableColumnId.NAME,
          title: 'Название',
          fixed: true,
          width: 200,
        },
        ...
      }
  `

  nestedColumnsConfig!: HseTableConfig<CompactTableColumnId>;
  nestedColumnsExample= `
      const tableConfig: HseTableConfig\< TableColumns \> = {
        columns: [
          ...,
          {
            id: CompactTableColumnId.DESCRIPTION,
            title: 'Описание',
            childColumns: [
              {
                id: CompactTableColumnId.ID,
                title: 'Id',
                width: {
                  fixed: true,
                  value: 100
                },
              },
              {
                id: CompactTableColumnId.DESCRIPTION_2,
                title: 'Название',
              },
              {
                id: CompactTableColumnId.DATE,
                title: 'Дата'
              }
            ]
          }
        ]
      }
  `

  columnSettingsConfig!: HseTableConfig<CompactTableColumnId>;
  columnSettingsExample= `
      const tableConfig: HseTableConfig\< TableColumns \> = {
      columns: [
        {
          id: CompactTableColumnId.NAME,
          title: 'Название',
          width: 200,
          settings: {
            filter: {  // настройки фильтра
              type: 'select', // тип фильтра 'combo-box' | 'select' | 'radio' | 'template'
              placeholder: 'Выбор',
              width: 200, // ширина элемента-фильтра
              collection: [ // масив значений
                {value: 1, label: 'value 1'},
                {value: 2, label: 'value 2'}
              ],
              onFilter(column, value: any) { // callback возвращающий выбранное значение
              }
            },
            inputField: { // настройки текстового поля
              type: 'number',  // тип значения 'string' | 'number'
              placeholder: 'Поиск',
              onEnter(value) { // callback введенное значение
              }
            },
            onOrder(column, direction) { // callback для сортировки
            }
          }
        },
        ...
        ]
      }
  `

  customTemplateConfig!: HseTableConfig<CompactTableColumnId>;
  customTemplateExample = `
        const tableConfig: HseTableConfig\< TableColumns \> = {
        columns: [
          ...,
         {
            id: CompactTableColumnId.SETTINGS,
            title: 'Настройки',
            width: [100, '1fr'],
            template: this.buttonColumnTmpl
          },
        ]
      }
  `;




  rowsDataCompact: any[] = TableComponent.duplicateRows([
    {
      [CompactTableColumnId.NAME]: 'Строка таблицы',
      [CompactTableColumnId.ID]: 1,
      [CompactTableColumnId.DESCRIPTION]: 'Описание',
      [CompactTableColumnId.DESCRIPTION_2]: 'Описание 2'
    }
  ], 10);

  rowsDataNested: any[] = TableComponent.duplicateRows([
    {
      [CompactTableColumnId.NAME]: 'Строка таблицы',
      [CompactTableColumnId.DESCRIPTION]: {
        [CompactTableColumnId.ID]: '1234',
        [CompactTableColumnId.DESCRIPTION_2]: 'Описание',
        [CompactTableColumnId.DATE]: '21.12.2023'
      }
    }
  ], 10);

  bigConfig!: HseTableConfig<TestTableColumnId>;
  bigRowsData: any[] = TableComponent.duplicateRows([
      {
        [TestTableColumnId.NAME]: 'Строка таблицы',
        [TestTableColumnId.ID]: 1234,
        [TestTableColumnId.DESCRIPTION]: {
          [TestTableColumnId.DESCRIPTION_ID]: '1.1',
          [TestTableColumnId.DESCRIPTION_VALUE]: 'Описание модуля',
          [TestTableColumnId.DESCRIPTION_NUMBER]: 4,
        },
        [TestTableColumnId.DATE]: '12.01.2022',
        [TestTableColumnId.LIST]: '1 2 3 4',
        [TestTableColumnId.SETTINGS]: null,
      }
  ], 10);


  bigConfigExample = `
      this.bigConfig = {
      name: 'test-table',
      stickyHeader: {
        scrollableParent: this.bigConfigContainer.nativeElement,
      },
      shiftButtons: true,
      columns: [
        {
          id: TestTableColumnId.NAME,
          title: 'Название',
          fixed: true,
          width: 150,
          settings: {
            inputField: {
              type: 'number',
              placeholder: 'Поиск',
              onEnter(value) {
                console.log(value);
              }
            },
            onOrder(column, direction) {
                console.log(column, direction);
            }
          }
        },
        {
          id: TestTableColumnId.ID,
          title: 'Id',
          fixed: true,
          width: 100,
          enableCheck: true,
          onColumnChecked(value) {
            console.log(value)
          },
          settings: {
            filter: {
              type: 'select',
              placeholder: 'Выбор',
              width: 200,
              collection: [
                {value: 1, label: 'value 1'},
                {value: 2, label: 'value 2'}
              ],
              onFilter(column, value: any) {
                console.log(column, value)
              }
            }
          }
        },
        {
          id: TestTableColumnId.DESCRIPTION,
          title: 'Описание',
          width: [300, '2fr'],
          childColumns: [
            {
              id: TestTableColumnId.DESCRIPTION_ID,
              title: 'ID:',
              width: {
                value: 80,
                fixed: true
              },
              settings: {
                filter: {
                  type: 'template',
                  template: this.filterTemplate,
                  placeholder: 'Выбор',
                  width: 200,
                  collection: [
                    {value: 1, label: 'value 1'},
                    {value: 2, label: 'value 2'}
                  ],
                  onFilter(column, value: any) {
                    console.log(column, value)
                  }
                }
              }
            },
            {
              id: TestTableColumnId.DESCRIPTION_VALUE,
              title: 'Значение',
              width: 150,
            },
            {
              id: TestTableColumnId.DESCRIPTION_NUMBER,
              title: '#',
              width: {
                value: 70,
                fixed: true
              }
            }
          ],
        },
        {
          id: TestTableColumnId.DATE,
          title: 'Дата',
          width: 200,
          settings: {
            filter: {
              type: 'radio',
              placeholder: 'Выбор',
              width: 200,
              collection: [
                {value: 1, label: 'value 1'},
                {value: 2, label: 'value 2'}
              ],
              onFilter(column, value: any) {
                console.log(column, value)
              }
            }
          }
        },
        {
          id: TestTableColumnId.LIST,
          title: 'Список элементов',
          width: 200,
        },
        {
          id: TestTableColumnId.SETTINGS,
          title: 'Настройки',
          width: 150,
          template: this.buttonColumnTmpl
        }
      ]
    }
  }
  `


  constructor() { }

  ngOnInit() {

    // simple config
    this.simpleConfig = {
      name: 'compact-table',
      shiftButtons: false,
      columns: [
        {
          id: CompactTableColumnId.NAME,
          title: 'Название',
          width: 200,
        },
        {
          id: CompactTableColumnId.ID,
          title: 'Id',
          width: 100,
        },
        {
          id: CompactTableColumnId.DESCRIPTION,
          title: 'Описание',
          width: 400,
        }
      ]
    }

    // STICKY HEADER DEMO
    this.stickyHeaderConfig = {
      name: 'sticky-header-table',
      stickyHeader: {
        scrollableParent: this.stickyHeaderContainer.nativeElement,
      },
      shiftButtons: false,
      columns: [
        {
          id: CompactTableColumnId.NAME,
          title: 'Название',
          width: 200,
        },
        {
          id: CompactTableColumnId.ID,
          title: 'Id',
          width: 100,
        },
        {
          id: CompactTableColumnId.DESCRIPTION,
          title: 'Описание',
          width: 400,
        }
      ]
    }

    // SHIFT BUTTONS DEMO
    this.shiftButtonsConfig = {
      name: 'sticky-header-table',
      stickyHeader: {
        scrollableParent: this.shiftButtonsContainer.nativeElement,
      },
      shiftButtons: true,
      columns: [
        {
          id: CompactTableColumnId.NAME,
          title: 'Название',
          width: 200,
        },
        {
          id: CompactTableColumnId.ID,
          title: 'Id',
          width: 100,
        },
        {
          id: CompactTableColumnId.DESCRIPTION,
          title: 'Описание',
          width: 400,
        }
      ]
    }

    // FIXED COLOMNS DEMO
    this.fixedColumnConfig = {
      name: 'sticky-header-table',
      stickyHeader: {
        scrollableParent: this.fixedColumnContainer.nativeElement,
      },
      shiftButtons: true,
      columns: [
        {
          id: CompactTableColumnId.NAME,
          title: 'Название',
          fixed: true,
          width: 200,
        },
        {
          id: CompactTableColumnId.ID,
          title: 'Id',
          width: 100,
        },
        {
          id: CompactTableColumnId.DESCRIPTION,
          title: 'Описание',
          width: 400,
        },
        {
          id: CompactTableColumnId.DESCRIPTION_2,
          title: 'Описание 2',
          width: 400,
        }
      ]
    }

    // FIXED COLOMNS DEMO
    this.nestedColumnsConfig = {
      name: 'nested-column-table',
      stickyHeader: {
        scrollableParent: this.nestedColumnsContainer.nativeElement,
      },
      shiftButtons: true,
      columns: [
        {
          id: CompactTableColumnId.NAME,
          title: 'Название',
          width: {
            fixed: true,
            value: 200,
          },
        },
        {
          id: CompactTableColumnId.DESCRIPTION,
          title: 'Описание',
          childColumns: [
            {
              id: CompactTableColumnId.ID,
              title: 'Id',
              width: {
                fixed: true,
                value: 100
              },
            },
            {
              id: CompactTableColumnId.DESCRIPTION_2,
              title: 'Название'
            },
            {
              id: CompactTableColumnId.DATE,
              title: 'Дата'
            }
          ]
        }
      ]
    }


    // COLUMN SETTINGS DEMO
    this.columnSettingsConfig = {
      name: 'columns-settings-table',
      stickyHeader: {
        scrollableParent: this.columnSettingsContainer.nativeElement,
      },
      shiftButtons: true,
      columns: [
        {
          id: CompactTableColumnId.NAME,
          title: 'Название',
          width: 200,
          settings: {
            filter: {
              type: 'select',
              placeholder: 'Выбор',
              width: 200,
              collection: [
                {value: 1, label: 'value 1'},
                {value: 2, label: 'value 2'}
              ],
              onFilter(column, value: any) {
              }
            },
            inputField: {
              type: 'number',
              placeholder: 'Поиск',
              onEnter(value) {
              }
            },
            onOrder(column, direction) {
            }
          }
        },
        {
          id: CompactTableColumnId.ID,
          title: 'Id',
          width: 100,
        },
        {
          id: CompactTableColumnId.DESCRIPTION,
          title: 'Описание',
          width: 400,
        }
      ]
    }

    // FIXED COLOMNS DEMO
    this.customTemplateConfig = {
      name: 'sticky-header-table',
      stickyHeader: {
        scrollableParent: this.customTemplateContainer.nativeElement,
      },
      shiftButtons: true,
      columns: [
        {
          id: CompactTableColumnId.NAME,
          title: 'Название',
          width: [200, '1fr'],
        },
        {
          id: CompactTableColumnId.ID,
          title: 'Id',
          width: 100,
        },
        {
          id: CompactTableColumnId.DESCRIPTION,
          title: 'Описание',
          width: [200, '2fr'],
        },
        {
          id: CompactTableColumnId.SETTINGS,
          title: 'Настройки',
          width: [100, '1fr'],
          template: this.buttonColumnTmpl
        }
      ]
    }

    this.bigConfig = {
      name: 'test-table',
      stickyHeader: {
        scrollableParent: this.bigConfigContainer.nativeElement,
      },
      shiftButtons: true,
      columns: [
        {
          id: TestTableColumnId.NAME,
          title: 'Название',
          fixed: true,
          width: 150,
          settings: {
            inputField: {
              type: 'number',
              placeholder: 'Поиск',
              onEnter(value) {
                console.log(value);
              }
            },
            onOrder(column, direction) {
                console.log(column, direction);
            }
          }
        },
        {
          id: TestTableColumnId.ID,
          title: 'Id',
          fixed: true,
          width: 100,
          enableCheck: true,
          onColumnChecked(value) {
            console.log(value)
          },
          settings: {
            filter: {
              type: 'select',
              placeholder: 'Выбор',
              width: 200,
              collection: [
                {value: 1, label: 'value 1'},
                {value: 2, label: 'value 2'}
              ],
              onFilter(column, value: any) {
                console.log(column, value)
              }
            }
          }
        },
        {
          id: TestTableColumnId.DESCRIPTION,
          title: 'Описание',
          width: [300, '2fr'],
          childColumns: [
            {
              id: TestTableColumnId.DESCRIPTION_ID,
              title: 'ID:',
              width: {
                value: 80,
                fixed: true
              },
              settings: {
                filter: {
                  type: 'template',
                  template: this.filterTemplate,
                  placeholder: 'Выбор',
                  width: 200,
                  collection: [
                    {value: 1, label: 'value 1'},
                    {value: 2, label: 'value 2'}
                  ],
                  onFilter(column, value: any) {
                    console.log(column, value)
                  }
                }
              }
            },
            {
              id: TestTableColumnId.DESCRIPTION_VALUE,
              title: 'Значение',
              width: 150,
            },
            {
              id: TestTableColumnId.DESCRIPTION_NUMBER,
              title: '#',
              width: {
                value: 70,
                fixed: true
              }
            }
          ],
        },
        {
          id: TestTableColumnId.DATE,
          title: 'Дата',
          width: 200,
          settings: {
            filter: {
              type: 'radio',
              placeholder: 'Выбор',
              width: 200,
              collection: [
                {value: 1, label: 'value 1'},
                {value: 2, label: 'value 2'}
              ],
              onFilter(column, value: any) {
                console.log(column, value)
              }
            }
          }
        },
        {
          id: TestTableColumnId.LIST,
          title: 'Список элементов',
          width: 200,
        },
        {
          id: TestTableColumnId.SETTINGS,
          title: 'Настройки',
          width: 150,
          template: this.buttonColumnTmpl
        }
      ]
    }
  }

  static duplicateRows(array: any[], count: number) {
    for (let ix= 0; ix <= count; ix++) {
      array.push(array[0])
    }

    return array;
  }
}
