import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  HseRadioGroup,
  HseRadioGroupModule,
  HseTabItem,
  HseTabsModule,
  HseTabsVariants,
  HseUiSizes,
} from '@ngx-hse-ui';
import { ComponentPreviewComponent, PageContentComponent, PageHeaderComponent } from '@shared/standalone-components';

@Component({
  selector: 'app-tabs',
  standalone: true,
  imports: [
    CommonModule,
    ComponentPreviewComponent,
    HseRadioGroupModule,
    HseTabsModule,
    PageContentComponent,
    PageHeaderComponent,
  ],
  templateUrl: './tabs.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TabsComponent implements OnInit {
  get lineFilled(): HseTabsVariants.LINE_FILLED {
    return HseTabsVariants.LINE_FILLED;
  }

  get lineOutlined(): HseTabsVariants.LINE_OUTLINED {
    return HseTabsVariants.LINE_OUTLINED;
  }

  get round(): HseTabsVariants.ROUND {
    return HseTabsVariants.ROUND;
  }

  get square(): HseTabsVariants.SQUARE {
    return HseTabsVariants.SQUARE;
  }

  public radioButtons: HseRadioGroup<HseUiSizes> = [
    { label: 'Small', value: HseUiSizes.SMALL },
    { label: 'Medium', value: HseUiSizes.MEDIUM },
    { label: 'Large', value: HseUiSizes.LARGE },
  ];

  public tabs: HseTabItem[] = [
    { label: 'Tab 1', selected: false, id: 1 },
    { label: 'Tab 2', selected: false, id: 2 },
    { label: 'Tab 3', selected: false, id: 3, counter: 15 },
    { label: 'Tab 4', selected: false, id: 4 },
    { label: 'Tab 5', selected: false, id: 5, counter: 22 },
  ];

  public tabsWithDisabled: HseTabItem[] = [
    { label: 'Tab 1', selected: false, id: 1 },
    { label: 'Tab 2', selected: false, id: 2, disabled: true },
    { label: 'Tab 3', selected: false, id: 3, counter: 15, disabled: true },
    { label: 'Tab 4', selected: false, id: 4 },
    { label: 'Tab 5', selected: false, id: 5, counter: 22 },
  ];

  public tabsWithError: HseTabItem[] = [
    { label: 'Tab 1', selected: false, id: 1 },
    { label: 'Tab 2', selected: false, id: 2 },
    { label: 'Tab 3', selected: false, id: 3, hasError: true },
    { label: 'Tab 4', selected: false, id: 4, hasError: true, disabled: true },
    { label: 'Tab 5', selected: false, id: 5, hasError: true, counter: 22 },
  ];

  public size: HseUiSizes = HseUiSizes.MEDIUM;

  constructor() {}

  ngOnInit(): void {}
}
