import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HseTagsModule, HseTags, HseUiSizes, HseUiBaseVariants, HseUiVariants } from '@ngx-hse-ui';
import { ComponentPreviewComponent, PageContentComponent, PageHeaderComponent } from '@shared/standalone-components';

@Component({
  selector: 'app-tags',
  standalone: true,
  imports: [CommonModule, ComponentPreviewComponent, HseTagsModule, PageContentComponent, PageHeaderComponent],
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TagsComponent implements OnInit {
  sizes: HseUiSizes[] = [HseUiSizes.LARGE, HseUiSizes.MEDIUM, HseUiSizes.SMALL];
  variants: HseUiBaseVariants[] = [HseUiVariants.PRIMARY, HseUiVariants.SECONDARY];

  tags: HseTags = [
    { label: 'Tag #1', disabled: false, leftIcon: '', rightIcon: '' },
    { label: 'Tag #2', disabled: true, leftIcon: '', rightIcon: '' },
    { label: 'Tag #3', disabled: false, leftIcon: 'chevronLeft', rightIcon: '' },
    { label: 'Tag #4', disabled: false, leftIcon: '', rightIcon: 'chevronRight' },
    { label: 'Tag #5', disabled: true, leftIcon: 'chevronLeft', rightIcon: '' },
  ];

  tagsVariants: any = [
    {
      name: HseUiVariants.PRIMARY,
      sizes: [
        {
          name: HseUiSizes.LARGE,
          tags: [
            { label: 'Tag #1', disabled: false, leftIcon: '', rightIcon: '' },
            { label: 'Tag #2', disabled: true, leftIcon: '', rightIcon: '' },
            { label: 'Tag #3', disabled: false, leftIcon: 'chevronLeft', rightIcon: '' },
            { label: 'Tag #4', disabled: false, leftIcon: '', rightIcon: 'chevronRight' },
            { label: 'Tag #5', disabled: true, leftIcon: 'chevronLeft', rightIcon: '' },
          ],
        },
        {
          name: HseUiSizes.MEDIUM,
          tags: [
            { label: 'Tag #1', disabled: false, leftIcon: '', rightIcon: '' },
            { label: 'Tag #2', disabled: true, leftIcon: '', rightIcon: '' },
            { label: 'Tag #3', disabled: false, leftIcon: 'chevronLeft', rightIcon: '' },
            { label: 'Tag #4', disabled: false, leftIcon: '', rightIcon: 'chevronRight' },
            { label: 'Tag #5', disabled: true, leftIcon: 'chevronLeft', rightIcon: '' },
          ],
        },
        {
          name: HseUiSizes.SMALL,
          tags: [
            { label: 'Tag #1', disabled: false, leftIcon: '', rightIcon: '' },
            { label: 'Tag #2', disabled: true, leftIcon: '', rightIcon: '' },
            { label: 'Tag #3', disabled: false, leftIcon: 'chevronLeft', rightIcon: '' },
            { label: 'Tag #4', disabled: false, leftIcon: '', rightIcon: 'chevronRight' },
            { label: 'Tag #5', disabled: true, leftIcon: 'chevronLeft', rightIcon: '' },
          ],
        },
      ],
    },
  ];

  constructor() {}

  ngOnInit(): void {}
}
