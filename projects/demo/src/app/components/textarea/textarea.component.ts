import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HseCheckboxModule, HseTextareaModule } from '@ngx-hse-ui';
import { ComponentPreviewComponent, PageContentComponent, PageHeaderComponent } from '@shared/standalone-components';

@Component({
  selector: 'app-textarea',
  standalone: true,
  imports: [
    CommonModule,
    ComponentPreviewComponent,
    HseCheckboxModule,
    HseTextareaModule,
    PageContentComponent,
    PageHeaderComponent,
  ],
  templateUrl: './textarea.component.html',
  styleUrls: ['./textarea.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TextareaComponent implements OnInit {
  hasError: boolean = false;

  model1: string = '';
  model2: string = `azazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazazaazaza`;
  model3: string = '';

  placeholder: string = 'Start writing text...';

  constructor() {}

  ngOnInit(): void {}
}
