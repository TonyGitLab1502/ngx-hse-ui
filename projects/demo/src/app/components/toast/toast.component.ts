import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HseButtonModule, HseToastService } from '@ngx-hse-ui';
import { ComponentPreviewComponent, PageContentComponent, PageHeaderComponent } from '@shared/standalone-components';

@Component({
  selector: 'app-toast',
  standalone: true,
  imports: [CommonModule, ComponentPreviewComponent, HseButtonModule, PageContentComponent, PageHeaderComponent],
  templateUrl: './toast.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ToastComponent implements OnInit {
  constructor(private readonly _toastService: HseToastService) {}

  ngOnInit(): void {}

  addToast(): void {
    const title = 'Просматривая этот сайт, вы соглашаетесь с нашей политикой конфиденциальности';
    const description = `
      Текст поясняющий заголовок. Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько
      абзацев более менее осмысленного текста рыбы на русском языке, а начинающему оратору отточить навык публичных
      выступлений в домашних условиях.
    `;

    this._toastService.addToast({ title, description, approveCallback: this.toastApprove.bind(this) });
  }

  toastApprove(): void {
    console.log(this);
  }
}
