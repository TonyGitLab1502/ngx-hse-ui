import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageHeaderComponent } from '@standaloneComponents/page-header/page-header.component';
import { PageContentComponent } from '@standaloneComponents/page-content/page-content.component';

@Component({
  selector: 'app-heading',
  standalone: true,
  imports: [CommonModule, PageHeaderComponent, PageContentComponent],
  templateUrl: './heading.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeadingComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
