import { HseIconStore } from '@ngx-hse-ui';

const icons: HseIconStore = new Map([['hseLogo', { url: 'assets/icons/hse-design.svg', data: null }]]);

export default icons;
