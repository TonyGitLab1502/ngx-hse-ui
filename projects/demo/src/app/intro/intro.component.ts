import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, HostBinding, OnInit } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppCard, AppRoutesEnum, componentRoutes } from '@shared';
import { PageHeaderComponent } from '@standaloneComponents/page-header/page-header.component';
import { PageContentComponent } from '@standaloneComponents/page-content/page-content.component';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.component.html',
  styleUrls: ['./intro.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [CommonModule, PageHeaderComponent, PageContentComponent, RouterModule],
  standalone: true,
})
export class IntroComponent implements OnInit {
  @HostBinding('class.intro') private readonly baseCss: boolean = true;

  cards: AppCard[] = [
    { className: 'components', name: 'Компоненты', routePath: componentRoutes[0].path },
    { className: 'heading', name: 'Типографика', routePath: AppRoutesEnum.HEADING },
    { className: 'palette', name: 'Палитра', routePath: AppRoutesEnum.PALETTE },
  ];

  constructor() {}

  ngOnInit(): void {}
}
