import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HseIconModule, HseUiSizes } from '@ngx-hse-ui';
import { PageContentComponent } from '@standaloneComponents/page-content/page-content.component';
import { PageHeaderComponent } from '@standaloneComponents/page-header/page-header.component';
import { AppPaletteGroup } from '@shared';

@Component({
  selector: 'app-palette',
  standalone: true,
  imports: [CommonModule, HseIconModule, PageContentComponent, PageHeaderComponent],
  templateUrl: './palette.component.html',
  styleUrls: ['./palette.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PaletteComponent implements OnInit {
  sizeLg: HseUiSizes = HseUiSizes.LARGE;
  palette: AppPaletteGroup[] = [
    {
      groupName: 'base',
      colors: [
        { name: 'black', value: '#0F0F14' },
        { name: 'white', value: '#FFFFFF' },
      ],
    },
    {
      groupName: 'brand',
      colors: [
        { name: 'dark', value: '#002D6E' },
        { name: 'twilight', value: '#0044B0' },
        { name: 'brand-1', value: '#0050CF' },
        { name: 'brand-2', value: '#3A74D0' },
        { name: 'brand-3', value: '#8EB2EC' },
        { name: 'brand-4', value: '#C2DAFF' },
        { name: 'brand-5', value: '#F0F5FF' },
      ],
    },
    {
      groupName: 'background',
      colors: [
        { name: 'brand', value: '#0060CF' },
        { name: 'white', value: '#FFFFFF' },
        { name: 'gray-1', value: '#EDEFF3' },
        { name: 'gray-2', value: '#F8F9FB' },
      ],
    },
    {
      groupName: 'element-overlay',
      colors: [
        { name: 'dark', value: '#252541' },
        { name: 'light', value: '#FFFFFF' },
      ],
    },
    {
      groupName: 'screen-overlay',
      colors: [
        { name: 'dark', value: '#0F0F1466' },
        { name: 'light', value: '#FFFFFF66' },
      ],
    },
    {
      groupName: 'system',
      colors: [
        { name: 'success', value: '#28C75D' },
        { name: 'success-dark', value: '#228B45' },
        { name: 'success-deepdark', value: '#096237' },
        { name: 'success-light', value: '#E9F9EF' },
        { name: 'error', value: '#FF564E' },
        { name: 'error-dark', value: '#D82E2E' },
        { name: 'error-deepdark', value: '#B9212E' },
        { name: 'error-light', value: '#FFEEED' },
        { name: 'warning', value: '#F8B73F' },
        { name: 'warning-dark', value: '#E9A21D' },
        { name: 'warning-deepdark', value: '#8F5714' },
        { name: 'warning-light', value: '#FEF8EC' },
      ],
    },
    {
      groupName: 'gray',
      colors: [
        { name: 'dusk-1', value: '#6B7A99' },
        { name: 'dusk-2', value: '#7C89A3' },
        { name: 'dusk-3', value: '#A2A9B8' },
        { name: 'dusk-4', value: '#C0C6D1' },
        { name: 'dusk-5', value: '#F0F2F5' },
        { name: 'morn-1', value: '#D3D8E6' },
        { name: 'morn-2', value: '#E1E4EB' },
        { name: 'morn-3', value: '#EDEFF3' },
        { name: 'morn-4', value: '#F5F6FA' },
      ],
    },
    {
      groupName: 'accent',
      colors: [
        { name: 'dodger-deepdark', value: '#0065B0' },
        { name: 'dodger-dark', value: '#008EF6' },
        { name: 'dodger-1', value: '#27A3FF' },
        { name: 'dodger-2', value: '#6CB4F5' },
        { name: 'dodger-3', value: '#90C3F0' },
        { name: 'dodger-4', value: '#C3DDF4' },
        { name: 'dodger-5', value: '#EBF3FA' },
        { name: 'slateblue-deepdark', value: '#581ADD' },
        { name: 'slateblue-dark', value: '#6650EC' },
        { name: 'slateblue-1', value: '#6E73F7' },
        { name: 'slateblue-2', value: '#9497FF' },
        { name: 'slateblue-3', value: '#B2B6FF' },
        { name: 'slateblue-4', value: '#CCCEFF' },
        { name: 'slateblue-5', value: '#F0F0FA' },
        { name: 'purple-deepdark', value: '#3B0B80' },
        { name: 'purple-dark', value: '#6929C4' },
        { name: 'purple-1', value: '#935CE5' },
        { name: 'purple-2', value: '#A37FF5' },
        { name: 'purple-3', value: '#C2A9F9' },
        { name: 'purple-4', value: '#D8C9FB' },
        { name: 'purple-5', value: '#F3F0FA' },
        { name: 'red-deepdark', value: '#A42F15' },
        { name: 'red-dark', value: '#E03A15' },
        { name: 'red-1', value: '#F0522B' },
        { name: 'red-2', value: '#FF6C47' },
        { name: 'red-3', value: '#FF8D70' },
        { name: 'red-4', value: '#FFBEAD' },
        { name: 'red-5', value: '#FFEEEA' },
        { name: 'orange-deepdark', value: '#964002' },
        { name: 'orange-dark', value: '#EB6D00' },
        { name: 'orange-1', value: '#FA8500' },
        { name: 'orange-2', value: '#FF941A' },
        { name: 'orange-3', value: '#FFB366' },
        { name: 'orange-4', value: '#FFCC99' },
        { name: 'orange-5', value: '#FFF3E6' },
        { name: 'green-deepdark', value: '#0E603D' },
        { name: 'green-dark', value: '#00A35F' },
        { name: 'green-1', value: '#00B86E' },
        { name: 'green-2', value: '#49B88B' },
        { name: 'green-3', value: '#86D1B3' },
        { name: 'green-4', value: '#AFE1CD' },
        { name: 'green-5', value: '#EBF5F1' },
      ],
    },
  ];

  constructor() {}

  ngOnInit(): void {}
}
