import { AppRoute } from '@shared';

export enum AppRoutesEnum {
  ALERT = 'components/alert',
  BUTTON = 'components/button',
  CHECKBOX = 'components/checkbox',
  COLLAPSE = 'components/collapse',
  HEADING = 'heading',
  DATEPICKER = 'components/date-picker',
  FILE_INPUT = 'components/file-input',
  ICON = 'components/icon',
  INPUT = 'components/input',
  INTRO = 'intro',
  LOADER = 'components/loader',
  PAGINATION = 'components/pagination',
  PALETTE = 'palette',
  RADIO_GROUP = 'components/radio_group',
  SELECT = 'components/select',
  TABS = 'components/tabs',
  TAGS = 'components/tags',
  TEXTAREA = 'components/textarea',
  TABLE = 'components/table',
  TOAST = 'components/toast',
}

export const sectionRoutes: AppRoute[] = [
  { path: AppRoutesEnum.HEADING, name: 'Heading' },
  { path: AppRoutesEnum.PALETTE, name: 'Palette' },
];

export const componentRoutes: AppRoute[] = [
  { path: AppRoutesEnum.ALERT, name: 'Alert' },
  { path: AppRoutesEnum.BUTTON, name: 'Button' },
  { path: AppRoutesEnum.CHECKBOX, name: 'Checkbox' },
  { path: AppRoutesEnum.COLLAPSE, name: 'Collapse' },
  { path: AppRoutesEnum.DATEPICKER, name: 'Datepicker' },
  { path: AppRoutesEnum.FILE_INPUT, name: 'File input' },
  { path: AppRoutesEnum.ICON, name: 'Icons' },
  { path: AppRoutesEnum.INPUT, name: 'Input' },
  { path: AppRoutesEnum.LOADER, name: 'Loader' },
  { path: AppRoutesEnum.PAGINATION, name: 'Pagination' },
  { path: AppRoutesEnum.RADIO_GROUP, name: 'Radio group' },
  { path: AppRoutesEnum.SELECT, name: 'Select' },
  { path: AppRoutesEnum.TABS, name: 'Tabs' },
  { path: AppRoutesEnum.TAGS, name: 'Tags' },
  { path: AppRoutesEnum.TEXTAREA, name: 'Textarea' },
  { path: AppRoutesEnum.TABLE, name: 'Table'},
  { path: AppRoutesEnum.TOAST, name: 'Toast' },
];
