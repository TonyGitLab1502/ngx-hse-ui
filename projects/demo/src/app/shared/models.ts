import { AppRoutesEnum } from '@shared';

type AppPaletteColor = { name: string; value: string };

export type AppRoute = { path: AppRoutesEnum; name: string };
export type AppCard = { className: string; name: string; routePath: AppRoutesEnum };
export type AppPaletteGroup = { groupName: string; colors: AppPaletteColor[] };
