import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

const declarations: any[] = [];

@NgModule({
  declarations,
  imports: [CommonModule, RouterModule],
  exports: [...declarations],
})
export class SharedModule {}
