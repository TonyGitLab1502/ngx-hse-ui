import { ChangeDetectionStrategy, Component, HostBinding, Input, OnInit, TemplateRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HseCommonUiModule } from '@ngx-hse-ui';

@Component({
  selector: 'app-component-preview',
  standalone: true,
  imports: [CommonModule, HseCommonUiModule],
  template: `
    <ng-container *ngIf="heading">
      <h4 class="hse-heading">{{ heading }}</h4>
    </ng-container>
    <ng-container *ngIf="description">
      <p class="component-preview__description">
        <ng-container hseLabelTemplate [label]="description"></ng-container>
      </p>
    </ng-container>
    <div class="component-preview__preview">
      <ng-content></ng-content>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ComponentPreviewComponent implements OnInit {
  @HostBinding('class.component-preview') private readonly baseCss: boolean = true;

  @Input() description: string | TemplateRef<void> = '';
  @Input() heading: string = '';

  constructor() {}

  ngOnInit(): void {}
}
