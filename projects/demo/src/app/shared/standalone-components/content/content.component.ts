import { ChangeDetectionStrategy, Component, HostBinding, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-content',
  template: ` <ng-content></ng-content> `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule],
})
export class ContentComponent implements OnInit {
  @HostBinding('class.content') private readonly baseCss: boolean = true;

  constructor() {}

  ngOnInit(): void {}
}
