import { ChangeDetectionStrategy, Component, HostBinding, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-footer',
  template: ` <p>footer works!</p> `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule],
})
export class FooterComponent implements OnInit {
  @HostBinding('class.footer') private readonly baseCss: boolean = true;

  constructor() {}

  ngOnInit(): void {}
}
