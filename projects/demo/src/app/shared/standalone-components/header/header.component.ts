import { ChangeDetectionStrategy, Component, HostBinding, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HseIconModule } from '@ngx-hse-ui';
import { AppRoutesEnum } from '@shared';

@Component({
  selector: 'app-header',
  template: ` <a [routerLink]="[introPage]"><hse-icon class="logo" name="hseLogo"></hse-icon></a> `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule, HseIconModule, RouterModule],
})
export class HeaderComponent implements OnInit {
  @HostBinding('class.header') private readonly baseCss: boolean = true;

  public readonly introPage: string = `/${AppRoutesEnum.INTRO}`;
  constructor() {}

  ngOnInit(): void {}
}
