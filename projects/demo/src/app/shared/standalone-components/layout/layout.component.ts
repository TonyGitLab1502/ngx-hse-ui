import { ChangeDetectionStrategy, Component, HostBinding } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContentComponent } from '@standaloneComponents/content/content.component';
import { FooterComponent } from '@standaloneComponents/footer/footer.component';
import { HeaderComponent } from '@standaloneComponents/header/header.component';
import { LeftNavComponent } from '@standaloneComponents/left-nav/left-nav.component';

@Component({
  selector: 'app-layout',
  template: `
    <app-header></app-header>
    <app-left-nav></app-left-nav>
    <app-content>
      <ng-content></ng-content>
    </app-content>
    <app-footer></app-footer>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule, ContentComponent, FooterComponent, HeaderComponent, LeftNavComponent],
})
export class LayoutComponent {
  @HostBinding('class.layout') private readonly baseCss: boolean = true;
}
