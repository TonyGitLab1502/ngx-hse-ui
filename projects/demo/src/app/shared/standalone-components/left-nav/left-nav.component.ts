import { ChangeDetectionStrategy, Component, HostBinding, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HseCollapseModule, HseCollapsePanel } from '@ngx-hse-ui';
import { AppRoute, componentRoutes, sectionRoutes } from '@shared';

@Component({
  selector: 'app-left-nav',
  template: `
    <ul>
      <ng-container *ngFor="let route of sectionRoutes">
        <a class="left-nav__link" [routerLink]="['/' + route.path]" routerLinkActive="left-nav__link_active">
          <li class="left-nav__item">
            {{ route.name }}
          </li>
        </a>
      </ng-container>
    </ul>
    <hse-collapse class="components-menu" [borderless]="true" [hasLeftIndent]="true" [panels]="panels"></hse-collapse>
    <ng-template #componentLinksTmpl>
      <ul>
        <ng-container *ngFor="let route of componentRoutes">
          <a class="left-nav__link" [routerLink]="['/' + route.path]" routerLinkActive="left-nav__link_active">
            <li class="left-nav__component-item">{{ route.name }}</li>
          </a>
        </ng-container>
      </ul>
    </ng-template>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule, HseCollapseModule, RouterModule],
})
export class LeftNavComponent implements OnInit {
  @HostBinding('class.left-nav') private readonly baseCss: boolean = true;
  @ViewChild('componentLinksTmpl', {static: true})
  private componentLinksTmpl!: TemplateRef<any>;
  
  panels: HseCollapsePanel[] = [];
  componentRoutes: AppRoute[] = [];
  sectionRoutes: AppRoute[] = [];

  constructor() {
    this.componentRoutes = componentRoutes;
    this.sectionRoutes = sectionRoutes;
  }

  ngOnInit(): void {
    this.panels = [{ active: true, content: this.componentLinksTmpl, label: 'Components' }];
  }
}
