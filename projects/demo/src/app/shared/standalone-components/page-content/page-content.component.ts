import { ChangeDetectionStrategy, Component, HostBinding, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-page-content',
  template: ` <ng-content></ng-content> `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [CommonModule],
  standalone: true,
})
export class PageContentComponent implements OnInit {
  @HostBinding('class.page-content') private readonly baseCss: boolean = true;

  constructor() {}

  ngOnInit(): void {}
}
