import { ChangeDetectionStrategy, Component, HostBinding, Input, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-page-header',
  template: `
    <h1 class="hse-heading">{{ pageTitle }}</h1>
    <div class="page-header__description-block">
      <ng-content></ng-content>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [CommonModule],
  standalone: true,
})
export class PageHeaderComponent implements OnInit {
  @HostBinding('class.page-header') private readonly baseCss: boolean = true;
  @Input() pageTitle: string = '';

  constructor() {}

  ngOnInit(): void {}
}
