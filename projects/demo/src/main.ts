import { HttpClientModule } from '@angular/common/http';
import { enableProdMode, importProvidersFrom } from '@angular/core';
import { bootstrapApplication } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';

import { HseIconModule } from '@ngx-hse-ui';
import { AppComponent } from './app/app.component';
import { appRoutes } from './app/app-routes.const';
import icons from './app/icons.const';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

bootstrapApplication(AppComponent, {
  providers: [
    importProvidersFrom(
      BrowserAnimationsModule,
      HseIconModule.forRoot(icons),
      HttpClientModule,
      RouterModule.forRoot(appRoutes)
    ),
  ],
}).catch((err) => console.error(err));
