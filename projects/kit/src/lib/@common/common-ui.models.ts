export type NumberOrString = number | string;
export type HseUiBaseVariants = Exclude<HseUiVariants, HseUiVariants.TERTIARY>;

export enum HseUiSizes {
  SMALL = 'small',
  MEDIUM = 'medium',
  LARGE = 'large',
}

export enum HseButtonTypes {
  BUTTON = 'button',
  SUBMIT = 'submit',
  RESET = 'reset',
}

export enum HseDirectionTypes {
  COLUMN = 'column',
  ROW = 'row',
}

export enum HseUiVariants {
  PRIMARY = 'primary',
  SECONDARY = 'secondary',
  TERTIARY = 'tertiary',
}

export enum HseColorVariants {
  PRIMARY = 'primary',
  SUCCESS = 'success',
  WARNING = 'warning',
  ERROR = 'error',
}
