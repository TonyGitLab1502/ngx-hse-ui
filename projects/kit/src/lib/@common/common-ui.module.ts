import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HseLabelTemplateDirective } from './directives';

const declarations: any[] = [HseLabelTemplateDirective];

@NgModule({
  declarations,
  imports: [CommonModule],
  exports: [...declarations],
})
export class HseCommonUiModule {}
