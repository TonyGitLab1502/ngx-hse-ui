import { Directive, Input, OnDestroy, Renderer2, TemplateRef, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[hseLabelTemplate]',
})
export class HseLabelTemplateDirective implements OnDestroy {
  @Input()
  set label(value: string | TemplateRef<any>) {
    this.updateView(value);
  }

  textNode: Text | null = null;

  constructor(private readonly _vcRef: ViewContainerRef, private readonly _renderer: Renderer2) {}

  ngOnDestroy(): void {
    this.clearView();
  }

  private updateView(value: string | TemplateRef<any>): void {
    this.clearView();

    if (value instanceof TemplateRef) {
      this._vcRef.createEmbeddedView(value);
    } else {
      const element: HTMLElement = this._vcRef.element.nativeElement;
      this.textNode = this._renderer.createText(value);

      this._renderer.insertBefore(element.parentNode, this.textNode, element);
    }
  }

  private clearView(): void {
    this._vcRef.clear();

    if (this.textNode) {
      this._renderer.removeChild(this.textNode.parentNode, this.textNode);
    }
  }
}
