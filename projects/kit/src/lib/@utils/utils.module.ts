import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UtilsService } from './utils.service';

const declarations: any[] = [];

@NgModule({
  declarations,
  imports: [CommonModule],
  providers: [UtilsService],
  exports: [...declarations],
})
export class UtilsModule {}
