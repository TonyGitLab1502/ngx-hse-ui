import { Injectable } from '@angular/core';
import { HseUiSizes } from '../@common';

@Injectable({
  providedIn: 'root',
})
export class UtilsService {
  static s4(): string {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }

  constructor() {}

  public getSizeClass(size: HseUiSizes): string {
    switch (size) {
      case HseUiSizes.SMALL:
        return 'sm';
      case HseUiSizes.LARGE:
        return 'lg';
      default:
        return 'md';
    }
  }

  public generateUniqId(): string {
    return `${new Date().getTime()}-${UtilsService.s4()}-${UtilsService.s4()}-${UtilsService.s4()}-${UtilsService.s4()}`;
  }

  public createRange(start: number, end: number): number[] {
    return Array.from({ length: end - start + 1 }, (value, index) => start + index);
  }

  public times(count: number = 1, func: Function): void {
    for (let i = 0; i < count; i++) {
      func();
    }
  }

  public sum(arr: number[]): number {
    return arr.reduce((a, b) => a + b);
  }

  public isObject(value: any) {
    return !Array.isArray(value) && typeof value === 'object' && value !== null
  }

  public isArray(value: any) {
    return Array.isArray(value);
  }

  public isNumber(value: any) {
    return typeof value === 'number';
  }

  public removeItem(arr: any[], value: any) {
    const index = arr.indexOf(value);
    if (index > -1) {
      arr.splice(index, 1);
    }

    return arr;
  }

  humanFileSize(size: number) {
    const i = size == 0 ? 0 : Math.floor(Math.log(size) / Math.log(1024));
    // @ts-ignore
    return (size / Math.pow(1024, i)).toFixed(2) * 1 + ' ' + ['B', 'kB', 'MB', 'GB', 'TB'][i];
  }
}
