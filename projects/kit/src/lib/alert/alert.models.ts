import { TemplateRef } from '@angular/core';

export enum HseAlertVariants {
  INFO = 'info',
  SUCCESS = 'success',
  WARNING = 'warning',
  ERROR = 'error'
}

export interface HseAlertItem {
  id: string;
  title: string;
  description: string | TemplateRef<void>;
  icon: string;
  actions: HseAlertAction[];
  isFadeOut: boolean;
  variant: HseAlertVariants
}

export interface HseAlertAction {
  name: string;
  callback: () => void
}

export type HseAlertProps = { title: string } & Partial<Pick<HseAlertItem, 'description' | 'icon' | 'actions' | 'variant'>>
