import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HseAlertService } from './alert.service';
import { HseAlertsComponent } from './components/alerts/alerts.component';
import { HseCommonUiModule } from '../@common';
import { HseIconModule } from '../icon';

const declarations: any[] = [HseAlertsComponent];

@NgModule({
  declarations,
  imports: [CommonModule, HseCommonUiModule, HseIconModule],
  providers: [HseAlertService],
  exports: [...declarations],
})
export class HseAlertModule { }
