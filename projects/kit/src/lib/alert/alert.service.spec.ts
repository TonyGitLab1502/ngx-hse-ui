import { TestBed } from '@angular/core/testing';

import { HseAlertService } from './alert.service';

describe('HseAlertService', () => {
  let service: HseAlertService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HseAlertService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
