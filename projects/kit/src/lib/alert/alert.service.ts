import { Injectable, TemplateRef } from '@angular/core';
import { HseAlertAction, HseAlertProps, HseAlertVariants } from './alert.models';
import { UtilsService } from '../@utils';
import { BehaviorSubject } from 'rxjs';

export class HseAlert {
  constructor(
    public id: string,
    public icon: string,
    public title: string,
    public description: string | TemplateRef<void>,
    public actions: HseAlertAction[],
    public variant: HseAlertVariants = HseAlertVariants.INFO,
    public isFadeOut: boolean = false
  ) {}
}

@Injectable({
  providedIn: 'root'
})
export class HseAlertService {
  private readonly _alertTimeout: number = 5000;
  private readonly _timeout: number = 1500;
  public alerts$: BehaviorSubject<HseAlert[]> = new BehaviorSubject<HseAlert[]>([]);
  
  constructor(private readonly _utilsService: UtilsService) { }
  
  addAlert(props: HseAlertProps): void {
    const { title, description = '', icon = 'info', actions = [], variant = HseAlertVariants.INFO } = props;
    const id = this._utilsService.generateUniqId();
    const newAlert = new HseAlert(id, icon, title, description, actions, variant);
    
    this.alerts$.next([newAlert, ...this.alerts$.value]);
    
    if (!newAlert.actions.length) {
      setTimeout(() => this.removeAlert(newAlert), this._alertTimeout);
    }
  }
  
  removeAlert(alert: HseAlert): void {
    alert.isFadeOut = true;

    setTimeout(() => {
      this.alerts$.next(this.alerts$.value.filter(($alert: HseAlert) => $alert.id !== alert.id));
    }, this._timeout);
  }
}
