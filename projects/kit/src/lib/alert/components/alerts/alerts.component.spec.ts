import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HseAlertsComponent } from './alerts.component';

describe('HseAlertsComponent', () => {
  let component: HseAlertsComponent;
  let fixture: ComponentFixture<HseAlertsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HseAlertsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HseAlertsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
