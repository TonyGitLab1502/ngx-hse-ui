import { ChangeDetectionStrategy, Component, HostBinding, ViewEncapsulation } from '@angular/core';
import { HseAlert, HseAlertService } from '../../alert.service';
import { HseAlertAction } from '../../alert.models';
import { BehaviorSubject, Observable } from 'rxjs';

@Component({
  selector: 'hse-alerts',
  template: `
    <ng-container *ngFor="let alert of alerts$ | async">
      <div class="hse-alert__wrapper fadeInRight" [class.fadeOutRight]="alert.isFadeOut">
        <div class="hse-alert hse-alert_{{ alert.variant }}">
          <div class="hse-alert__icon-wrapper">
            <hse-icon class="hse-alert__icon" [name]="alert.icon"></hse-icon>
          </div>
          <div class="hse-alert__content">
            <div class="hse-alert__title">{{ alert.title }}</div>
            <ng-container *ngIf="alert.description">
              <div class="hse-alert__description">
                <ng-container hseLabelTemplate [label]="alert.description"></ng-container>
              </div>
            </ng-container>
            <ng-container *ngIf="alert.actions.length">
              <div class="hse-alert__actions">
                <ng-container *ngFor="let action of alert.actions">
                  <button class="hse-alert__action" (click)="executeAlertAction(action, alert)">
                    {{ action.name }}
                  </button>
                </ng-container>
              </div>
            </ng-container>
          </div>
          <div class="hse-alert__icon-wrapper">
            <hse-icon class="hse-alert__close-icon" name="clear" (click)="closeAlert(alert)"></hse-icon>
          </div>
        </div>
      </div>
    </ng-container>
  `,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HseAlertsComponent {
  @HostBinding('class.hse-alerts') private readonly baseCss: boolean = true;
  
  get alerts$(): Observable<HseAlert[]> {
    return this._alertService.alerts$.asObservable();
  }
  
  constructor(private readonly _alertService: HseAlertService) {}
  
  executeAlertAction(action: HseAlertAction, alert: HseAlert): void {
    this.closeAlert(alert);
    action.callback();
  }
  
  closeAlert(alert: HseAlert): void {
    this._alertService.removeAlert(alert);
  }
}
