import { TemplateRef } from '@angular/core';

export type HseButtonGroup = HseButtonGroupItem[];
export type HseButtonGroupItem = {
  label: string | TemplateRef<void>;
  active: boolean;
  disabled: boolean;
  onClick: (btn: HseButtonGroupItem) => void;
};
