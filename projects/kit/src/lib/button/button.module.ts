import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HseCommonUiModule } from '../@common';
import { HseButtonComponent } from './components/button/button.component';
import { HseButtonGroupComponent } from './components/button-group/button-group.component';
import { HseIconButtonComponent } from './components/icon-button/icon-button.component';
import { HseIconModule } from '../icon';

const declarations: any[] = [HseButtonComponent, HseButtonGroupComponent, HseIconButtonComponent];

@NgModule({
  declarations,
  imports: [CommonModule, HseCommonUiModule, HseIconModule],
  exports: [...declarations],
})
export class HseButtonModule {}
