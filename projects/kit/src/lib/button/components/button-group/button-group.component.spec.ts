import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HseButtonGroupComponent } from './button-group.component';

describe('HseButtonGroupComponent', () => {
  let component: HseButtonGroupComponent;
  let fixture: ComponentFixture<HseButtonGroupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HseButtonGroupComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(HseButtonGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
