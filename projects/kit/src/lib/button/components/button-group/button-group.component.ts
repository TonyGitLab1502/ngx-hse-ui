import { ChangeDetectionStrategy, Component, HostBinding, Input, ViewEncapsulation } from '@angular/core';
import { HseUiSizes } from '../../../@common';
import { UtilsService } from '../../../@utils';
import { HseButtonGroup, HseButtonGroupItem } from '../../button.models';

@Component({
  selector: 'hse-button-group',
  template: `
    <ng-container *ngFor="let button of buttons">
      <button
        class="hse-button-group__button"
        [class.active]="button.active"
        [disabled]="button.disabled"
        (click)="onButtonClick(button)"
      >
        <ng-container hseLabelTemplate [label]="button.label"></ng-container>
      </button>
    </ng-container>
  `,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HseButtonGroupComponent {
  @HostBinding('class.hse-button-group') private readonly baseCss: boolean = true;

  @Input() size: HseUiSizes = HseUiSizes.MEDIUM;
  @Input()
  set buttons(value: HseButtonGroup) {
    this._buttons = [...value];
  }

  @HostBinding('class') get additionalClasses() {
    return `
      hse-button-group_primary
      hse-button-group_${this._utilsService.getSizeClass(this.size)}
    `;
  }

  get buttons(): HseButtonGroup {
    return this._buttons;
  }

  private _buttons: HseButtonGroup = [];

  constructor(private readonly _utilsService: UtilsService) {}

  onButtonClick(button: HseButtonGroupItem) {
    button.active = !button.active;
    button.onClick(button);
  }
}
