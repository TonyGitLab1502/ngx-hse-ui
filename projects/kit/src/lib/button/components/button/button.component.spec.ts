import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HseButtonComponent } from './button.component';

describe('HseButtonComponent', () => {
  let component: HseButtonComponent;
  let fixture: ComponentFixture<HseButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HseButtonComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(HseButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
