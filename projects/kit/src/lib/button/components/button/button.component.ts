import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  HostBinding,
  Input,
  Output,
  ViewEncapsulation,
} from '@angular/core';
import { HseButtonTypes, HseUiSizes, HseUiVariants } from '../../../@common';
import { UtilsService } from '../../../@utils';

@Component({
  selector: 'hse-button',
  template: `
    <button class="hse-button__button" [disabled]="disabled" [type]="type" (click)="onButtonClick()">
      <ng-content></ng-content>
    </button>
  `,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HseButtonComponent {
  @HostBinding('class.hse-button') private readonly baseCss: boolean = true;

  @Input() disabled: boolean = false;
  @Input() inactive: boolean = false;
  @Input() size: HseUiSizes = HseUiSizes.MEDIUM;
  @Input() type: HseButtonTypes = HseButtonTypes.BUTTON;
  @Input() variant: HseUiVariants = HseUiVariants.PRIMARY;

  @Output() onClick: EventEmitter<void> = new EventEmitter<void>();

  @HostBinding('class') get AdditionalClasses(): string {
    return `
      hse-button_${this._utilsService.getSizeClass(this.size)}
      hse-button_${this.variant}
      ${this.inactive ? 'hse-button_inactive' : ''}
    `;
  }

  constructor(private readonly _utilsService: UtilsService) {}

  onButtonClick(): void {
    if (!this.inactive) {
      this.onClick.emit();
    }
  }
}
