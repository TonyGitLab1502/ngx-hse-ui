import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HseIconButtonComponent } from './icon-button.component';

describe('HseIconButtonComponent', () => {
  let component: HseIconButtonComponent;
  let fixture: ComponentFixture<HseIconButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HseIconButtonComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(HseIconButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
