import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  HostBinding,
  Input,
  OnInit,
  Output,
  ViewEncapsulation,
} from '@angular/core';
import { HseUiSizes, HseUiVariants } from '../../../@common';
import { UtilsService } from '../../../@utils';

@Component({
  selector: 'hse-icon-button',
  template: `
    <button class="hse-icon-button__button" [disabled]="disabled" (click)="onButtonClick()">
      <ng-container *ngIf="iconName && !pageNumber">
        <hse-icon class="hse-icon-button__icon" [name]="iconName"></hse-icon>
      </ng-container>
      <ng-container *ngIf="pageNumber && !iconName">
        <span class="hse-icon-button__page-number">{{ pageNumber }}</span>
      </ng-container>
    </button>
  `,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HseIconButtonComponent implements OnInit {
  @HostBinding('class.hse-icon-button') private readonly baseCss: boolean = true;

  @Input() disabled: boolean = false;
  @Input() iconName: string = '';
  @Input() inactive: boolean = false;
  @Input() pageNumber: number | null = null;
  @Input() size: HseUiSizes = HseUiSizes.MEDIUM;
  @Input() variant: HseUiVariants = HseUiVariants.PRIMARY;

  @Output() onClick: EventEmitter<void> = new EventEmitter<void>();

  @HostBinding('class') get additionalClasses(): string {
    return `
      hse-icon-button_${this._utilsService.getSizeClass(this.size)}
      hse-icon-button_${this.variant}
      ${this.inactive ? 'hse-icon-button_inactive' : ''}
    `;
  }

  constructor(private readonly _utilsService: UtilsService) {}

  ngOnInit(): void {
    if (!this.iconName && !this.pageNumber) {
      throw `Ошибка! В компонент необходимо передать либо опцию iconName, либо опцию pageNumber`;
    }
  }

  onButtonClick(): void {
    if (!this.inactive) {
      this.onClick.emit();
    }
  }
}
