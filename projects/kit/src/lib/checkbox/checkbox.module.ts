import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HseCheckboxComponent } from './components/checkbox/checkbox.component';
import { HseSwitchComponent } from './components/switch/switch.component';
import { HseIconModule } from '../icon';

const declarations: any[] = [HseCheckboxComponent, HseSwitchComponent];

@NgModule({
  declarations,
  imports: [CommonModule, FormsModule, HseIconModule, ReactiveFormsModule],
  exports: [...declarations],
})
export class HseCheckboxModule {}
