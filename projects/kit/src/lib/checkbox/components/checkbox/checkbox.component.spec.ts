import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HseCheckboxComponent } from './checkbox.component';

describe('HseCheckboxComponent', () => {
  let component: HseCheckboxComponent;
  let fixture: ComponentFixture<HseCheckboxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HseCheckboxComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(HseCheckboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
