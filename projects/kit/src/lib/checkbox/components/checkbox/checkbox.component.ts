import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  forwardRef,
  HostBinding,
  Input,
  Output,
  ViewEncapsulation,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { HseUiSizes } from '../../../@common';
import { UtilsService } from '../../../@utils';

@Component({
  selector: 'hse-checkbox',
  template: `
    <ng-container *ngIf="model">
      <hse-icon
        class="hse-checkbox__input_checked"
        [class.hse-checkbox__input_disabled]="disabled"
        (click)="onChange(false)"
        name="check"
      ></hse-icon>
    </ng-container>
    <input
      class="hse-checkbox__input"
      type="checkbox"
      [disabled]="disabled"
      [id]="checkboxId"
      [ngModel]="model"
      (ngModelChange)="onChange($event)"
    />
    <label class="hse-checkbox__label" for="{{ checkboxId }}">
      <ng-content></ng-content>
    </label>
  `,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => HseCheckboxComponent),
      multi: true,
    },
  ],
})
export class HseCheckboxComponent implements ControlValueAccessor {
  @HostBinding('class.hse-checkbox') private readonly baseCss: boolean = true;

  @Input() disabled: boolean = false;
  @Input() hasError: boolean = false;
  @Input()
  set model(value: boolean) {
    this._value = value;
    this._onChange(value);
  }
  @Input() size: HseUiSizes = HseUiSizes.MEDIUM;

  @Output() modelChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @HostBinding('class') get additionalClasses(): string {
    return `
      hse-checkbox_${this._utilsService.getSizeClass(this.size)}
      ${this.hasError ? 'hse-checkbox_invalid' : ''}
    `;
  }

  get model(): boolean {
    return this._value;
  }

  get checkboxId(): string {
    return this._checkboxId;
  }

  private readonly _checkboxId: string;
  private _value: boolean = false;
  private _onChange: (value: boolean) => void = () => {};
  private _onTouched: () => void = () => {};

  constructor(private readonly _utilsService: UtilsService) {
    this._checkboxId = this._utilsService.generateUniqId();
  }

  writeValue(value: boolean): void {
    this._value = value;
  }

  registerOnChange(fn: any): void {
    this._onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this._onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  onChange(value: boolean): void {
    this._value = value;

    this._onChange(this._value);
    this._onTouched();

    this.modelChange.emit(this._value);
  }
}
