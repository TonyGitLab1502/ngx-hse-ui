import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HseSwitchComponent } from './switch.component';

describe('HseSwitchComponent', () => {
  let component: HseSwitchComponent;
  let fixture: ComponentFixture<HseSwitchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HseSwitchComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(HseSwitchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
