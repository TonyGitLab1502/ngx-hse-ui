import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  forwardRef,
  HostBinding,
  Input,
  Output,
  ViewEncapsulation,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { HseUiSizes } from '../../../@common';
import { UtilsService } from '../../../@utils';

@Component({
  selector: 'hse-switch',
  template: `
    <input
      class="hse-switch__input"
      type="checkbox"
      [disabled]="disabled"
      [id]="switchId"
      [ngModel]="model"
      (ngModelChange)="onChange($event)"
    />
    <label class="hse-switch__label" for="{{ switchId }}">
      <ng-content></ng-content>
    </label>
  `,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => HseSwitchComponent),
      multi: true,
    },
  ],
})
export class HseSwitchComponent implements ControlValueAccessor {
  @HostBinding('class.hse-switch') private readonly baseCss: boolean = true;

  @Input() disabled: boolean = false;
  @Input() hasError: boolean = false;
  @Input()
  set model(value: boolean) {
    this._value = value;
    this._onChange(value);
  }
  @Input() size: HseUiSizes = HseUiSizes.MEDIUM;

  @Output() modelChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @HostBinding('class') get additionalClasses(): string {
    return `
      hse-switch_${this._utilsService.getSizeClass(this.size)}
      ${this.hasError ? 'hse-switch_invalid' : ''}
    `;
  }

  get model(): boolean {
    return this._value;
  }

  get switchId(): string {
    return this._switchId;
  }

  private readonly _switchId: string;
  private _value: boolean = false;
  private _onChange: (value: boolean) => void = () => {};
  private _onTouched: () => void = () => {};

  constructor(private readonly _utilsService: UtilsService) {
    this._switchId = this._utilsService.generateUniqId();
  }

  writeValue(value: boolean): void {
    this._value = value;
  }

  registerOnChange(fn: any): void {
    this._onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this._onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  onChange(value: boolean): void {
    this._value = value;

    this._onChange(this._value);
    this._onTouched();
    this.modelChange.emit(this._value);
  }
}
