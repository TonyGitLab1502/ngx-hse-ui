import { TemplateRef } from '@angular/core';

export type HseCollapsePanel = {
  active: boolean,
  content: string | TemplateRef<void>,
  label: string | TemplateRef<void>,
}
