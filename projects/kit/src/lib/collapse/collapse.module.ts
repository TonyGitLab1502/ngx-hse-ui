import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HseCollapseComponent } from './components/collapse/collapse.component';
import { HseCollapsePanelComponent } from './components/collapse-panel/collapse-panel.component';
import { HseCommonUiModule } from '../@common';
import { HseIconModule } from '../icon';

const declarations: any = [HseCollapseComponent, HseCollapsePanelComponent];

@NgModule({
  declarations,
  imports: [CommonModule, HseCommonUiModule, HseIconModule],
  exports: [...declarations],
})
export class HseCollapseModule { }
