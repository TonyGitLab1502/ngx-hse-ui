import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HseCollapsePanelComponent } from './collapse-panel.component';

describe('HseCollapsePanelComponent', () => {
  let component: HseCollapsePanelComponent;
  let fixture: ComponentFixture<HseCollapsePanelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HseCollapsePanelComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HseCollapsePanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
