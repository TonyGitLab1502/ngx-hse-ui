import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  HostBinding,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  TemplateRef,
  ViewEncapsulation,
} from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'hse-collapse-panel',
  template: `
    <button
      class="hse-collapse-panel__trigger"
      [class.active]="active"
      (click)="toggle()"
    >
      <div class="hse-collapse-panel__title">
        <ng-container hseLabelTemplate [label]="label"></ng-container>
      </div>
      <hse-icon
        class="hse-collapse-panel__icon"
        name="expandMore"
        [style.transform]="active ? 'rotate(-180deg)' : 'rotate(0deg)'"
      ></hse-icon>
    </button>
    <div class="hse-collapse-panel__content-wrapper" [@slideInOut]="contentState">
      <div class="hse-collapse-panel__content" [class.has-left-indent]="hasLeftIndent">
        <ng-container hseLabelTemplate [label]="content"></ng-container>
      </div>
    </div>
  `,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('slideInOut', [
      state('in', style({
        overflow: 'hidden',
        height: '*'
      })),
      state('out', style({
        overflow: 'hidden',
        height: 0
      })),
      transition('in <=> out', animate('300ms ease-in-out'))
    ])
  ]
})
export class HseCollapsePanelComponent implements OnInit, OnChanges {
  @HostBinding('class.hse-collapse-panel') private readonly baseCss: boolean = true;
  
  @Input() active: boolean = false;
  @Input() borderless: boolean = false;
  @Input() content: string | TemplateRef<void> = '';
  @Input() hasLeftIndent: boolean = false;
  @Input() label: string | TemplateRef<void> = '';
  
  @Output() onPanelToggle: EventEmitter<void> = new EventEmitter<void>();
  
  @HostBinding('class') get additionalClasses(): string {
    return `
      ${this.borderless ? 'hse-collapse-panel_borderless' : ''}
    `;
  }

  public contentState: string = '';

  ngOnInit(): void {
    this.contentState = this.active ? 'in' : 'out';
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!changes['active'].isFirstChange()) {
      this.contentState = this.active ? 'in' : 'out'
    }
  }
  
  toggle(): void {
    this.onPanelToggle.emit();
  }
}
