import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HseCollapseComponent } from './collapse.component';

describe('HseCollapseComponent', () => {
  let component: HseCollapseComponent;
  let fixture: ComponentFixture<HseCollapseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HseCollapseComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HseCollapseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
