import { ChangeDetectionStrategy, Component, HostBinding, Input, ViewEncapsulation } from '@angular/core';
import { HseCollapsePanel } from '../../collapse.models';

@Component({
  selector: 'hse-collapse',
  template: `
    <ng-container *ngFor="let panel of panels">
      <hse-collapse-panel
        [active]="panel.active"
        [borderless]="borderless"
        [content]="panel.content"
        [hasLeftIndent]="hasLeftIndent"
        [label]="panel.label"
        (onPanelToggle)="toggle(panel)"
      ></hse-collapse-panel>
    </ng-container>
  `,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HseCollapseComponent {
  @HostBinding('class.hse-collapse') private readonly baseCss: boolean = true;
  
  @Input() accordionMode: boolean = false;
  @Input() panels: HseCollapsePanel[] = [];
  @Input() borderless: boolean = false;
  @Input() hasLeftIndent: boolean = false;
  
  toggle(panel: HseCollapsePanel): void {
    if (this.accordionMode) {
      this.panels.forEach(
        (_panel: HseCollapsePanel) => _panel.active = Object.is(_panel, panel) ? !_panel.active : false
      );
    } else {
      panel.active = !panel.active;
    }
  }
}
