import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HseCalendarComponent } from './calendar.component';

describe('HseCalendarComponent', () => {
  let component: HseCalendarComponent;
  let fixture: ComponentFixture<HseCalendarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HseCalendarComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(HseCalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
