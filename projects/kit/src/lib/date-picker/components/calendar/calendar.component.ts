import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  HostBinding,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewEncapsulation,
} from '@angular/core';
import { HseUiVariants } from '../../../@common';
import { UtilsService } from '../../../@utils';
import {
  HseCalendarDateRangeTypes,
  HseCalendarItem,
  HseCalendarSelectionModes,
  HseCalendarWeekday,
  HseCalendarWeeks,
  HseDateModel,
  HseDatePickerLocale,
  HseDateSelectionModes,
} from '../../date-picker.models';
import { DateTime, Interval } from 'luxon';

@Component({
  selector: 'hse-calendar',
  template: `
    <div class="hse-calendar__toolbar">
      <hse-icon-button
        iconName="chevronLeft"
        [disabled]="mode !== modes.DAY"
        [variant]="tertiary"
        (onClick)="switchToAnotherMonth()"
      ></hse-icon-button>
      <hse-button [variant]="mode === modes.MONTH ? primary : tertiary" (onClick)="onSwitchSelectionMode(modes.MONTH)">
        {{ selectedMonth?.label | titlecase }}
      </hse-button>
      <hse-button [variant]="mode === modes.YEAR ? primary : tertiary" (onClick)="onSwitchSelectionMode(modes.YEAR)">
        {{ selectedYear?.label }}
      </hse-button>
      <hse-icon-button
        iconName="chevronRight"
        [disabled]="mode !== modes.DAY"
        [variant]="tertiary"
        (onClick)="switchToAnotherMonth(true)"
      ></hse-icon-button>
    </div>
    <div class="hse-calendar__calendar-wrapper">
      <ng-container [ngSwitch]="mode">
        <ng-container *ngSwitchCase="modes.DAY">
          <div class="hse-calendar__weekdays">
            <ng-container *ngFor="let weekday of weekdays">
              <span class="hse-calendar__weekday">{{ weekday.label | titlecase }}</span>
            </ng-container>
          </div>
          <div class="hse-calendar__weeks-wrapper">
            <ng-container *ngFor="let week of weeks">
              <div class="hse-calendar__week">
                <ng-container *ngFor="let day of week">
                  <div [ngClass]="day.classNames" (click)="onSelectDay(day)" (mouseover)="hoverDay(day)">
                    {{ day.label }}
                  </div>
                </ng-container>
              </div>
            </ng-container>
          </div>
        </ng-container>
        <ng-container *ngSwitchCase="modes.MONTH">
          <div class="hse-calendar__months">
            <ng-container *ngFor="let month of months">
              <hse-button
                [variant]="month.value === selectedMonth?.value ? primary : tertiary"
                (onClick)="onMonthChange(month)"
              >
                {{ month.label | titlecase }}
              </hse-button>
            </ng-container>
          </div>
        </ng-container>
        <ng-container *ngSwitchCase="modes.YEAR">
          <div class="hse-calendar__years-wrapper">
            <div class="hse-calendar__years">
              <ng-container *ngFor="let year of years">
                <hse-button
                  [variant]="year.value === selectedYear?.value ? primary : tertiary"
                  (onClick)="onYearChange(year)"
                >
                  {{ year.label }}
                </hse-button>
              </ng-container>
            </div>
          </div>
        </ng-container>
      </ng-container>
    </div>
  `,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HseCalendarComponent implements OnInit, OnChanges {
  @HostBinding('class.hse-calendar') private readonly baseCss: boolean = true;

  @Input() dateStart: DateTime | null = null;
  @Input() dateEnd: DateTime | null = null;
  @Input() locale: HseDatePickerLocale = 'ru';
  @Input() maxDate: DateTime | null = null;
  @Input() maxYear: number = 2100;
  @Input() minDate: DateTime | null = null;
  @Input() minYear: number = 2000;
  @Input() selectionMode: HseDateSelectionModes = HseDateSelectionModes.SINGLE;

  @Output() onChange: EventEmitter<HseDateModel> = new EventEmitter<HseDateModel>();

  get primary(): HseUiVariants.PRIMARY {
    return HseUiVariants.PRIMARY;
  }

  get tertiary(): HseUiVariants.TERTIARY {
    return HseUiVariants.TERTIARY;
  }

  get months(): HseCalendarItem[] {
    return this._months;
  }

  get weekdays(): HseCalendarItem[] {
    return this._weekDays;
  }

  get weeks(): HseCalendarWeeks {
    return this._weeks;
  }

  get years(): HseCalendarItem[] {
    return this._years;
  }

  private readonly MONTH_FORMAT: string = 'LLLL';
  private readonly YEAR_FORMAT: string = 'yyyy';
  private readonly WEEKDAY_FORMAT: string = 'ccc';
  private readonly FIRST_MONTH_NUMBER: number = 1;
  private readonly LAST_MONTH_NUMBER: number = 12;
  private readonly WEEKDAYS_COUNT: number = 7;
  private readonly MONTHS_COUNT: number = 12;
  private _currentDate: DateTime | null = null;
  private _hoverStart: DateTime | null = null;
  private _hoverEnd: DateTime | null = null;
  private _isHovering: boolean = false;
  private _lastEditedDateRangeType: HseCalendarDateRangeTypes | null = null;
  private _months: HseCalendarItem[] = [];
  private _now: DateTime | null = null;
  private _weekDays: HseCalendarItem[] = [];
  private _weeks: HseCalendarWeeks = [];
  private _years: HseCalendarItem[] = [];

  public mode: HseCalendarSelectionModes = HseCalendarSelectionModes.DAY;
  public modes: typeof HseCalendarSelectionModes = HseCalendarSelectionModes;
  public selectedMonth: HseCalendarItem | null = null;
  public selectedYear: HseCalendarItem | null = null;

  constructor(private readonly _utilsService: UtilsService) {
    this._currentDate = DateTime.now();
    this._now = DateTime.now();
    this.selectedMonth = {
      label: this._currentDate.setLocale(this.locale).toFormat(this.MONTH_FORMAT),
      value: this._currentDate.month,
    };
    this.selectedYear = {
      label: this._currentDate.setLocale(this.locale).toFormat(this.YEAR_FORMAT),
      value: this._currentDate.year,
    };
  }

  ngOnInit(): void {
    this.buildCalendarItems();
    this.buildWeeks();
  }

  ngOnChanges(changes: SimpleChanges): void {
    const { locale } = changes;

    if (locale && !locale.isFirstChange()) {
      this.buildCalendarItems();
      this.buildWeeks();
    }
  }

  switchToAnotherMonth(toNextMonth: boolean = false): void {
    if (this._currentDate) {
      const newDate: DateTime = toNextMonth
        ? this._currentDate.plus({ month: 1 })
        : this._currentDate.minus({ month: 1 });

      this._currentDate = newDate;
      this.selectedMonth = this.months.find(
        (month: HseCalendarItem) => month.value === this._currentDate?.month
      ) as HseCalendarItem;

      if ([this.FIRST_MONTH_NUMBER, this.LAST_MONTH_NUMBER].includes(this._currentDate.month)) {
        this.selectedYear = this.years.find(
          (year: HseCalendarItem) => year.value === this._currentDate?.year
        ) as HseCalendarItem;
      }

      this.buildWeeks();
    }
  }

  onSwitchSelectionMode(mode: HseCalendarSelectionModes): void {
    this.mode = mode;
  }

  onMonthChange(month: HseCalendarItem): void {
    const newCurrentDate = DateTime.fromObject({
      month: month.value,
      year: (this._currentDate as DateTime).year,
    });

    this._currentDate = newCurrentDate;
    this.mode = this.modes.DAY;
    this.selectedMonth = month;
    this.buildWeeks();
  }

  onYearChange(year: HseCalendarItem): void {
    const newCurrentDate = DateTime.fromObject({
      month: (this._currentDate as DateTime).month,
      year: year.value,
    });

    this._currentDate = newCurrentDate;
    this.mode = this.modes.DAY;
    this.selectedYear = year;
    this.buildWeeks();
  }

  onSelectDay(day: HseCalendarWeekday): void {
    if (day.isDisabled) {
      return;
    }

    switch (this.selectionMode) {
      case HseDateSelectionModes.SINGLE:
        this.dateStart = day.date;
        break;
      case HseDateSelectionModes.WEEK:
        this.dateStart = day.date.startOf('week');
        this.dateEnd = day.date.endOf('week');
        break;
      case HseDateSelectionModes.RANGE:
        this.selectRange(day);
        break;
    }

    this.setDaysClasses();

    if (this.dateStart && this.dateEnd) {
      this.clearHoverRange();
    }

    if (
      (this.dateStart && !this.dateEnd && this.selectionMode === HseDateSelectionModes.SINGLE) ||
      (this.dateStart && this.dateEnd && this.selectionMode !== HseDateSelectionModes.SINGLE)
    ) {
      this.onChange.emit({
        dateStart: this.dateStart,
        dateEnd: this.dateEnd,
      });
    }
  }

  hoverDay(day: HseCalendarWeekday) {
    if (
      this.selectionMode === HseDateSelectionModes.SINGLE ||
      (this.selectionMode === HseDateSelectionModes.RANGE && !this._isHovering)
    ) {
      return;
    }

    if (this.selectionMode === HseDateSelectionModes.WEEK) {
      if (
        !(this.dateStart && this.dateEnd) ||
        Interval.fromDateTimes(this.dateStart, this.dateEnd).contains(day.date)
      ) {
        this._hoverStart = day.date.startOf('week');
        this._hoverEnd = day.date.endOf('week');
      }
    }

    if (this.selectionMode === HseDateSelectionModes.RANGE && this.dateStart) {
      if (day.date.startOf('day') <= this.dateStart.startOf('day')) {
        this._hoverStart = day.date;
        this._hoverEnd = this.dateStart;
      }

      if (
        (this.dateEnd && day.date.startOf('day') >= this.dateEnd.startOf('day')) ||
        (!this.dateEnd && day.date.startOf('day') >= this.dateStart.startOf('day'))
      ) {
        this._hoverStart = this.dateStart;
        this._hoverEnd = day.date;
      }

      if (this.dateStart && this.dateEnd && Interval.fromDateTimes(this.dateStart, this.dateEnd).contains(day.date)) {
        if (this._lastEditedDateRangeType === HseCalendarDateRangeTypes.START) {
          this._hoverStart = day.date;
        } else {
          this._hoverEnd = day.date;
        }
      }
    }

    this.setDaysClasses();
  }

  clearHoverRange() {
    this._hoverStart = null;
    this._hoverEnd = null;
  }

  private buildCalendarItems(): void {
    this._months = this._utilsService.createRange(1, this.MONTHS_COUNT).map((month: number) => ({
      label: DateTime.fromObject({ month }).setLocale(this.locale).toFormat(this.MONTH_FORMAT),
      value: month,
    }));

    this._years = this._utilsService.createRange(this.minYear, this.maxYear).map((year: number) => ({
      label: year.toString(),
      value: year,
    }));

    this._weekDays = this._utilsService.createRange(1, this.WEEKDAYS_COUNT).map((weekday: number) => ({
      label: DateTime.fromObject({ weekday }).setLocale(this.locale).toFormat(this.WEEKDAY_FORMAT),
      value: weekday,
    }));
  }

  private buildWeeks(): void {
    const monthStart: DateTime = (this._currentDate as DateTime).startOf('month');
    const monthEnd: DateTime = (this._currentDate as DateTime).endOf('month');
    let start: DateTime = monthStart.weekday > 1 ? monthStart.minus({ day: monthStart.weekday - 1 }) : monthStart;
    let end: DateTime =
      monthEnd.weekday < this.WEEKDAYS_COUNT
        ? monthEnd.plus({ day: this.WEEKDAYS_COUNT - monthEnd.weekday })
        : monthEnd;

    this._weeks = [];

    while (start.startOf('day') <= end.startOf('day')) {
      const week: HseCalendarWeekday[] = [];

      this._utilsService.times(this.WEEKDAYS_COUNT, () => {
        week.push({
          date: start,
          isCurrentMonth: start.month === (this._currentDate as DateTime).month,
          isDisabled: this.dayIsDisabled(start),
          isToday: start.hasSame(DateTime.now(), 'day'),
          label: start.day,
        });

        start = start.plus({ day: 1 });
      });

      this._weeks.push(week);
    }

    this.setDaysClasses();
  }

  private setDaysClasses(): void {
    this._weeks.forEach((week: HseCalendarWeekday[]) => {
      week.forEach((day: HseCalendarWeekday) => {
        let active: boolean = false;

        if (this.dateStart && this.dateEnd && this.selectionMode !== HseDateSelectionModes.SINGLE) {
          //Interval полуоткрыт: включает начало, но не конец, поэтому добавляем к правой границе 1 день
          active =
            this.selectionMode === HseDateSelectionModes.RANGE
              ? Interval.fromDateTimes(
                  this.dateStart.startOf('day'),
                  this.dateEnd.startOf('day').plus({ day: 1 })
                ).contains(day.date)
              : Interval.fromDateTimes(this.dateStart.startOf('day'), this.dateEnd.startOf('day')).contains(day.date);
        }

        const isStart: boolean = !!this.dateStart && day.date.hasSame(this.dateStart as DateTime, 'day');
        const isEnd: boolean = !!this.dateEnd && day.date.hasSame(this.dateEnd as DateTime, 'day');
        const singleActive: boolean =
          this.selectionMode === HseDateSelectionModes.SINGLE &&
          !!this.dateStart &&
          day.date.hasSame(this.dateStart as DateTime, 'day');

        const commonClasses: string[] = [
          'hse-calendar__day',
          `${!day.isCurrentMonth ? 'not-current-month' : ''}`,
          `${day.isToday ? 'today' : ''}`,
          `${active ? 'active' : ''}`,
          `${singleActive ? 'single-active' : ''}`,
          `${isStart ? 'start' : ''}`,
          `${isEnd ? 'end' : ''}`,
          `${day.isDisabled ? 'is-disabled-day' : ''}`,
          `${active && !isStart && !isEnd ? 'is-inside-interval' : ''}`,
        ];

        let hoverClasses: string[] = [];

        if (this.selectionMode === HseDateSelectionModes.RANGE) {
          const isHoverStart: boolean = !!this._hoverStart && day.date.hasSame(this._hoverStart as DateTime, 'day');
          const isHoverEnd: boolean = !!this._hoverEnd && day.date.hasSame(this._hoverEnd as DateTime, 'day');
          //Interval полуоткрыт: включает начало, но не конец, поэтому добавляем к правой границе 1 день
          const isInHover: boolean =
            !!this._hoverStart &&
            !!this._hoverEnd &&
            Interval.fromDateTimes(
              this._hoverStart.startOf('day'),
              this._hoverEnd.startOf('day').plus({ day: 1 })
            ).contains(day.date);

          hoverClasses = [
            `${isHoverStart ? 'hover-start' : ''}`,
            `${isHoverEnd ? 'hover-end' : ''}`,
            `${isInHover ? 'hover-in' : ''}`,
          ];
        }

        day.classNames = [...commonClasses, ...hoverClasses].filter((classItem: string) => !!classItem).join(' ');
      });
    });
  }

  private dayIsDisabled(day: DateTime): boolean {
    const minDate =
      this.minDate && this.selectionMode === HseDateSelectionModes.WEEK ? this.minDate.endOf('week') : this.minDate;
    const maxDate =
      this.maxDate && this.selectionMode === HseDateSelectionModes.WEEK ? this.maxDate.startOf('week') : this.maxDate;

    return Boolean(
      (minDate && day.startOf('day') < minDate.startOf('day')) ||
        (maxDate && day.startOf('day') > maxDate.startOf('day'))
    );
  }

  private selectRange(day: HseCalendarWeekday): void {
    if ((!this.dateStart && !this.dateEnd) || (this.dateStart && this.dateEnd)) {
      this.dateStart = day.date;
      this.dateEnd = null;
      this._hoverStart = day.date;
      this._isHovering = true;

      return;
    }

    if (this.dateStart && day.date.startOf('day') <= this.dateStart.startOf('day')) {
      this.dateEnd = this.dateStart;
      this.dateStart = day.date;
    } else {
      this.dateEnd = day.date;
    }

    this._isHovering = false;
  }
}
