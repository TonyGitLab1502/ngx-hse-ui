import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HseDatePickerComponent } from './date-picker.component';

describe('HseDatePickerComponent', () => {
  let component: HseDatePickerComponent;
  let fixture: ComponentFixture<HseDatePickerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HseDatePickerComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(HseDatePickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
