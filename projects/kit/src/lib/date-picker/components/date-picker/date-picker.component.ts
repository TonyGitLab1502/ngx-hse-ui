import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  forwardRef,
  HostBinding,
  Input,
  OnInit,
  Output,
  TemplateRef,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { MatMenuTrigger } from '@angular/material/menu';
import { HseUiSizes } from '../../../@common';
import { UtilsService } from '../../../@utils';
import { HseDateModel, HseDatePickerLocale, HseDateSelectionModes } from '../../date-picker.models';
import { DateTime } from 'luxon';

@Component({
  selector: 'hse-date-picker',
  template: `
    <ng-container *ngIf="label">
      <label class="hse-date-picker__label" for="{{ datePickerId }}">
        <ng-container hseLabelTemplate [label]="label"></ng-container>
      </label>
    </ng-container>
    <div
      #datePickerMenuTrigger
      class="hse-date-picker__date-picker"
      [id]="datePickerId"
      [matMenuTriggerFor]="!disabled ? datePickerMenu : null"
    >
      <div class="hse-date-picker__selected-value-wrapper">
        <ng-container *ngIf="placeholder && !model">
          <span class="hse-date-picker__placeholder">{{ placeholder }}</span>
        </ng-container>
        <ng-container *ngIf="model">
          <span class="hse-date-picker__selected-value">
            <ng-container hseLabelTemplate [label]="selectedValue"></ng-container>
          </span>
        </ng-container>
      </div>
      <div class="hse-date-picker__icons">
        <hse-icon class="hse-date-picker__icon" name="calendar"></hse-icon>
      </div>
    </div>
    <mat-menu class="hse-date-picker__menu-override" #datePickerMenu="matMenu" [overlapTrigger]="false">
      <div class="hse-date-picker__menu-content" (click)="stopPropagation($event)">
        <hse-calendar
          [dateStart]="model?.dateStart || null"
          [dateEnd]="model?.dateEnd || null"
          [selectionMode]="selectionMode"
          (onChange)="onChange($event)"
        ></hse-calendar>
      </div>
    </mat-menu>
  `,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => HseDatePickerComponent),
      multi: true,
    },
  ],
})
export class HseDatePickerComponent implements ControlValueAccessor, OnInit {
  @HostBinding('class.hse-date-picker') private readonly baseCss: boolean = true;

  @Input() disabled: boolean = false;
  @Input() hasError: boolean = false;
  @Input() label: string | TemplateRef<void> = '';
  @Input() locale: HseDatePickerLocale = 'ru';
  @Input() maxDate: DateTime | null = null;
  @Input() maxYear: number = 2100;
  @Input() minDate: DateTime | null = null;
  @Input() minYear: number = 2000;
  @Input()
  set model(value: HseDateModel | null) {
    this._value = value;
    this._onChange(value);
  }
  @Input() placeholder: string = '';
  @Input() size: HseUiSizes = HseUiSizes.MEDIUM;

  @Output() modelChange: EventEmitter<HseDateModel | null> = new EventEmitter<HseDateModel | null>();
  @ViewChild(MatMenuTrigger, { static: false }) private _trigger!: MatMenuTrigger;

  @HostBinding('class') get additionalClasses(): string {
    return `
      hse-date-picker_${this._utilsService.getSizeClass(this.size)}
      ${this.disabled ? 'hse-date-picker_disabled' : ''}
      ${this.hasError ? 'hse-date-picker_invalid' : ''}
    `;
  }

  get model(): HseDateModel | null {
    return this._value;
  }

  get selectionMode(): HseDateSelectionModes.SINGLE {
    return HseDateSelectionModes.SINGLE;
  }

  get selectedValue(): string {
    return this._value
      ? (this._value.dateStart as DateTime).startOf('day').toFormat(this.locale === 'ru' ? 'dd.LL.yyyy' : 'yyyy-LL-dd')
      : '';
  }

  private _value: HseDateModel | null = null;
  private _onChange: (value: HseDateModel | null) => void = () => {};
  private _onTouched: () => void = () => {};

  public readonly datePickerId: string = '';

  constructor(private readonly _utilsService: UtilsService) {
    this.datePickerId = this._utilsService.generateUniqId();
  }

  ngOnInit(): void {}

  writeValue(value: HseDateModel | null): void {
    this._value = value;
  }

  registerOnChange(fn: any): void {
    this._onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this._onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  onClear(event: MouseEvent): void {
    event.stopPropagation();

    if (!this.disabled) {
      this.writeValue(null);
      this._onChange(null);
      this._onTouched();
      this.modelChange.emit(this._value);
    }
  }

  onChange(value: HseDateModel): void {
    this.writeValue(value);
    this._onChange(value);
    this._onTouched();
    this._trigger.closeMenu();
    this.modelChange.emit(this._value);
  }

  stopPropagation(event: MouseEvent): void {
    event.stopPropagation();
  }
}
