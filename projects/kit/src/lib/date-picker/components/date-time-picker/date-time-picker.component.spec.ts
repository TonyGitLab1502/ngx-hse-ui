import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HseDateTimePickerComponent } from './date-time-picker.component';

describe('HseDateTimePickerComponent', () => {
  let component: HseDateTimePickerComponent;
  let fixture: ComponentFixture<HseDateTimePickerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HseDateTimePickerComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(HseDateTimePickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
