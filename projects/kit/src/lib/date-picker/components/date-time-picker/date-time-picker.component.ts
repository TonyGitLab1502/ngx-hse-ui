import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  HostBinding,
  HostListener,
  Input,
  OnInit,
  Output,
  TemplateRef,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { MatMenuTrigger } from '@angular/material/menu';
import { HseDirectionTypes, HseUiSizes } from '../../../@common';
import { UtilsService } from '../../../@utils';
import { HseSelectOption } from '../../../select';
import { HseDateModel, HseDatePickerLocale, HseDateSelectionModes } from '../../date-picker.models';
import { DateTime, DateTimeUnit, DurationLike } from 'luxon';

@Component({
  selector: 'hse-date-time-picker',
  template: `
    <div class="hse-date-time-picker__component-wrapper">
      <ng-container *ngIf="dateLabel">
        <label class="hse-date-time-picker__label" for="{{ datePickerId }}">
          <ng-container hseLabelTemplate [label]="dateLabel"></ng-container>
        </label>
      </ng-container>
      <div
        #datePickerMenuTrigger
        class="hse-date-time-picker__picker"
        [id]="datePickerId"
        [matMenuTriggerFor]="!disabled ? datePickerMenu : null"
      >
        <div class="hse-date-time-picker__selected-value-wrapper">
          <ng-container *ngIf="datePlaceholder && !model">
            <span class="hse-date-time-picker__placeholder">{{ datePlaceholder }}</span>
          </ng-container>
          <ng-container *ngIf="model">
            <span class="hse-date-time-picker__selected-value">
              <ng-container hseLabelTemplate [label]="selectedDateValue"></ng-container>
            </span>
          </ng-container>
        </div>
        <div class="hse-date-time-picker__icons">
          <hse-icon class="hse-date-time-picker__icon" name="calendar"></hse-icon>
        </div>
      </div>
      <mat-menu class="hse-date-time-picker__menu-override" #datePickerMenu="matMenu" [overlapTrigger]="false">
        <div class="hse-date-time-picker__menu-content" (click)="stopPropagation($event)">
          <hse-calendar
            [dateStart]="model?.dateStart || null"
            [dateEnd]="model?.dateEnd || null"
            [selectionMode]="selectionMode"
            (onChange)="onChangeDate($event)"
          ></hse-calendar>
        </div>
      </mat-menu>
    </div>
    <div class="hse-date-time-picker__component-wrapper">
      <ng-container *ngIf="timeLabel">
        <label class="hse-date-time-picker__label" for="{{ timePickerId }}">{{ timeLabel }}</label>
      </ng-container>
      <div
        #timePickerEl
        class="hse-date-time-picker__picker"
        [id]="timePickerId"
        [matMenuTriggerFor]="!disabled ? optionList : null"
      >
        <div class="hse-date-time-picker__selected-value-wrapper">
          <ng-container *ngIf="timePlaceholder && !model">
            <span class="hse-date-time-picker__placeholder">{{ timePlaceholder }}</span>
          </ng-container>
          <ng-container *ngIf="model">
            <span class="hse-date-time-picker__selected-value">
              <ng-container hseLabelTemplate [label]="selectedTimeValue"></ng-container>
            </span>
          </ng-container>
        </div>
        <div class="hse-date-time-picker__icons">
          <hse-icon class="hse-date-time-picker__icon" name="time"></hse-icon>
        </div>
      </div>
      <mat-menu class="hse-date-time-picker__menu-override" #optionList="matMenu" [overlapTrigger]="false">
        <div [style.width.px]="timePickerWidth">
          <div class="hse-date-time-picker__option-list">
            <ng-container *ngFor="let option of options">
              <button
                class="hse-date-time-picker__option"
                [class.selected]="isSelected(option)"
                [disabled]="option.disabled"
                (click)="onChangeTime(option)"
              >
                <ng-container hseLabelTemplate [label]="option.label"></ng-container>
              </button>
            </ng-container>
          </div>
        </div>
      </mat-menu>
    </div>
  `,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => HseDateTimePickerComponent),
      multi: true,
    },
  ],
})
export class HseDateTimePickerComponent implements AfterViewInit, ControlValueAccessor, OnInit {
  @HostBinding('class.hse-date-time-picker') private readonly baseCss: boolean = true;

  @Input() dateLabel: string | TemplateRef<void> = '';
  @Input() datePlaceholder: string = '';
  @Input() direction: HseDirectionTypes = HseDirectionTypes.ROW;
  @Input() disabled: boolean = false;
  @Input() endHour: number = 23;
  @Input() hasError: boolean = false;
  @Input() locale: HseDatePickerLocale = 'ru';
  @Input() maxDate: DateTime | null = null;
  @Input() maxYear: number = 2100;
  @Input() minDate: DateTime | null = null;
  @Input() minYear: number = 2000;
  @Input()
  set model(value: HseDateModel | null) {
    this._value = value;
    this._onChange(value);
  }
  @Input() size: HseUiSizes = HseUiSizes.MEDIUM;
  @Input() startHour: number = 0;
  @Input() stepInMinutes: number = 60;
  @Input() timeLabel: string | TemplateRef<void> = '';
  @Input() timePlaceholder: string = '';

  @Output() modelChange: EventEmitter<HseDateModel | null> = new EventEmitter<HseDateModel | null>();

  @ViewChild(MatMenuTrigger, { static: false }) private _trigger!: MatMenuTrigger;
  @ViewChild('timePickerEl', { static: true })
  private readonly _timePickerEl: ElementRef<HTMLElement> = new ElementRef<HTMLElement>(this._elRef.nativeElement);

  @HostBinding('class') get additionalClasses(): string {
    return `
      hse-date-time-picker_${this.direction}
      hse-date-time-picker_${this._utilsService.getSizeClass(this.size)}
      ${this.disabled ? 'hse-date-time-picker_disabled' : ''}
      ${this.hasError ? 'hse-date-time-picker_invalid' : ''}
    `;
  }

  @HostListener('window:resize')
  onResize() {
    this.updateSelectMenuProps();
  }

  get model(): HseDateModel | null {
    return this._value;
  }

  get selectionMode(): HseDateSelectionModes.SINGLE {
    return HseDateSelectionModes.SINGLE;
  }

  get selectedDateValue(): string {
    return this._value
      ? (this._value.dateStart as DateTime).startOf('day').toFormat(this.locale === 'ru' ? 'dd.LL.yyyy' : 'yyyy-LL-dd')
      : '';
  }

  get selectedTimeValue(): string {
    return this._value ? (this._value.dateStart as DateTime).toFormat('T') : '';
  }

  get options(): HseSelectOption<DateTime>[] {
    return this._options;
  }

  get selectedOption(): HseSelectOption<DateTime> | null {
    return this._selectedOption;
  }

  private readonly MIN_START_HOUR: number = 0;
  private readonly MAX_START_HOUR: number = 23;
  private _options: HseSelectOption<DateTime>[] = [];
  private _selectedOption: HseSelectOption<DateTime> | null = null;
  private _value: HseDateModel | null = null;
  private _onChange: (value: HseDateModel | null) => void = () => {};
  private _onTouched: () => void = () => {};

  public readonly datePickerId: string = '';
  public readonly timePickerId: string = '';
  public timePickerWidth: number = 0;

  constructor(private readonly _elRef: ElementRef, private readonly _utilsService: UtilsService) {
    this.datePickerId = this._utilsService.generateUniqId();
    this.timePickerId = this._utilsService.generateUniqId();
  }

  ngOnInit(): void {
    if (this.checkTimeLimits()) {
      this.buildTimeOptions();
    }
  }

  ngAfterViewInit(): void {
    this.updateSelectMenuProps();
  }

  writeValue(value: HseDateModel | null): void {
    this._value = value;
  }

  registerOnChange(fn: any): void {
    this._onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this._onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  isSelected(option: HseSelectOption<DateTime>): boolean {
    return this.model
      ? option.value.hour === (this.model.dateStart as DateTime).hour &&
          option.value.minute === (this.model.dateStart as DateTime).minute
      : false;
  }

  stopPropagation(event: MouseEvent): void {
    event.stopPropagation();
  }

  onChangeDate(value: HseDateModel): void {
    this.writeValue(value);
    this._onChange(value);
    this._onTouched();
    this._trigger.closeMenu();
    this.modelChange.emit(this._value);
  }

  onChangeTime(option: HseSelectOption<DateTime>): void {
    const duration: DurationLike = { hour: option.value.hour, minute: option.value.minute };
    const newDateTimeValue = this._value
      ? { dateStart: (this._value.dateStart as DateTime).startOf('day').plus(duration), dateEnd: null }
      : { dateStart: DateTime.now().startOf('day').plus(duration), dateEnd: null };

    this._selectedOption = option;

    this.writeValue(newDateTimeValue);
    this._onChange(newDateTimeValue);
    this._onTouched();

    this.modelChange.emit(this._value);
  }

  updateSelectMenuProps(): void {
    this.timePickerWidth = this._timePickerEl.nativeElement.offsetWidth;
  }

  private checkTimeLimits(): boolean {
    const startTimeLimitCondition: boolean =
      this.startHour >= this.MIN_START_HOUR && this.startHour <= this.MAX_START_HOUR - 1;
    const endTimeLimitCondition: boolean =
      this.endHour >= this.MIN_START_HOUR + 1 && this.endHour <= this.MAX_START_HOUR;

    return startTimeLimitCondition && endTimeLimitCondition;
  }

  private buildTimeOptions(): void {
    let time: DateTime = DateTime.fromObject({ hour: this.startHour }).startOf('hour');
    let endTime: DateTime = DateTime.fromObject({ hour: this.endHour }).startOf('hour');

    while (time <= endTime) {
      this._options.push({
        label: time.toFormat('T'),
        value: time,
      });

      time = time.plus({ minute: this.stepInMinutes });
    }
  }
}
