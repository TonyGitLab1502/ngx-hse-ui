import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HseRangeDatePickerComponent } from './range-date-picker.component';

describe('HseRangeDatePickerComponent', () => {
  let component: HseRangeDatePickerComponent;
  let fixture: ComponentFixture<HseRangeDatePickerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HseRangeDatePickerComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(HseRangeDatePickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
