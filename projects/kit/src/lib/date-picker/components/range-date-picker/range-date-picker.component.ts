import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  forwardRef,
  HostBinding,
  Input,
  OnInit,
  Output,
  TemplateRef,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { MatMenuTrigger } from '@angular/material/menu';
import { HseDirectionTypes, HseUiSizes } from '../../../@common';
import { UtilsService } from '../../../@utils';
import { HseDateModel, HseDatePickerLocale, HseDateSelectionModes } from '../../date-picker.models';
import { DateTime } from 'luxon';

@Component({
  selector: 'hse-range-date-picker',
  template: `
    <div class="hse-range-date-picker__date-pickers">
      <div class="hse-range-date-picker__date-picker-wrapper" [class.with-label]="!!startDateLabel">
        <ng-container *ngIf="startDateLabel">
          <label class="hse-range-date-picker__label" for="{{ startDateId }}">
            <ng-container hseLabelTemplate [label]="startDateLabel"></ng-container>
          </label>
        </ng-container>
        <div
          class="hse-range-date-picker__date-picker"
          [id]="startDateId"
          [matMenuTriggerFor]="!disabled ? calendarMenu : null"
        >
          <div class="hse-range-date-picker__selected-value-wrapper">
            <ng-container *ngIf="startDatePlaceholder && !model">
              <span class="hse-range-date-picker__placeholder">{{ startDatePlaceholder }}</span>
            </ng-container>
            <ng-container *ngIf="model">
              <span class="hse-range-date-picker__selected-value">
                <ng-container hseLabelTemplate [label]="selectedStartDate"></ng-container>
              </span>
            </ng-container>
          </div>
          <div class="hse-range-date-picker__icons">
            <hse-icon class="hse-range-date-picker__icon" name="calendar"></hse-icon>
          </div>
        </div>
      </div>
      <hse-icon class="hse-range-date-picker__separate-icon" name="line"></hse-icon>
      <div class="hse-range-date-picker__date-picker-wrapper" [class.with-label]="!!endDateLabel">
        <ng-container *ngIf="endDateLabel">
          <label class="hse-range-date-picker__label" for="{{ endDateId }}">
            <ng-container hseLabelTemplate [label]="endDateLabel"></ng-container>
          </label>
        </ng-container>
        <div
          class="hse-range-date-picker__date-picker"
          [id]="endDateId"
          [matMenuTriggerFor]="!disabled ? calendarMenu : null"
        >
          <div class="hse-range-date-picker__selected-value-wrapper">
            <ng-container *ngIf="endDatePlaceholder && !model">
              <span class="hse-range-date-picker__placeholder">{{ endDatePlaceholder }}</span>
            </ng-container>
            <ng-container *ngIf="model">
              <span class="hse-range-date-picker__selected-value">
                <ng-container hseLabelTemplate [label]="selectedEndDate"></ng-container>
              </span>
            </ng-container>
          </div>
          <div class="hse-range-date-picker__icons">
            <hse-icon class="hse-range-date-picker__icon" name="calendar"></hse-icon>
          </div>
        </div>
      </div>
    </div>
    <mat-menu class="hse-date-picker__menu-override" #calendarMenu="matMenu" [overlapTrigger]="false">
      <div class="hse-date-picker__menu-content" (click)="stopPropagation($event)">
        <hse-calendar
          [dateStart]="model?.dateStart || null"
          [dateEnd]="model?.dateEnd || null"
          [selectionMode]="mode"
          (onChange)="onChange($event)"
        ></hse-calendar>
      </div>
    </mat-menu>
  `,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => HseRangeDatePickerComponent),
      multi: true,
    },
  ],
})
export class HseRangeDatePickerComponent implements ControlValueAccessor, OnInit {
  @HostBinding('class.hse-range-date-picker') private readonly baseCss: boolean = true;

  @Input() direction: HseDirectionTypes = HseDirectionTypes.ROW;
  @Input() disabled: boolean = false;
  @Input() endDateLabel: string | TemplateRef<void> = '';
  @Input() endDatePlaceholder: string = '';
  @Input() hasError: boolean = false;
  @Input() locale: HseDatePickerLocale = 'ru';
  @Input() maxDate: DateTime | null = null;
  @Input() maxYear: number = 2100;
  @Input() minDate: DateTime | null = null;
  @Input() minYear: number = 2000;
  @Input() mode: Exclude<HseDateSelectionModes, HseDateSelectionModes.SINGLE> = HseDateSelectionModes.RANGE;
  @Input()
  set model(value: HseDateModel | null) {
    this._value = value;
    this._onChange(value);
  }
  @Input() size: HseUiSizes = HseUiSizes.MEDIUM;
  @Input() startDateLabel: string | TemplateRef<void> = '';
  @Input() startDatePlaceholder: string = '';

  @Output() modelChange: EventEmitter<HseDateModel | null> = new EventEmitter<HseDateModel | null>();
  @ViewChild(MatMenuTrigger, { static: false }) private _trigger!: MatMenuTrigger;
  @HostBinding('class') get additionalClasses(): string {
    return `
      hse-range-date-picker_${this._utilsService.getSizeClass(this.size)}
      ${this.direction === HseDirectionTypes.ROW ? 'hse-range-date-picker_row' : 'hse-range-date-picker_column'}
      ${this.disabled ? 'hse-range-date-picker_disabled' : ''}
      ${this.hasError ? 'hse-range-date-picker_invalid' : ''}
    `;
  }

  get model(): HseDateModel | null {
    return this._value;
  }

  get selectedStartDate(): string {
    return this._value
      ? (this._value.dateStart as DateTime)
          .startOf('day')
          .toFormat(this.locale === 'ru' ? this.DATE_FORMAT_RU : this.DATE_FORMAT_EN)
      : '';
  }

  get selectedEndDate(): string {
    return this._value
      ? (this._value.dateEnd as DateTime)
          .startOf('day')
          .toFormat(this.locale === 'ru' ? this.DATE_FORMAT_RU : this.DATE_FORMAT_EN)
      : '';
  }

  private readonly DATE_FORMAT_EN: string = 'yyyy-LL-dd';
  private readonly DATE_FORMAT_RU: string = 'dd.LL.yyyy';
  private _value: HseDateModel | null = null;
  private _onChange: (value: HseDateModel | null) => void = () => {};
  private _onTouched: () => void = () => {};

  public readonly startDateId: string = '';
  public readonly endDateId: string = '';

  constructor(private readonly _utilsService: UtilsService) {
    this.startDateId = this._utilsService.generateUniqId();
    this.endDateId = this._utilsService.generateUniqId();
  }

  ngOnInit(): void {}

  writeValue(value: HseDateModel | null): void {
    this._value = value;
  }

  registerOnChange(fn: any): void {
    this._onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this._onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  onChange(value: HseDateModel | null): void {
    this.writeValue(value);
    this._onChange(value);
    this._onTouched();
    this._trigger.closeMenu();
    this.modelChange.emit(this._value);
  }

  stopPropagation(event: MouseEvent): void {
    event.stopPropagation();
  }
}
