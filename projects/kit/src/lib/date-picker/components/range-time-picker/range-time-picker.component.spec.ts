import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HseRangeTimePickerComponent } from './range-time-picker.component';

describe('HseRangeTimePickerComponent', () => {
  let component: HseRangeTimePickerComponent;
  let fixture: ComponentFixture<HseRangeTimePickerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HseRangeTimePickerComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(HseRangeTimePickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
