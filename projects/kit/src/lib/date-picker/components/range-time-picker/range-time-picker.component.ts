import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  HostBinding,
  HostListener,
  Input,
  OnInit,
  Output,
  TemplateRef,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { HseDirectionTypes, HseUiSizes } from '../../../@common';
import { UtilsService } from '../../../@utils';
import { HseSelectOption } from '../../../select';
import { HseDateModel } from '../../date-picker.models';
import { DateTime } from 'luxon';

@Component({
  selector: 'hse-range-time-picker',
  template: `
    <div class="hse-range-time-picker__time-picker-wrapper" [class.with-label]="!!startTimeLabel">
      <ng-container *ngIf="startTimeLabel">
        <label class="hse-range-date-picker__label" for="{{ startTimeId }}">
          <ng-container hseLabelTemplate [label]="startTimeLabel"></ng-container>
        </label>
      </ng-container>
      <div
        #startTimePicker
        class="hse-range-time-picker__time-picker"
        [id]="startTimeId"
        [matMenuTriggerFor]="!disabled ? optionListForStart : null"
        (menuOpened)="scrollToSelectedValues(true)"
        (menuClosed)="onChangeValue(true)"
      >
        <div class="hse-range-time-picker__selected-value-wrapper">
          <ng-container *ngIf="startTimePlaceholder && !model">
            <span class="hse-range-time-picker__placeholder">{{ startTimePlaceholder }}</span>
          </ng-container>
          <ng-container *ngIf="model">
            <span class="hse-range-time-picker__selected-value">
              <ng-container hseLabelTemplate [label]="selectedStartTime"></ng-container>
            </span>
          </ng-container>
        </div>
        <div class="hse-range-time-picker__icons">
          <hse-icon class="hse-range-time-picker__icon" name="time"></hse-icon>
        </div>
      </div>
      <mat-menu #optionListForStart="matMenu" class="hse-range-time-picker__menu-override" [overlapTrigger]="false">
        <div
          class="hse-range-time-picker__menu-content"
          [style.width.px]="startTimePickerWidth"
          (click)="stopPropagation($event)"
        >
          <ul class="hse-range-time-picker__option-list" [style.width.px]="startTimePickerColumnWidth">
            <ng-container *ngFor="let option of startPickerHours">
              <li class="hse-range-time-picker__option-wrapper">
                <button
                  class="hse-range-time-picker__option"
                  [class.selected]="option.selected"
                  [disabled]="option.disabled"
                  (click)="selectHour(option, true)"
                >
                  {{ option.label }}
                </button>
              </li>
            </ng-container>
          </ul>
          <div class="hse-range-time-picker__divider"></div>
          <ul class="hse-range-time-picker__option-list" [style.width.px]="startTimePickerColumnWidth">
            <ng-container *ngFor="let option of startPickerMinutes">
              <li class="hse-range-time-picker__option-wrapper">
                <button
                  class="hse-range-time-picker__option"
                  [class.selected]="option.selected"
                  [disabled]="option.disabled"
                  (click)="selectMinute(option, true)"
                >
                  {{ option.label }}
                </button>
              </li>
            </ng-container>
          </ul>
        </div>
      </mat-menu>
    </div>
    <hse-icon class="hse-range-time-picker__separate-icon" name="line"></hse-icon>
    <div class="hse-range-time-picker__time-picker-wrapper" [class.with-label]="!!endTimeLabel">
      <ng-container *ngIf="endTimeLabel">
        <label class="hse-range-date-picker__label" for="{{ endTimeId }}">
          <ng-container hseLabelTemplate [label]="endTimeLabel"></ng-container>
        </label>
      </ng-container>
      <div
        #endTimePicker
        class="hse-range-time-picker__time-picker"
        [id]="endTimeId"
        [matMenuTriggerFor]="!disabled ? optionListForEnd : null"
        (menuOpened)="scrollToSelectedValues()"
        (menuClosed)="onChangeValue()"
      >
        <div class="hse-range-time-picker__selected-value-wrapper">
          <ng-container *ngIf="endTimePlaceholder && !model">
            <span class="hse-range-time-picker__placeholder">{{ endTimePlaceholder }}</span>
          </ng-container>
          <ng-container *ngIf="model">
            <span class="hse-range-time-picker__selected-value">
              <ng-container hseLabelTemplate [label]="selectedEndTime"></ng-container>
            </span>
          </ng-container>
        </div>
        <div class="hse-range-time-picker__icons">
          <hse-icon class="hse-range-time-picker__icon" name="time"></hse-icon>
        </div>
      </div>
      <mat-menu #optionListForEnd="matMenu" class="hse-range-time-picker__menu-override" [overlapTrigger]="false">
        <div
          class="hse-range-time-picker__menu-content"
          [style.width.px]="endTimePickerWidth"
          (click)="stopPropagation($event)"
        >
          <ul class="hse-range-time-picker__option-list" [style.width.px]="endTimePickerColumnWidth">
            <ng-container *ngFor="let option of endPickerHours">
              <li class="hse-range-time-picker__option-wrapper">
                <button
                  class="hse-range-time-picker__option"
                  [class.selected]="option.selected"
                  [disabled]="option.disabled"
                  (click)="selectHour(option)"
                >
                  {{ option.label }}
                </button>
              </li>
            </ng-container>
          </ul>
          <div class="hse-range-time-picker__divider"></div>
          <ul class="hse-range-time-picker__option-list" [style.width.px]="endTimePickerColumnWidth">
            <ng-container *ngFor="let option of endPickerMinutes">
              <li class="hse-range-time-picker__option-wrapper">
                <button
                  class="hse-range-time-picker__option"
                  [class.selected]="option.selected"
                  [disabled]="option.disabled"
                  (click)="selectMinute(option)"
                >
                  {{ option.label }}
                </button>
              </li>
            </ng-container>
          </ul>
        </div>
      </mat-menu>
    </div>
  `,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => HseRangeTimePickerComponent),
      multi: true,
    },
  ],
})
export class HseRangeTimePickerComponent implements AfterViewInit, ControlValueAccessor, OnInit {
  @HostBinding('class.hse-range-time-picker') private readonly baseCss: boolean = true;

  @Input() direction: HseDirectionTypes = HseDirectionTypes.ROW;
  @Input() disabled: boolean = false;
  @Input() endHour: number = 23;
  @Input() endTimeLabel: string | TemplateRef<void> = '';
  @Input() endTimePlaceholder: string = '';
  @Input() hasError: boolean = false;
  @Input()
  set model(value: HseDateModel | null) {
    this._value = value;
    this._onChange(value);
  }
  @Input() size: HseUiSizes = HseUiSizes.MEDIUM;
  @Input() startHour: number = 0;
  @Input() startTimeLabel: string | TemplateRef<void> = '';
  @Input() startTimePlaceholder: string = '';

  @Output() modelChange: EventEmitter<HseDateModel | null> = new EventEmitter<HseDateModel | null>();
  @ViewChild('startTimePicker', { static: true })
  private readonly _startTimePicker: ElementRef<HTMLElement> = new ElementRef<HTMLElement>(this._elRef.nativeElement);
  @ViewChild('endTimePicker', { static: true })
  private readonly _endTimePicker: ElementRef<HTMLElement> = new ElementRef<HTMLElement>(this._elRef.nativeElement);

  @HostBinding('class') get additionalClasses(): string {
    return `
      hse-range-time-picker_${this.direction}
      hse-range-time-picker_${this._utilsService.getSizeClass(this.size)}
      ${this.disabled ? 'hse-range-time-picker_disabled' : ''}
      ${this.hasError ? 'hse-range-time-picker_invalid' : ''}
    `;
  }

  @HostListener('window:resize')
  onResize() {
    this.updateSelectMenuProps();
  }

  get model(): HseDateModel | null {
    return this._value;
  }

  get selectedStartTime(): string {
    return this._value ? (this._value.dateStart as DateTime).toFormat(this.TIME_FORMAT) : '';
  }

  get selectedEndTime(): string {
    return this._value ? (this._value.dateEnd as DateTime).toFormat(this.TIME_FORMAT) : '';
  }

  get startPickerHours(): HseSelectOption<DateTime>[] {
    return this._startPickerHours;
  }

  get startPickerMinutes(): HseSelectOption<DateTime>[] {
    return this._startPickerMinutes;
  }

  get endPickerHours(): HseSelectOption<DateTime>[] {
    return this._endPickerHours;
  }

  get endPickerMinutes(): HseSelectOption<DateTime>[] {
    return this._endPickerMinutes;
  }

  private readonly OPTION_LIST_WRAPPER_CLASSNAME: string = 'hse-range-time-picker__option-list';
  private readonly MIN_START_HOUR: number = 0;
  private readonly MAX_START_HOUR: number = 23;
  private readonly MINUTES_IN_HOUR_FOR_DISPLAY: number = 59;
  private readonly TIME_FORMAT: string = 'T';

  private _startPickerHours: HseSelectOption<DateTime>[] = [];
  private _endPickerHours: HseSelectOption<DateTime>[] = [];
  private _startPickerMinutes: HseSelectOption<DateTime>[] = [];
  private _endPickerMinutes: HseSelectOption<DateTime>[] = [];
  private _value: HseDateModel | null = null;
  private _onChange: (value: HseDateModel | null) => void = () => {};
  private _onTouched: () => void = () => {};

  public endTimeId: string = '';
  public endTimePickerWidth: number = 0;
  public endTimePickerColumnWidth: number = 0;
  public startTimeId: string = '';
  public startTimePickerWidth: number = 0;
  public startTimePickerColumnWidth: number = 0;

  constructor(private readonly _elRef: ElementRef, private readonly _utilsService: UtilsService) {
    this.endTimeId = this._utilsService.generateUniqId();
    this.startTimeId = this._utilsService.generateUniqId();
  }

  ngOnInit(): void {
    if (this.checkTimeLimits()) {
      this.buildTimeOptions();
    }
  }

  ngAfterViewInit(): void {
    this.updateSelectMenuProps();
  }

  writeValue(value: HseDateModel | null): void {
    this._value = value;
  }

  registerOnChange(fn: any): void {
    this._onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this._onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  stopPropagation(event: MouseEvent): void {
    event.stopPropagation();
  }

  onChangeValue(isStartPicker: boolean = false): void {
    const hoursCollection = isStartPicker ? this._startPickerHours : this._endPickerHours;
    const minutesCollection = isStartPicker ? this._startPickerMinutes : this._endPickerMinutes;
    const selectedHour = hoursCollection.find((hour: HseSelectOption<DateTime>) => hour.selected);
    const selectedMinute = minutesCollection.find((minute: HseSelectOption<DateTime>) => minute.selected);

    if (selectedHour && selectedMinute) {
      const selectedTime: DateTime = DateTime.fromObject({
        hour: selectedHour.value.hour,
        minute: selectedMinute.value.minute,
      });
      const value = this._value
        ? this.getValidTimeRange(selectedTime, isStartPicker)
        : { dateStart: selectedTime, dateEnd: selectedTime };

      this.writeValue(value);
      this._onChange(value);
      this._onTouched();

      this.updateHoursOptions(this._value as HseDateModel);
      this.updateMinutesOptions(this._value as HseDateModel);

      this.modelChange.emit(this._value);
    }
  }

  getValidTimeRange(selectedTime: DateTime, isStartPicker: boolean = false): HseDateModel {
    const { hour: startHour, minute: startMinute } = this._value?.dateStart as DateTime;
    const { hour: endHour, minute: endMinute } = this._value?.dateEnd as DateTime;

    if (
      isStartPicker &&
      (selectedTime.hour > endHour || (selectedTime.hour === endHour && selectedTime.minute > endMinute))
    ) {
      return { dateStart: this._value?.dateEnd as DateTime, dateEnd: selectedTime };
    } else if (
      !isStartPicker &&
      (selectedTime.hour < startHour || (selectedTime.hour === startHour && selectedTime.minute < startMinute))
    ) {
      return { dateStart: selectedTime, dateEnd: this._value?.dateStart as DateTime };
    } else {
      return isStartPicker
        ? { ...(this._value as HseDateModel), dateStart: selectedTime }
        : { ...(this._value as HseDateModel), dateEnd: selectedTime };
    }
  }

  updateHoursOptions(range: HseDateModel): void {
    // т.к. метод hasSame возвращает true только при полном совпадении всех единиц времени,
    // то тут сравниваем только по значению часов
    this._startPickerHours.forEach(
      (hour: HseSelectOption<DateTime>) => (hour.selected = hour.value.hour === (range.dateStart as DateTime).hour)
    );

    this._endPickerHours.forEach(
      (hour: HseSelectOption<DateTime>) => (hour.selected = hour.value.hour === (range.dateEnd as DateTime).hour)
    );
  }

  updateMinutesOptions(range: HseDateModel): void {
    // т.к. метод hasSame возвращает true только при полном совпадении всех единиц времени,
    // то тут сравниваем только по значению минут
    this._startPickerMinutes.forEach(
      (minute: HseSelectOption<DateTime>) =>
        (minute.selected = minute.value.minute === (range.dateStart as DateTime).minute)
    );

    this._endPickerMinutes.forEach(
      (minute: HseSelectOption<DateTime>) =>
        (minute.selected = minute.value.minute === (range.dateEnd as DateTime).minute)
    );
  }

  selectHour(option: HseSelectOption<DateTime>, isStartPicker: boolean = false): void {
    const hoursCollection = isStartPicker ? this._startPickerHours : this._endPickerHours;
    const minutesCollection = isStartPicker ? this._startPickerMinutes : this._endPickerMinutes;
    const needScrollMinutes = minutesCollection.every((minute: HseSelectOption<DateTime>) => !minute.selected);

    hoursCollection.forEach(
      (hourOption: HseSelectOption<DateTime>) =>
        // т.к. метод hasSame возвращает true только при полном совпадении всех единиц времени,
        // то тут сравниваем только по значению часов
        (hourOption.selected = hourOption.value.hour === option.value.hour)
    );

    if (needScrollMinutes) {
      const firstAvailableMinute = minutesCollection.find((option: HseSelectOption<DateTime>) => !option.disabled);

      if (firstAvailableMinute) {
        firstAvailableMinute.selected = true;
      }
    }

    setTimeout(() => {
      this.scrollToSelectedOption();

      if (needScrollMinutes) {
        this.scrollToSelectedOption(true);
      }
    }, 0);
  }

  selectMinute(option: HseSelectOption<DateTime>, isStartPicker: boolean = false): void {
    const hoursCollection = isStartPicker ? this._startPickerHours : this._endPickerHours;
    const minutesCollection = isStartPicker ? this._startPickerMinutes : this._endPickerMinutes;
    const needScrollHours = hoursCollection.every((hour: HseSelectOption<DateTime>) => !hour.selected);

    minutesCollection.forEach(
      (minuteOption: HseSelectOption<DateTime>) =>
        // т.к. метод hasSame возвращает true только при полном совпадении всех единиц времени,
        // то тут сравниваем только по значению минут
        (minuteOption.selected = minuteOption.value.minute === option.value.minute)
    );

    if (needScrollHours) {
      const firstAvailableHour = hoursCollection.find((option: HseSelectOption<DateTime>) => !option.disabled);

      if (firstAvailableHour) {
        firstAvailableHour.selected = true;
      }
    }

    setTimeout(() => {
      this.scrollToSelectedOption(true);

      if (needScrollHours) {
        this.scrollToSelectedOption();
      }
    }, 0);
  }

  scrollToSelectedOption(isMinutes: boolean = false, behavior: 'auto' | 'smooth' = 'smooth'): void {
    const wrapper: Element = document.getElementsByClassName(this.OPTION_LIST_WRAPPER_CLASSNAME)[isMinutes ? 1 : 0];
    const selectedElement = wrapper.querySelector<HTMLButtonElement>('.selected') as HTMLButtonElement;

    wrapper.scrollTo({ top: selectedElement.offsetTop, behavior });
  }

  scrollToSelectedValues(isStartPicker: boolean = false): void {
    const hoursCollection = isStartPicker ? this._startPickerHours : this._endPickerHours;
    const minutesCollection = isStartPicker ? this._startPickerMinutes : this._endPickerMinutes;

    if (hoursCollection.some((hour: HseSelectOption<DateTime>) => hour.selected)) {
      this.scrollToSelectedOption(false, 'auto');
    }

    if (minutesCollection.some((minute: HseSelectOption<DateTime>) => minute.selected)) {
      this.scrollToSelectedOption(true, 'auto');
    }
  }

  private checkTimeLimits(): boolean {
    const startTimeLimitCondition: boolean =
      this.startHour >= this.MIN_START_HOUR && this.startHour <= this.MAX_START_HOUR - 1;
    const endTimeLimitCondition: boolean =
      this.endHour >= this.MIN_START_HOUR + 1 && this.endHour <= this.MAX_START_HOUR;

    return startTimeLimitCondition && endTimeLimitCondition;
  }

  private buildTimeOptions(): void {
    this._startPickerHours = this.buildHoursOptions(true);
    this._endPickerHours = this.buildHoursOptions();
    this._startPickerMinutes = this.buildMinutesOptions(true);
    this._endPickerMinutes = this.buildMinutesOptions();
  }

  private buildHoursOptions(isStart: boolean = false): HseSelectOption<DateTime>[] {
    const startEnabledHour: DateTime = DateTime.fromObject({ hour: this.startHour });
    const endEnabledHour: DateTime = DateTime.fromObject({ hour: this.endHour });

    return this._utilsService.createRange(this.MIN_START_HOUR, this.MAX_START_HOUR).map((hour: number) => {
      const time: DateTime = DateTime.fromObject({ hour });
      const selected = this._value
        ? time.hour === (this._value[isStart ? 'dateStart' : 'dateEnd'] as DateTime).hour
        : false;

      return {
        label: time.toFormat('HH'),
        value: time,
        disabled: time < startEnabledHour || time > endEnabledHour,
        selected,
      };
    });
  }

  private buildMinutesOptions(isStart: boolean = false): HseSelectOption<DateTime>[] {
    return this._utilsService.createRange(0, this.MINUTES_IN_HOUR_FOR_DISPLAY).map((minute: number) => {
      const time: DateTime = DateTime.fromObject({ minute });
      const selected = this._value
        ? time.minute === (this._value[isStart ? 'dateStart' : 'dateEnd'] as DateTime).minute
        : false;

      return {
        label: time.toFormat('mm'),
        value: time,
        selected,
      };
    });
  }

  private updateSelectMenuProps(): void {
    this.startTimePickerWidth = this._startTimePicker.nativeElement.offsetWidth;
    this.endTimePickerWidth = this._endTimePicker.nativeElement.offsetWidth;
    this.startTimePickerColumnWidth = this.startTimePickerWidth / 2 - 2;
    this.endTimePickerColumnWidth = this.endTimePickerWidth / 2 - 2;
  }
}
