import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HseTimePickerComponent } from './time-picker.component';

describe('HseTimePickerComponent', () => {
  let component: HseTimePickerComponent;
  let fixture: ComponentFixture<HseTimePickerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HseTimePickerComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(HseTimePickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
