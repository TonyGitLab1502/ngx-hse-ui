import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  HostBinding,
  HostListener,
  Input,
  OnInit,
  Output,
  TemplateRef,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { HseUiSizes } from '../../../@common';
import { UtilsService } from '../../../@utils';
import { HseSelectOption } from '../../../select';
import { HseDateModel } from '../../date-picker.models';
import { DateTime } from 'luxon';

@Component({
  selector: 'hse-time-picker',
  template: `
    <ng-container *ngIf="label">
      <label class="hse-time-picker__label" for="{{ timeSelectId }}">{{ label }}</label>
    </ng-container>
    <div
      #timePickerEl
      class="hse-time-picker__select"
      [id]="timeSelectId"
      [matMenuTriggerFor]="!disabled ? optionList : null"
    >
      <div class="hse-time-picker__selected-value-wrapper">
        <ng-container *ngIf="placeholder && !model">
          <span class="hse-time-picker__placeholder">{{ placeholder }}</span>
        </ng-container>
        <ng-container *ngIf="model">
          <span class="hse-time-picker__selected-value">
            <ng-container hseLabelTemplate [label]="model.dateStart?.toFormat('T') || ''"></ng-container>
          </span>
        </ng-container>
      </div>
      <div class="hse-time-picker__icons">
        <ng-container *ngIf="clearable && model">
          <hse-icon class="hse-time-picker__icon clear-icon" name="clear" (click)="onClear($event)"></hse-icon>
        </ng-container>
        <hse-icon class="hse-time-picker__icon" name="time"></hse-icon>
      </div>
    </div>
    <mat-menu class="hse-time-picker__menu-override" #optionList="matMenu" [overlapTrigger]="false">
      <div [style.width.px]="timePickerWidth">
        <div class="hse-time-picker__option-list">
          <ng-container *ngFor="let option of options">
            <button
              class="hse-time-picker__option"
              [class.selected]="isSelected(option)"
              [disabled]="option.disabled"
              (click)="onSelect(option)"
            >
              <ng-container hseLabelTemplate [label]="option.label"></ng-container>
            </button>
          </ng-container>
        </div>
      </div>
    </mat-menu>
  `,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => HseTimePickerComponent),
      multi: true,
    },
  ],
})
export class HseTimePickerComponent implements AfterViewInit, ControlValueAccessor, OnInit {
  @HostBinding('class.hse-time-picker') private readonly baseCss: boolean = true;

  @Input() clearable: boolean = false;
  @Input() disabled: boolean = false;
  @Input() endHour: number = 23;
  @Input() hasError: boolean = false;
  @Input() label: string | TemplateRef<void> = '';
  @Input()
  set model(value: HseDateModel | null) {
    this._value = value;
    this._onChange(value);
  }
  @Input() placeholder: string = '';
  @Input() size: HseUiSizes = HseUiSizes.MEDIUM;
  @Input() startHour: number = 0;
  @Input() stepInMinutes: number = 60;

  @Output()
  modelChange: EventEmitter<HseDateModel | null> = new EventEmitter<HseDateModel | null>();

  @ViewChild('timePickerEl', { static: true })
  private readonly _timePickerEl: ElementRef<HTMLElement> = new ElementRef<HTMLElement>(this._elRef.nativeElement);

  @HostBinding('class') get additionalClasses(): string {
    return `
      hse-time-picker_${this._utilsService.getSizeClass(this.size)}
      ${this.disabled ? 'hse-time-picker_disabled' : ''}
      ${this.hasError ? 'hse-time-picker_invalid' : ''}
    `;
  }

  @HostListener('window:resize')
  onResize() {
    this.updateSelectMenuProps();
  }

  get model(): HseDateModel | null {
    return this._value;
  }

  get options(): HseSelectOption<DateTime>[] {
    return this._options;
  }

  get selectedOption(): HseSelectOption<DateTime> | null {
    return this._selectedOption;
  }

  private readonly MIN_START_HOUR: number = 0;
  private readonly MAX_START_HOUR: number = 23;
  private _options: HseSelectOption<DateTime>[] = [];
  private _selectedOption: HseSelectOption<DateTime> | null = null;
  private _value: HseDateModel | null = null;
  private _onChange: (value: HseDateModel | null) => void = () => {};
  private _onTouched: () => void = () => {};

  public timePickerWidth: number = 0;
  public timeSelectId: string = '';

  constructor(private readonly _elRef: ElementRef, private readonly _utilsService: UtilsService) {
    this.timeSelectId = this._utilsService.generateUniqId();
  }

  ngOnInit(): void {
    if (this.checkTimeLimits()) {
      this.buildTimeOptions();
    }
  }

  ngAfterViewInit(): void {
    this.updateSelectMenuProps();
  }

  writeValue(value: HseDateModel | null): void {
    this._value = value;
  }

  registerOnChange(fn: any): void {
    this._onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this._onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  onSelect(option: HseSelectOption<any>): void {
    this._selectedOption = option;

    const selectedTime: HseDateModel = {
      dateStart: option.value,
      dateEnd: null,
    };

    this.writeValue(selectedTime);
    this._onChange(selectedTime);
    this._onTouched();
    this.modelChange.emit(this._value);
  }

  onClear(event: MouseEvent): void {
    event.stopPropagation();

    if (!this.disabled) {
      this.writeValue(null);
      this._onChange(null);
      this._onTouched();
      this.modelChange.emit(this._value);
    }
  }

  isSelected(option: HseSelectOption<DateTime>): boolean {
    return this.model
      ? option.value.hour === (this.model.dateStart as DateTime).hour &&
          option.value.minute === (this.model.dateStart as DateTime).minute
      : false;
  }

  updateSelectMenuProps(): void {
    this.timePickerWidth = this._timePickerEl.nativeElement.offsetWidth;
  }

  private checkTimeLimits(): boolean {
    const startTimeLimitCondition: boolean =
      this.startHour >= this.MIN_START_HOUR && this.startHour <= this.MAX_START_HOUR - 1;
    const endTimeLimitCondition: boolean =
      this.endHour >= this.MIN_START_HOUR + 1 && this.endHour <= this.MAX_START_HOUR;

    return startTimeLimitCondition && endTimeLimitCondition;
  }

  private buildTimeOptions(): void {
    let time: DateTime = DateTime.fromObject({ hour: this.startHour }).startOf('hour');
    let endTime: DateTime = DateTime.fromObject({ hour: this.endHour }).startOf('hour');

    while (time <= endTime) {
      this._options.push({
        label: time.toFormat('T'),
        value: time,
      });

      time = time.plus({ minute: this.stepInMinutes });
    }
  }
}
