import { DateTime } from 'luxon';

export type HseCalendarItem = { label: string; value: number };
export type HseCalendarWeeks = Array<HseCalendarWeekday[]>;
export type HseCalendarWeekday = {
  classNames?: string;
  date: DateTime;
  isCurrentMonth: boolean;
  isDisabled: boolean;
  isToday: boolean;
  label: number;
};
export type HseDateModel = { dateStart: DateTime | null; dateEnd: DateTime | null };
export type HseDatePickerLocale = 'ru' | 'en';

export enum HseCalendarSelectionModes {
  DAY = 'day',
  MONTH = 'month',
  YEAR = 'year',
}

export enum HseDateSelectionModes {
  RANGE = 'range',
  SINGLE = 'single',
  WEEK = 'week',
}

export enum HseCalendarDateRangeTypes {
  START = 'start',
  END = 'end',
}
