import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatMenuModule } from '@angular/material/menu';
import { HseCommonUiModule } from '../@common';
import { HseButtonModule } from '../button';
import { HseIconModule } from '../icon';
import { HseSelectModule } from '../select';
import { HseCalendarComponent } from './components/calendar/calendar.component';
import { HseDatePickerComponent } from './components/date-picker/date-picker.component';
import { HseDateTimePickerComponent } from './components/date-time-picker/date-time-picker.component';
import { HseRangeDatePickerComponent } from './components/range-date-picker/range-date-picker.component';
import { HseRangeTimePickerComponent } from './components/range-time-picker/range-time-picker.component';
import { HseTimePickerComponent } from './components/time-picker/time-picker.component';

const declarations: any[] = [
  HseCalendarComponent,
  HseDatePickerComponent,
  HseDateTimePickerComponent,
  HseRangeDatePickerComponent,
  HseRangeTimePickerComponent,
  HseTimePickerComponent,
];

@NgModule({
  declarations,
  imports: [
    CommonModule,
    FormsModule,
    HseButtonModule,
    HseCommonUiModule,
    HseIconModule,
    HseSelectModule,
    MatMenuModule,
    ReactiveFormsModule,
  ],
  exports: [...declarations],
})
export class HseDatePickerModule {}
