import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  HostBinding,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { MAT_RIPPLE_GLOBAL_OPTIONS, RippleGlobalOptions } from '@angular/material/core';
import { fromEvent, Subscription } from 'rxjs';
import { UtilsService } from '../../../@utils';
import { HseUiSizes } from '../../../@common';

const RIPPLE_DURATION = 700;
const globalRippleConfig: RippleGlobalOptions = {
  disabled: false,
  animation: {
    enterDuration: RIPPLE_DURATION,
    exitDuration: 0
  }
};

export interface FileInputItem {
  file?: File,
  id: any,
  uploadProgress: number,
  size: string,
  type: string,
  name: string,
  url?: string,
  uploaded?: boolean,
  uploading?: boolean,
}

@Component({
  selector: 'hse-file-input',
  template: `
    <div class="hse-file-input__wrapper"
         (drop)="dropFile($event)"
         (dragenter)="dragEnter($event)"
         (dragleave)="dragLeave($event)"
         (dragover)="dragOver($event)">

      <!-- BUTTON OR DROPZONE -->
      <div *ngIf="model.length < maxFilesCount" class="hse-file-input__wrapper__dropzone"
           [class.drop-hover]="isDropHover" [class.dropzone]="isDropzone" [class.no-bg]="isAnimated"
           matRipple [matRippleColor]="getRippleColor()"
           (click)="selectFile()">
        <hse-icon class="attach-icon" name="attachFile"></hse-icon>
        <span class="hse-file-input__wrapper__dropzone__label">{{ getInputLabel() }}</span>
      </div>
      <!-- /BUTTON OR DROPZONE -->

      <!-- HINT MESSAGES -->
      <div *ngIf="hint" class="hse-file-input__wrapper__hint">{{ hint }}</div>
      <!-- /HINT MESSAGES -->

      <!-- SELECTED FILES -->
      <div class="hse-file-input__wrapper__list" *ngIf="model.length > 0">
        <div class="hse-file-input__wrapper__list__item default-card"
             *ngFor="let item of model" [class.multiple]="isMultiple">
          <hse-icon *ngIf="!isMultiple" class="attach-icon" name="attachFile"></hse-icon>
          <hse-icon *ngIf="isMultiple" class="attach-icon file-icon" name="file"></hse-icon>

          <div class="hse-file-input__wrapper__list__item__label-wrapper" *ngIf="!item.uploading">
            <div class="default-card__label">
              <span class="hse-file-input__wrapper__list__item__name">{{ item.name }}</span>
              <span class="hse-file-input__wrapper__list__item__type">{{ item.type }}</span>
            </div>
            <div class="hse-file-input__wrapper__list__item__size default-card__label">{{item.size}}</div>
          </div>

          <!-- PROGRESS BAR -->
          <div *ngIf="item.uploading" class="progress-bar-wrapper">
            <div class="progress-bar-wrapper__title">Загрузка...</div>
            <hse-progress-bar [progress]="item.uploadProgress"></hse-progress-bar>
          </div>
          <!-- PROGRESS BAR -->

          <!-- BUTTONS -->
          <hse-icon *ngIf="item.url" class="remove-icon" name="download" (click)="downloadFile(item)"></hse-icon>
          <hse-icon class="remove-icon" (click)="removeFile(item)" name="clear"></hse-icon>
          <!-- /BUTTONS -->
        </div>
      </div>
      <!-- /SELECTED FILES -->

      <!-- INVISIBLE FILE INPUT  -->
      <input
        #inputElement
        [hidden]="true"
        type="file"
        [multiple]="isMultiple"
        [accept]="accept"
        (change)="changeFile($event)"
      >
      <!-- /INVISIBLE FILE INPUT -->
    </div>
  `,
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => HseFileInputComponent),
      multi: true,
    },
    {provide: MAT_RIPPLE_GLOBAL_OPTIONS, useValue: globalRippleConfig}
  ],
})
export class HseFileInputComponent implements OnInit, OnDestroy, OnChanges, ControlValueAccessor {
  @HostBinding('class') get additionalClasses() {
    return `
      hse-file-input_${this.utils.getSizeClass(this.size)}
      ${this.hasError ? 'hse-file-input_invalid' : ''}
      ${this.disabled ? 'hse-file-input_disabled' : ''}
    `;
  }

  @ViewChild('inputElement', { static: false }) inputElement!: ElementRef<HTMLInputElement>;

  isDropzone = false;
  isDropHover = false;
  isAnimated = false;
  isMultiple = false;
  dragTimer: any = null;

  @Input() accept = '*';
  @Input() maxFilesCount = 1;
  @Input() disabled: boolean = false;
  @Input() hasError: boolean = false;
  @Input() size: HseUiSizes = HseUiSizes.MEDIUM;
  @Input() hint: string = '';

  @Input()
  set model(value: FileInputItem[]) {
    this._value = value;
    this._onChange(value);
  }

  get model(): FileInputItem[] {
    return this._value;
  }

  @Output() modelChange: EventEmitter<FileInputItem[]> = new EventEmitter<FileInputItem[]>();
  @Output() fileRemoved: EventEmitter<FileInputItem> = new EventEmitter<FileInputItem>();

  private _value: FileInputItem[] = [];
  private dragOverSubscription!: Subscription;
  private dragLeaveSubscription!: Subscription;

  private _onChange: (value: FileInputItem[]) => void = () => {};
  private _onTouched: () => void = () => {};

  constructor(private cdr: ChangeDetectorRef, private utils: UtilsService) {}

  writeValue(value: FileInputItem[]) {
    this._value = value;
  }

  registerOnChange(fn: any): void {
    this._onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this._onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }


  ngOnInit() {
    this.setIsMultiple();

    this.dragOverSubscription = fromEvent<DragEvent>(document.body, 'dragover')
      .subscribe((event) => {
        if (this.disabled) {
          return;
        }

        if (event.dataTransfer?.types.includes('Files')) {
          this.isDropzone = true;
          clearTimeout(this.dragTimer);
        } else {
          this.isDropzone = false;
        }

        this.cdr.detectChanges();
      });

    this.dragLeaveSubscription = fromEvent<DragEvent>(document.body, 'dragleave')
      .subscribe((event) => {
        if (this.disabled) {
          return;
        }

        if (!event.relatedTarget) {
          clearTimeout(this.dragTimer);
          this.dragTimer = setTimeout(() => {
            this.isDropzone = false;
            this.cdr.detectChanges();
          }, 25)
        }
      });
  }

  ngOnChanges() {
    this.setIsMultiple();
  }

  ngOnDestroy() {
    this.dragOverSubscription?.unsubscribe();
    this.dragLeaveSubscription?.unsubscribe();
  }

  setIsMultiple() {
    this.isMultiple = this.maxFilesCount > 1;
  }

  getInputLabel() {
    const fileLabel = this.isMultiple ? 'файлы' : 'файл';

    return this.isDropzone
      ? `Перетащить ${fileLabel} сюда`
      : `Прикрепить ${fileLabel}`;
  }

  disableBackgroundWhileRipple() {
    this.isAnimated = true;

    setTimeout(()=> {
      this.isAnimated = false;
      this.cdr.detectChanges();
    }, RIPPLE_DURATION);
  }

  changeFile($event: any) {
    try {
      const files = $event.target?.files;

      this.processFiles(Array.from(files));
      $event.target.value = null;
    } catch (e) {
      console.error(e);
    }
  }

  dropFile($event: any) {
    this.preventDefaults($event);
    this.isDropzone = false;
    this.isDropHover = false;

    const files: File[] = [...$event.dataTransfer.items]
      .filter((item) => item.kind === 'file')
      .map((item) => item.getAsFile());

    this.processFiles(files);
  }

  processFiles(files: File[]) {
    const result: FileInputItem[] = files
      .slice(0, this.maxFilesCount)
      .map((file)=> {
        const fileType = '.' + file.name.split('.').splice(-1)[0];

      return {
        file,
        id: null,
        uploaded: false,
        uploadProgress: 0,
        name: file.name.replace(fileType, ''),
        size: this.utils.humanFileSize(file.size),
        type: fileType
      }
    });

    this.model = this.model.concat(result);

    this.modelChange.emit(result);
  }

  dragEnter($event: Event) {
    if (this.disabled) {
      return;
    }

    this.preventDefaults($event);
    this.isDropHover = true;
  }

  dragLeave($event: Event) {
    if (this.disabled) {
      return;
    }

    this.preventDefaults($event);
    this.isDropHover = false;
  }

  dragOver($event: Event) {
    if (this.disabled) {
      return;
    }

    this.preventDefaults($event);
    this.isDropzone = true;
  }

  preventDefaults($event: Event) {
    $event.stopPropagation();
    $event.preventDefault();
  }

  removeFile(item: FileInputItem) {
    if (this.disabled) {
      return;
    }

    this.fileRemoved.emit(item);
    this.utils.removeItem(this.model, item);
  }

  downloadFile(item: FileInputItem) {
    window.open(item.url, '_blank')
  }

  selectFile() {
    if (this.disabled) {
      return;
    }

    this.inputElement.nativeElement.click();
    this.disableBackgroundWhileRipple();
  }

  getRippleColor() {
    if (this.disabled) {
      return ''
    }

    return  this.hasError ? '#ffeeed' : '#f0f5ff';
  }
}
