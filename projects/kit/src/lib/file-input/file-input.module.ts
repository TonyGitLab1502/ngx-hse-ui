import { NgModule } from "@angular/core";
import {HseFileInputComponent} from "./components/file-input/file-input.component";
import {HseIconModule} from "../icon";
import {MatRippleModule} from '@angular/material/core';
import { CommonModule } from "@angular/common";
import { HseProgressBarModule } from "../progress-bar";

const declarations: any[] = [HseFileInputComponent];

@NgModule({
  declarations,
  imports: [
    CommonModule,
    HseIconModule,
    MatRippleModule,
    HseProgressBarModule
  ],
  exports: [...declarations],
})
export class HseFileInputModule {}
