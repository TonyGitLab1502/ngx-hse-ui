import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HseBadgeIconComponent } from './badge-icon.component';

describe('HseBadgeIconComponent', () => {
  let component: HseBadgeIconComponent;
  let fixture: ComponentFixture<HseBadgeIconComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HseBadgeIconComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(HseBadgeIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
