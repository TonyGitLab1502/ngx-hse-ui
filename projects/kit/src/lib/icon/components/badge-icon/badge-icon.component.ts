import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  HostBinding,
  Input,
  OnChanges,
  OnInit,
  Renderer2,
  SimpleChanges,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { HseUiSizes } from '../../../@common';
import { UtilsService } from '../../../@utils';
import { HseIconService } from '../../icon.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'hse-badge-icon',
  template: `
    <div #badgeIconWrapper class="hse-badge-icon__wrapper" [style.backgroundColor]="badgeBackground"></div>
    <ng-container *ngIf="label">
      <p class="hse-badge-icon__label">{{ label }}</p>
    </ng-container>
  `,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HseBadgeIconComponent implements OnInit, OnChanges {
  @HostBinding('class.hse-badge-icon') private readonly baseCss: boolean = true;

  @Input() badgeBackground: string = '';
  @Input() icon: string = '';
  @Input() imageSrc: string = '';
  @Input() label: string = '';
  @Input() size: HseUiSizes = HseUiSizes.MEDIUM;

  @ViewChild('badgeIconWrapper', { static: true })
  private readonly _badgeIconWrapper: ElementRef<HTMLElement> = new ElementRef<HTMLElement>(this._elRef.nativeElement);

  @HostBinding('class') get additionalClasses() {
    return `hse-badge-icon_${this._utilsService.getSizeClass(this.size)} `;
  }

  constructor(
    private readonly _elRef: ElementRef,
    private readonly _iconService: HseIconService,
    private readonly _renderer: Renderer2,
    private readonly _utilsService: UtilsService
  ) {}

  ngOnInit(): void {
    if (this.icon && !this.imageSrc) {
      this.getIconByName(this.icon);
    }

    if (!this.icon && this.imageSrc) {
      this.setImageInWrapper(this.imageSrc);
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    const { icon, image }: SimpleChanges = changes;

    if (icon && !icon.isFirstChange() && !this.imageSrc) {
      this.getIconByName(icon.currentValue);
    }

    if (image && !image.isFirstChange() && !this.icon) {
      this.setImageInWrapper(image.currentValue);
    }
  }

  private getIconByName(name: string): void {
    this._iconService
      .getIconByName(name)
      .pipe(take(1))
      .subscribe((icon: string) => (this._badgeIconWrapper.nativeElement.innerHTML = icon));
  }

  private setImageInWrapper(source: string): void {
    const imgTag = this._renderer.createElement('img');

    this._renderer.setAttribute(imgTag, 'src', source);
    this._renderer.appendChild(this._badgeIconWrapper.nativeElement, imgTag);
  }
}
