import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HseIconComponent } from './icon.component';

describe('HseIconComponent', () => {
  let component: HseIconComponent;
  let fixture: ComponentFixture<HseIconComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HseIconComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(HseIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
