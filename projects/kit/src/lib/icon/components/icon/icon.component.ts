import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  HostBinding,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { take } from 'rxjs/operators';
import { HseUiSizes } from '../../../@common';
import { UtilsService } from '../../../@utils';
import { HseIconService } from '../../icon.service';

@Component({
  selector: 'hse-icon',
  template: ` <div class="hse-icon__wrapper" #iconWrapper></div> `,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HseIconComponent implements OnInit, OnChanges {
  @HostBinding('class.hse-icon') private readonly baseCss: boolean = true;

  @Input() name: string = '';
  @Input() size: HseUiSizes = HseUiSizes.MEDIUM;

  @ViewChild('iconWrapper', { static: true })
  private readonly _iconWrapper: ElementRef<HTMLElement> = new ElementRef<HTMLElement>(this._elRef.nativeElement);

  @HostBinding('class') get additionalClasses() {
    return `hse-icon_${this._utilsService.getSizeClass(this.size)}`;
  }

  constructor(
    private readonly _elRef: ElementRef,
    private readonly _iconService: HseIconService,
    private readonly _utilsService: UtilsService
  ) {}

  ngOnInit(): void {
    if (this.name) {
      this.getIconByName(this.name);
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    const { name }: SimpleChanges = changes;

    if (name && !name.isFirstChange()) {
      this.getIconByName(name.currentValue);
    }
  }

  private getIconByName(name: string): void {
    this._iconService
      .getIconByName(name)
      .pipe(take(1))
      .subscribe((icon: string) => (this._iconWrapper.nativeElement.innerHTML = icon));
  }
}
