import { HseIconStore } from './icon.models';

const icons: HseIconStore = new Map([
  ['add', { url: 'assets/icons/add.svg', data: null }],
  ['attachFile', { url: 'assets/icons/attach_file.svg', data: null }],
  ['calendar', { url: 'assets/icons/calendar.svg', data: null }],
  ['check', { url: 'assets/icons/check.svg', data: null }],
  ['chevronLeft', { url: 'assets/icons/chevron_left.svg', data: null }],
  ['chevronRight', { url: 'assets/icons/chevron_right.svg', data: null }],
  ['clear', { url: 'assets/icons/clear.svg', data: null }],
  ['dateRange', { url: 'assets/icons/date_range.svg', data: null }],
  ['dots', { url: 'assets/icons/dots.svg', data: null }],
  ['download', { url: 'assets/icons/download.svg', data: null }],
  ['error', { url: 'assets/icons/error.svg', data: null }],
  ['expandMore', { url: 'assets/icons/expand_more.svg', data: null }],
  ['eye', { url: 'assets/icons/eye.svg', data: null }],
  ['eyeOff', { url: 'assets/icons/eye_off.svg', data: null }],
  ['file', { url: 'assets/icons/file.svg', data: null }],
  ['fileCopy', { url: 'assets/icons/file_copy.svg', data: null }],
  ['info', { url: 'assets/icons/info.svg', data: null }],
  ['line', { url: 'assets/icons/line.svg', data: null }],
  ['menu', { url: 'assets/icons/menu.svg', data: null }],
  ['notifications', { url: 'assets/icons/notifications.svg', data: null }],
  ['resize', { url: 'assets/icons/resize.svg', data: null }],
  ['search', { url: 'assets/icons/search.svg', data: null }],
  ['sort', { url: 'assets/icons/sort.svg', data: null }],
  ['time', { url: 'assets/icons/time.svg', data: null }],
]);

export default icons;
