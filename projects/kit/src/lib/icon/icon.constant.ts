import { InjectionToken } from '@angular/core';
import { HseIconStore } from './icon.models';

export const HSE_ICON_STORE: InjectionToken<HseIconStore> = new InjectionToken<HseIconStore>('HSE_ICON_STORE');
