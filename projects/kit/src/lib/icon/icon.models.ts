export interface HseIcon {
  data: string | null;
  url: string;
}

export type HseIconStore = Map<string, HseIcon>;
