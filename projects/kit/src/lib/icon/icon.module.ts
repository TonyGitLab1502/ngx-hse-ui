import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { HseBadgeIconComponent } from './components/badge-icon/badge-icon.component';
import { HseIconComponent } from './components/icon/icon.component';
import { HSE_ICON_STORE } from './icon.constant';
import { HseIconService } from './icon.service';
import { HseIconStore } from './icon.models';
import icons from './icon-store.const';

const declarations: any[] = [HseBadgeIconComponent, HseIconComponent];

@NgModule({
  declarations,
  imports: [CommonModule, HttpClientModule],
  exports: [...declarations],
})
export class HseIconModule {
  static forRoot(additionalIcons: HseIconStore = new Map()): ModuleWithProviders<HseIconModule> {
    if (!additionalIcons.size) {
      Array.from(additionalIcons.keys()).forEach((key: string) => {
        if (icons.has(key)) {
          throw `An icon with name ${key} is already exists`;
        }
      });
    }

    return {
      ngModule: HseIconModule,
      providers: [
        HseIconService,
        {
          provide: HSE_ICON_STORE,
          useValue: new Map([...icons, ...additionalIcons]),
        },
      ],
    };
  }
}
