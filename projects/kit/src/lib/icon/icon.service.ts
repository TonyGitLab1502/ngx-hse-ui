import { HttpClient } from '@angular/common/http';
import { Inject, Injectable, SecurityContext } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { finalize, map, Observable, of, share } from 'rxjs';
import { HseIcon, HseIconStore } from './icon.models';
import { HSE_ICON_STORE } from './icon.constant';

@Injectable({
  providedIn: 'root',
})
export class HseIconService {
  private readonly _icons: HseIconStore;
  private readonly _inProgressUrlFetches: Map<string, Observable<string>> = new Map<string, Observable<string>>();

  get iconStore(): HseIconStore {
    return this._icons;
  }

  constructor(
    @Inject(HSE_ICON_STORE) icons: HseIconStore,
    private readonly http: HttpClient,
    private readonly sanitizer: DomSanitizer
  ) {
    this._icons = icons;
  }

  getIconByName(name: string): Observable<string> {
    const foundIcon: HseIcon | undefined = this._icons.get(name);

    if (!foundIcon) {
      throw `Icon with name "${name}" not found`;
    }

    const inProgressFetch: Observable<string> | undefined = this._inProgressUrlFetches.get(foundIcon.url);

    if (inProgressFetch) {
      return inProgressFetch;
    }

    if (Boolean(foundIcon.data)) {
      return of(foundIcon.data as string);
    }

    const fetchIcon$: Observable<string> = this.downloadIcon(name, foundIcon);
    this._inProgressUrlFetches.set(foundIcon.url, fetchIcon$);

    return fetchIcon$;
  }

  private downloadIcon(name: string, icon: HseIcon): Observable<string> {
    const resource: SafeResourceUrl = this.sanitizer.bypassSecurityTrustResourceUrl(icon.url);
    const url: string = this.sanitizer.sanitize(SecurityContext.URL, resource) as string;

    return this.http.get(url, { responseType: 'text' }).pipe(
      finalize(() => this._inProgressUrlFetches.delete(icon.url)),
      share(),
      map((data: string) => {
        const loadedIcon: HseIcon = { ...icon, data };
        this._icons.set(name, loadedIcon);

        return data;
      })
    );
  }
}
