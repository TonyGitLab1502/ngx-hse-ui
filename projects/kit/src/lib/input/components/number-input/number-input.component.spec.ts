import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HseNumberInputComponent } from './number-input.component';

describe('HseNumberInputComponent', () => {
  let component: HseNumberInputComponent;
  let fixture: ComponentFixture<HseNumberInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HseNumberInputComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(HseNumberInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
