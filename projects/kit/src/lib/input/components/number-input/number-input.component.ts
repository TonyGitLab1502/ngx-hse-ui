import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  HostBinding,
  Input,
  OnDestroy,
  OnInit,
  Output,
  Renderer2,
  TemplateRef,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { fromEvent, Subscription } from 'rxjs';
import { HseUiSizes } from '../../../@common';
import { UtilsService } from '../../../@utils';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'hse-number-input',
  template: `
    <div class="hse-number-input__input-wrapper">
      <ng-container *ngIf="leftIcon">
        <hse-icon class="hse-number-input__left-icon" [name]="leftIcon"></hse-icon>
      </ng-container>
      <ng-container *ngIf="rightIcon">
        <hse-icon class="hse-number-input__right-icon" [name]="rightIcon"></hse-icon>
      </ng-container>
      <ng-container *ngIf="label">
        <label class="hse-number-input__label" for="{{ inputId }}">
          <ng-container hseLabelTemplate [label]="label"></ng-container>
        </label>
      </ng-container>
      <input
        #numberInput
        class="hse-number-input__input"
        type="number"
        [disabled]="disabled"
        [id]="inputId"
        [ngModel]="model"
        [placeholder]="placeholder"
        (ngModelChange)="onChange($event)"
        (keydown)="checkEnter($event)"
        (blur)="onBlur()"
      />
    </div>
  `,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => HseNumberInputComponent),
      multi: true,
    },
  ],
})
export class HseNumberInputComponent implements ControlValueAccessor, OnInit, OnDestroy {
  @HostBinding('class.hse-number-input') private readonly baseCss: boolean = true;

  @Input() disabled: boolean = false;
  @Input() disableSpecialSymbols: boolean = true;
  @Input() hasError: boolean = false;
  @Input() hideArrows: boolean = false;
  @Input() label: string | TemplateRef<void> = '';
  @Input() leftIcon: string = '';
  @Input() max: number | null = null;
  @Input() min: number | null = null;
  @Input()
  set model(value: number | null) {
    this._value = value;
    this._onChange(value);
  }
  @Input() placeholder: string = '';
  @Input() rightIcon: string = '';
  @Input() size: HseUiSizes = HseUiSizes.MEDIUM;
  @Input() step: number | null = null;

  @Output() modelChange: EventEmitter<number | null> = new EventEmitter<number | null>();
  @Output() onEnter: EventEmitter<boolean> = new EventEmitter<boolean>();

  @ViewChild('numberInput', { static: true })
  private _numberInput: ElementRef<HTMLInputElement> = new ElementRef<HTMLInputElement>(this._elRef.nativeElement);

  @HostBinding('class') get additionalClasses(): string {
    return `
      hse-number-input_${this._utilsService.getSizeClass(this.size)}
      ${this.hideArrows ? 'hse-number-input_hide-arrows' : ''}
      ${this.hasError ? 'hse-number-input_invalid' : ''}
    `;
  }

  get model(): number | null {
    return this._value;
  }

  private readonly _SPECIAL_SYMBOLS_CODES: string[] = ['KeyE', 'Minus', 'Equal', 'Period'];
  private _subscription: Subscription = Subscription.EMPTY;
  private _value: number | null = null;
  private _onChange: (value: number | null) => void = () => {};
  private _onTouched: () => void = () => {};

  public readonly inputId: string = '';

  constructor(
    private readonly _elRef: ElementRef,
    private readonly _renderer: Renderer2,
    private readonly _utilsService: UtilsService
  ) {
    this.inputId = this._utilsService.generateUniqId();
  }

  ngOnInit(): void {
    this.setNumberInputAttributes();

    if (this.disableSpecialSymbols) {
      this._subscription = fromEvent<KeyboardEvent>(this._numberInput.nativeElement, 'keypress')
        .pipe(
          tap((event: KeyboardEvent) => {
            if (this._SPECIAL_SYMBOLS_CODES.includes(event.code)) {
              event.preventDefault();

              return;
            }
          })
        )
        .subscribe();
    }
  }

  ngOnDestroy(): void {
    this._subscription?.unsubscribe();
  }

  writeValue(value: number | null): void {
    this._value = value;
  }

  registerOnChange(fn: any): void {
    this._onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this._onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  onChange(value: number | null): void {
    this.writeValue(value);
    this._onChange(this._value);
    this._onTouched();
    this.modelChange.emit(this._value);
  }

  onBlur(): void {
    this._onTouched();
  }

  private setNumberInputAttributes(): void {
    if (this.min) {
      this._renderer.setAttribute(this._numberInput.nativeElement, 'min', this.min.toString());
    }

    if (this.max) {
      this._renderer.setAttribute(this._numberInput.nativeElement, 'max', this.max.toString());
    }

    if (this.step) {
      this._renderer.setAttribute(this._numberInput.nativeElement, 'step', this.step.toString());
    }
  }

  checkEnter($event: KeyboardEvent) {
    if ($event.keyCode == 13) {
      this.onEnter.emit(true);
    }
  }
}
