import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HsePasswordInputComponent } from './password-input.component';

describe('HsePasswordInputComponent', () => {
  let component: HsePasswordInputComponent;
  let fixture: ComponentFixture<HsePasswordInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HsePasswordInputComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(HsePasswordInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
