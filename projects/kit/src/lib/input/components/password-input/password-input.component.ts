import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  forwardRef,
  HostBinding,
  Input,
  Output,
  TemplateRef,
  ViewEncapsulation,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { HseUiSizes } from '../../../@common';
import { UtilsService } from '../../../@utils';

@Component({
  selector: 'hse-password-input',
  template: `
    <div class="hse-password-input__input-wrapper">
      <ng-container *ngIf="withIcons">
        <hse-icon
          class="hse-password-input__right-icon"
          [name]="type === 'text' ? 'eye' : 'eyeOff'"
          (click)="toggleType()"
        ></hse-icon>
      </ng-container>
      <ng-container *ngIf="label">
        <label class="hse-password-input__label" for="{{ inputId }}">
          <ng-container hseLabelTemplate [label]="label"></ng-container>
        </label>
      </ng-container>
      <input
        class="hse-password-input__input"
        [ngModel]="model"
        (ngModelChange)="onChange($event)"
        [disabled]="disabled"
        [id]="inputId"
        [placeholder]="placeholder"
        [type]="type"
      />
    </div>
  `,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => HsePasswordInputComponent),
      multi: true,
    },
  ],
})
export class HsePasswordInputComponent implements ControlValueAccessor {
  @HostBinding('class.hse-password-input') private readonly baseCss: boolean = true;

  @Input() disabled: boolean = false;
  @Input() hasError: boolean = false;
  @Input() label: string | TemplateRef<void> = '';
  @Input()
  set model(value: string) {
    this._value = value;
    this._onChange(value);
  }
  @Input() placeholder: string = '';
  @Input() size: HseUiSizes = HseUiSizes.MEDIUM;
  @Input() withIcons: boolean = false;

  @Output() modelChange: EventEmitter<string> = new EventEmitter<string>();

  @HostBinding('class') get additionalClasses(): string {
    return `
      hse-password-input_${this._utilsService.getSizeClass(this.size)}
      ${this.hasError ? 'hse-password-input_invalid' : ''}
    `;
  }

  get model(): string {
    return this._value;
  }

  get type(): 'password' | 'text' {
    return this._type;
  }

  private _type: 'password' | 'text' = 'password';
  private _value: string = '';
  private _onChange: (value: string) => void = () => {};
  private _onTouched: () => void = () => {};

  public readonly inputId: string = '';

  constructor(private readonly _utilsService: UtilsService) {
    this.inputId = this._utilsService.generateUniqId();
  }

  writeValue(value: string): void {
    this._value = value;
  }

  registerOnChange(fn: any): void {
    this._onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this._onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  onChange(value: string): void {
    this.writeValue(value);
    this._onChange(this._value);
    this._onTouched();

    this.modelChange.emit(this._value);
  }

  toggleType(): void {
    this._type = this._type === 'password' ? 'text' : 'password';
  }
}
