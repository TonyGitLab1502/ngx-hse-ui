import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HseTextInputComponent } from './text-input.component';

describe('HseTextInputComponent', () => {
  let component: HseTextInputComponent;
  let fixture: ComponentFixture<HseTextInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HseTextInputComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(HseTextInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
