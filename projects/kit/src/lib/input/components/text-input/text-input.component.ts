import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  forwardRef,
  HostBinding,
  Input,
  Output,
  TemplateRef,
  ViewEncapsulation,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { HseUiSizes } from '../../../@common';
import { UtilsService } from '../../../@utils';

@Component({
  selector: 'hse-text-input',
  template: `
    <div class="hse-text-input__input-wrapper">
      <ng-container *ngIf="leftIcon">
        <hse-icon class="hse-text-input__left-icon" [name]="leftIcon"></hse-icon>
      </ng-container>
      <ng-container *ngIf="rightIcon">
        <hse-icon class="hse-text-input__right-icon" [name]="rightIcon"></hse-icon>
      </ng-container>
      <ng-container *ngIf="label">
        <label class="hse-text-input__label" for="{{ inputId }}">
          <ng-container hseLabelTemplate [label]="label"></ng-container>
        </label>
      </ng-container>
      <input
        class="hse-text-input__input"
        type="text"
        [disabled]="disabled"
        [dropSpecialCharacters]="dropSpecialCharacters"
        [id]="inputId"
        [mask]="mask"
        [showMaskTyped]="showMaskTyped"
        [ngModel]="model"
        (ngModelChange)="onChange($event)"
        (keydown)="checkEnter($event)"
        [placeholder]="placeholder"
      />
    </div>
  `,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => HseTextInputComponent),
      multi: true,
    },
  ],
})
export class HseTextInputComponent implements ControlValueAccessor {
  @HostBinding('class.hse-text-input') private readonly baseCss: boolean = true;

  @Input() disabled: boolean = false;
  @Input() dropSpecialCharacters: boolean = true;
  @Input() hasError: boolean = false;
  @Input() label: string | TemplateRef<void> = '';
  @Input() leftIcon: string = '';
  @Input() mask: string = '';
  @Input()
  set model(value: string) {
    this._value = value;
    this._onChange(value);
  }
  @Input() placeholder: string = '';
  @Input() showMaskTyped: boolean = false;
  @Input() rightIcon: string = '';
  @Input() size: HseUiSizes = HseUiSizes.MEDIUM;

  @Output() modelChange: EventEmitter<string> = new EventEmitter<string>();
  @Output() onEnter: EventEmitter<boolean> = new EventEmitter<boolean>();

  @HostBinding('class') get additionalClasses(): string {
    return `
      hse-text-input_${this._utilsService.getSizeClass(this.size)}
      ${this.hasError ? 'hse-text-input_invalid' : ''}
    `;
  }

  get model(): string {
    return this._value;
  }

  private _value: string = '';
  private _onChange: (value: string) => void = () => {};
  private _onTouched: () => void = () => {};

  public readonly inputId: string = '';

  constructor(private readonly _utilsService: UtilsService) {
    this.inputId = this._utilsService.generateUniqId();
  }

  ngOnInit(): void {}

  writeValue(value: string): void {
    this._value = value;
  }

  registerOnChange(fn: any): void {
    this._onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this._onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  onChange(value: string): void {
    this.writeValue(value);
    this._onChange(this._value);
    this._onTouched();

    this.modelChange.emit(this._value);
  }

  checkEnter($event: KeyboardEvent) {
    if ($event.keyCode == 13) {
      this.onEnter.emit(true);
    }
  }
}
