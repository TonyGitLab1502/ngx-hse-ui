import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';
import { HseNumberInputComponent } from './components/number-input/number-input.component';
import { HsePasswordInputComponent } from './components/password-input/password-input.component';
import { HseTextInputComponent } from './components/text-input/text-input.component';
import { HseCommonUiModule } from '../@common';
import { HseIconModule } from '../icon';

const declarations: any[] = [HseNumberInputComponent, HsePasswordInputComponent, HseTextInputComponent];

@NgModule({
  declarations,
  imports: [CommonModule, FormsModule, HseCommonUiModule, HseIconModule, NgxMaskModule.forRoot(), ReactiveFormsModule],
  exports: [...declarations],
})
export class HseInputModule {}
