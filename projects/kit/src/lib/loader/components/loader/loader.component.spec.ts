import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HseLoaderComponent } from './loader.component';

describe('HseLoaderComponent', () => {
  let component: HseLoaderComponent;
  let fixture: ComponentFixture<HseLoaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HseLoaderComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HseLoaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
