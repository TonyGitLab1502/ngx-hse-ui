import { ChangeDetectionStrategy, Component, HostBinding, ViewEncapsulation } from '@angular/core';
import { Observable } from 'rxjs';
import { HseLoaderService } from '../../loader.service';

@Component({
  selector: 'hse-loader',
  template: `
    <ng-container *ngIf="isLoading | async">
      <div class="hse-loader__container">
        <div class="hse-loader__loader">
          <div></div>
          <div></div>
          <div></div>
          <div></div>
        </div>
      </div>
    </ng-container>
  `,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HseLoaderComponent {
  @HostBinding('class.hse-loader') private readonly baseCss: boolean = true;
  get isLoading(): Observable<boolean> {
    return this._loaderService.isLoading;
  }
  
  constructor(private readonly _loaderService: HseLoaderService) { }
}
