import { TestBed } from '@angular/core/testing';

import { HseLoaderInterceptorService } from './loader-interceptor.service';

describe('HseLoaderInterceptorService', () => {
  let service: HseLoaderInterceptorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HseLoaderInterceptorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
