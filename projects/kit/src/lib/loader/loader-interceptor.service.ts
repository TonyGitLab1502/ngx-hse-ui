import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { finalize } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HttpMethods } from './loader.models';
import { HseLoaderService } from './loader.service';

@Injectable({
  providedIn: 'root'
})
export class HseLoaderInterceptorService implements HttpInterceptor {
  /**
   * Количество активных запросов
   */
  private _activeRequestCount: number = 0;
  
  /**
   * Список url, для которых не нужно отображать hse-loader
   */
  private _skippedUrls: string[] = [];
  
  /**
   * HTTP-методы, для которых не отображаем hse-loader
   */
  private _skippedMethods: HttpMethods[] = [];
  
  constructor(private readonly _loaderService: HseLoaderService) { }
  
  addSkippedUrl(url: string): void {
    if (!this._skippedUrls.find((skippedUrl: string) => skippedUrl === url)) {
      this._skippedUrls.push(url);
    }
  }
  
  addSkippedMethods(method: HttpMethods): void {
    if (!this._skippedMethods.find((skippedMethod: HttpMethods) => skippedMethod === method)) {
      this._skippedMethods.push(method);
    }
  }
  
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.interceptRequest(req);
  
    return next.handle(req).pipe(
      finalize(() => {
        this.interceptResponse(req);
      })
    );
  }
  
  interceptRequest(request: HttpRequest<any>) {
    const shouldBypass = this.shouldBypass(request);
    
    if (!shouldBypass) {
      if (this._activeRequestCount === 0) {
        this._loaderService.show();
      }
      
      this._activeRequestCount++;
    }
  }
  
  interceptResponse(request: HttpRequest<any>) {
    const shouldBypass = this.shouldBypass(request);
    
    if (!shouldBypass) {
      this._activeRequestCount--;
    }
    
    if (this._activeRequestCount < 0) {
      this._activeRequestCount = 0;
    }
    
    if (this._activeRequestCount === 0) {
      this._loaderService.hide();
    }
  }
  
  /**
   * Перехватить ошибку
   */
  interceptError(request: any) {
    const shouldBypass = this.shouldBypass(request);
    
    if (!shouldBypass) {
      if (this._activeRequestCount === 0) {
        this._loaderService.show();
      }
      
      this._activeRequestCount++;
    }
    
    this.interceptResponse(request);
  }
  
  /**
   * Проверить, следует ли пропустить запрос, переданный в параметр
   */
  private shouldBypass(request: HttpRequest<any>): boolean {
    return this.shouldBypassMethod(request.method)
      || this.shouldBypassUrl(request.url);
  }
  
  /**
   * Проверить, следует пропустить запрос по заданному url
   */
  private shouldBypassUrl(url: string): boolean {
    return this._skippedUrls.some((skipped: string) => {
      return url.indexOf(skipped) !== -1;
    });
  }
  
  /**
   * Проверить, следует ли пропустить запрос с заданным http-методом
   */
  private shouldBypassMethod(method: string) {
    return this._skippedMethods.some((skipped: HttpMethods) => {
      return new RegExp(skipped, 'i').test(method);
    });
  }
  
}
