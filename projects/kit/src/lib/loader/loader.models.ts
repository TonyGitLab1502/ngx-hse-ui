export enum HttpMethods {
  GET = 'GET',
  POST = 'POST',
  PUT = 'PUT',
  DELETE = 'DELETE',
  PATCH = 'PATCH',
  CONNECT = 'CONNECT',
  TRACE = 'TRACE',
  OPTIONS = 'OPTIONS',
  HEAD = 'HEAD'
}
