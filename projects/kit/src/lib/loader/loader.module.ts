import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HseLoaderComponent } from './components/loader/loader.component';
import { HseLoaderInterceptorService } from './loader-interceptor.service';
import { HseLoaderService } from './loader.service';

const declarations: any[] = [HseLoaderComponent];

@NgModule({
  declarations,
  imports: [CommonModule],
  providers: [HseLoaderInterceptorService, HseLoaderService],
  exports: [...declarations]
})
export class HseLoaderModule { }
