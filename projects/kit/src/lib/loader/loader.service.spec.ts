import { TestBed } from '@angular/core/testing';

import { HseLoaderService } from './loader.service';

describe('HseLoaderService', () => {
  let service: HseLoaderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HseLoaderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
