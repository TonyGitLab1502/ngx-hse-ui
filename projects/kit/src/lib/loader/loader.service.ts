import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HseLoaderService {
  private loadingStatus$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  get isLoading(): Observable<boolean> {
    return this.loadingStatus$.asObservable();
  }
  
  /**
   * Показать лоадер
   */
  show() {
    this.loading = true;
  }
  
  /**
   * Скрыть лоадер
   */
  hide() {
    this.loading = false;
  }
  
  /**
   * Установить видимость лоадера
   */
  private set loading(value: boolean) {
    this.loadingStatus$.next(value);
  }
  
}
