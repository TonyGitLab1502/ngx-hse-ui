import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HsePaginationComponent } from './pagination.component';

describe('HsePaginationComponent', () => {
  let component: HsePaginationComponent;
  let fixture: ComponentFixture<HsePaginationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HsePaginationComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(HsePaginationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
