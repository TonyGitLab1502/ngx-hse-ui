import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  HostBinding,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
  ViewEncapsulation,
} from '@angular/core';
import { HsePaginationButtons } from '../../pagination.models';
import { HseUiVariants } from '../../../@common';
import { UtilsService } from '../../../@utils';

@Component({
  selector: 'hse-pagination',
  template: `
    <hse-icon-button
      class="hse-pagination-button"
      iconName="chevronLeft"
      [variant]="tertiaryVariant"
      (onClick)="goToPreviousPage()"
    ></hse-icon-button>
    <ng-container *ngFor="let page of pages">
      <ng-container *ngIf="page; else dotsTmpl">
        <hse-icon-button
          class="hse-pagination-button"
          [pageNumber]="page"
          [variant]="page === currentPage ? primaryVariant : tertiaryVariant"
          (onClick)="setPage(page)"
        ></hse-icon-button>
      </ng-container>
      <ng-template #dotsTmpl>
        <hse-icon-button
          class="hse-pagination-button"
          iconName="dots"
          [variant]="tertiaryVariant"
          (onClick)="setPage(null)"
        ></hse-icon-button>
      </ng-template>
    </ng-container>
    <hse-icon-button
      class="hse-pagination-button"
      iconName="chevronRight"
      [variant]="tertiaryVariant"
      (onClick)="goToNextPage()"
    ></hse-icon-button>
  `,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HsePaginationComponent implements OnChanges {
  @HostBinding('class.hse-pagination') private readonly baseCss: boolean = true;

  @Input() currentPage: number = 1;
  @Input() totalPages: number = 1;

  @Output() onChangePage: EventEmitter<number> = new EventEmitter<number>();

  constructor(private readonly _utilsService: UtilsService) {}

  get pages(): HsePaginationButtons {
    return this._pageButtons;
  }

  get primaryVariant(): HseUiVariants {
    return HseUiVariants.PRIMARY;
  }

  get tertiaryVariant(): HseUiVariants {
    return HseUiVariants.TERTIARY;
  }

  private _pageButtons: HsePaginationButtons = [];
  private readonly pagesInLine: number = 7;
  private readonly pagesSideOffset: number = 3;

  ngOnChanges(changes: SimpleChanges): void {
    this.buildPages();
  }

  goToPreviousPage(): void {
    if (this.currentPage > 1) {
      this.setPage(this.currentPage - 1);
    }
  }

  goToNextPage(): void {
    if (this.currentPage < this.totalPages) {
      this.setPage(this.currentPage + 1);
    }
  }

  setPage(page: number | null) {
    if ([this.currentPage, null].includes(page)) {
      return;
    }

    this.currentPage = page as number;
    this.buildPages();
    this.onChangePage.emit(this.currentPage);
  }

  private buildPages(): void {
    if (this.totalPages <= this.pagesInLine) {
      this._pageButtons = this._utilsService.createRange(1, this.totalPages);
    } else {
      if (this.currentPage < this.pagesSideOffset) {
        this._pageButtons = [
          ...this._utilsService.createRange(1, this.pagesSideOffset),
          null,
          ...this._utilsService.createRange(this.totalPages - this.pagesSideOffset + 1, this.totalPages),
        ];
      } else if (this.currentPage === this.pagesSideOffset) {
        this._pageButtons = [
          ...this._utilsService.createRange(1, this.pagesSideOffset + 1),
          null,
          ...this._utilsService.createRange(this.totalPages - 1, this.totalPages),
        ];
      } else if (this.currentPage === this.pagesSideOffset + 1) {
        this._pageButtons = [...this._utilsService.createRange(1, this.pagesSideOffset + 2), null, this.totalPages];
      } else if (this.totalPages - this.currentPage < this.pagesSideOffset + 1) {
        this._pageButtons = [
          1,
          null,
          ...this._utilsService.createRange(this.totalPages - this.pagesSideOffset - 1, this.totalPages),
        ];
      } else {
        this._pageButtons = [
          1,
          null,
          ...this._utilsService.createRange(this.currentPage - 1, this.currentPage + 1),
          null,
          this.totalPages,
        ];
      }
    }
  }
}
