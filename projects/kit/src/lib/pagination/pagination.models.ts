export type HsePaginationButton = number | null;
export type HsePaginationButtons = HsePaginationButton[];
