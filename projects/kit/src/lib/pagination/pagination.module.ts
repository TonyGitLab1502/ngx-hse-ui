import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HsePaginationComponent } from './components/pagination/pagination.component';
import { HseButtonModule } from '../button';

const declarations: any[] = [HsePaginationComponent];

@NgModule({
  declarations,
  imports: [CommonModule, HseButtonModule],
  exports: [...declarations],
})
export class HsePaginationModule {}
