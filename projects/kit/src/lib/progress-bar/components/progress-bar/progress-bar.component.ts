import {ChangeDetectionStrategy, Component, HostBinding, Input, OnChanges, OnInit, ViewEncapsulation } from "@angular/core";
import {UtilsService} from "../../../@utils";
import {HseUiSizes} from "../../../@common";

@Component({
  selector: 'hse-progress-bar',
  template: `
  <div class="hse-progress-bar__wrapper">
    <div class="hse-progress-bar__wrapper__value" [style.width.%]="progress"></div>
  </div>
  `,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HseProgressBarComponent implements OnInit {

  @Input() progress: number = 0;
  @Input() size: HseUiSizes = HseUiSizes.MEDIUM;

  @HostBinding('class') get additionalClasses(): string {
    return `
      hse-progress-bar_${this._utilsService.getSizeClass(this.size)}
    `;
  }

  constructor(private readonly _utilsService: UtilsService) {


  }

  ngOnInit() {

  }
}
