import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import {HseProgressBarComponent} from "./components/progress-bar/progress-bar.component";



const declarations: any[] = [HseProgressBarComponent];

@NgModule({
  declarations,
  imports: [CommonModule],
  exports: [...declarations],
})
export class HseProgressBarModule {}
