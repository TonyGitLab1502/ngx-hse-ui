import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HseRadioGroupComponent } from './radio-group.component';

describe('HseRadioGroupComponent', () => {
  let component: HseRadioGroupComponent;
  let fixture: ComponentFixture<HseRadioGroupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HseRadioGroupComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(HseRadioGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
