import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  forwardRef,
  HostBinding,
  Input,
  Output,
  TemplateRef,
  ViewEncapsulation,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { HseDirectionTypes, HseUiSizes } from '../../../@common';
import { UtilsService } from '../../../@utils';
import { HseRadioGroup } from '../../radio-group.models';

@Component({
  selector: 'hse-radio-group',
  template: `
    <ng-container *ngIf="label">
      <div class="hse-radio-group__label">
        <ng-container hseLabelTemplate [label]="label"></ng-container>
      </div>
    </ng-container>
    <div class="hse-radio-group__buttons" [ngClass]="'hse-radio-group__' + direction + '-buttons'">
      <ng-container *ngFor="let radio of collection; index as idx">
        <div class="hse-radio-button">
          <input
            class="hse-radio-button__input"
            id="{{ radioGroupId }}-{{ idx + 1 }}"
            type="radio"
            [disabled]="radio?.disabled || false"
            [name]="name"
            [ngModel]="model"
            (ngModelChange)="onChange($event)"
            [value]="radio.value"
          />
          <label class="hse-radio-button__label" for="{{ radioGroupId }}-{{ idx + 1 }}">
            <ng-container hseLabelTemplate [label]="radio.label"></ng-container>
          </label>
        </div>
      </ng-container>
    </div>
  `,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => HseRadioGroupComponent),
      multi: true,
    },
  ],
})
export class HseRadioGroupComponent implements ControlValueAccessor {
  @HostBinding('class.hse-radio-group') private readonly baseCss: boolean = true;

  @Input() collection: HseRadioGroup<any> = [];
  @Input() direction: HseDirectionTypes = HseDirectionTypes.ROW;
  @Input() hasError: boolean = false;
  @Input() label: string | TemplateRef<void> = '';
  @Input()
  set model(value: any) {
    this._value = value;
    this._onChange(value);
  }
  @Input() name: string = 'radio-group';
  @Input() size: HseUiSizes = HseUiSizes.MEDIUM;
  @Output() modelChange: EventEmitter<any> = new EventEmitter<any>();
  @HostBinding('class') get additionalClasses(): string {
    return `
      hse-radio-group_${this._utilsService.getSizeClass(this.size)}
      ${this.hasError && !this.model ? 'hse-radio-group_invalid' : ''}
    `;
  }

  get model(): string {
    return this._value;
  }

  private _value: any = '';
  private _onChange: (value: any) => void = () => {};
  private _onTouched: () => void = () => {};

  public readonly radioGroupId: string;

  constructor(private readonly _utilsService: UtilsService) {
    this.radioGroupId = this._utilsService.generateUniqId();
  }

  writeValue(value: any): void {
    this._value = value;
  }

  registerOnChange(fn: any): void {
    this._onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this._onTouched = fn;
  }

  onChange(value: any): void {
    this._value = value;

    this._onChange(this._value);
    this._onTouched();
    this.modelChange.emit(this._value);
  }
}
