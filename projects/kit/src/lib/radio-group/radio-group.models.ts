import { TemplateRef } from '@angular/core';

export type HseRadioButton<T> = {
  label: string | TemplateRef<void>;
  value: T;
  disabled?: boolean;
};
export type HseRadioGroup<T> = HseRadioButton<T>[];
