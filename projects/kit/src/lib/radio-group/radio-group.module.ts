import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HseCommonUiModule } from '../@common';
import { HseRadioGroupComponent } from './components/radio-group/radio-group.component';

const declarations: any[] = [HseRadioGroupComponent];

@NgModule({
  declarations,
  imports: [CommonModule, HseCommonUiModule, FormsModule, ReactiveFormsModule],
  exports: [...declarations],
})
export class HseRadioGroupModule {}
