import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HseMultiSelectComponent } from './multi-select.component';

describe('HseMultiSelectComponent', () => {
  let component: HseMultiSelectComponent;
  let fixture: ComponentFixture<HseMultiSelectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HseMultiSelectComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(HseMultiSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
