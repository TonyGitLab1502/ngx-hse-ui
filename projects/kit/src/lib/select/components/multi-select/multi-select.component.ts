import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  HostBinding,
  HostListener,
  Input,
  OnDestroy,
  Output,
  TemplateRef,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { fromEvent, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { HseSelectOption } from '../../select.models';
import { HseUiSizes } from '../../../@common';
import { UtilsService } from '../../../@utils';

@Component({
  selector: 'hse-multiple-select',
  template: `
    <ng-container *ngIf="label">
      <label class="hse-multiple-select__label" for="{{ selectId }}">{{ label }}</label>
    </ng-container>
    <div
      #selectEl
      class="hse-multiple-select__select"
      [id]="selectId"
      [matMenuTriggerFor]="!disabled ? optionList : null"
      (menuOpened)="menuOpenedHandle()"
      (menuClosed)="menuClosedHandle()"
    >
      <div class="hse-multiple-select__selected-value-wrapper">
        <ng-container *ngIf="placeholder && model.length === 0">
          <span class="hse-multiple-select__placeholder">{{ placeholder }}</span>
        </ng-container>
        <ng-container *ngIf="model.length > 0">
          <div class="hse-multiple-select__selected-value">
            <ng-container *ngFor="let selectedOption of model">
              <hse-tag class="hse-multiple-select__tag-item" [disabled]="disabled" [size]="tagSize">
                <ng-container hseLabelTemplate [label]="selectedOption.label"></ng-container>
              </hse-tag>
            </ng-container>
          </div>
        </ng-container>
      </div>
      <div class="hse-multiple-select__icons">
        <ng-container *ngIf="hiddenTagsCount > 0">
          <div class="hse-multiple-select__hidden-tags">
            <span class="hse-multiple-select__hidden-tags-counter">{{ hiddenTagsCount }}</span>
          </div>
        </ng-container>
        <ng-container *ngIf="clearable && model.length > 0">
          <hse-icon class="hse-multiple-select__icon clear-icon" name="clear" (click)="onClear($event)"></hse-icon>
        </ng-container>
        <ng-container *ngIf="expandIcon">
          <hse-icon class="hse-multiple-select__icon" [name]="expandIcon"></hse-icon>
        </ng-container>
      </div>
    </div>
    <mat-menu class="hse-multiple-select__menu-override" #optionList="matMenu" [overlapTrigger]="false">
      <div [style.width.px]="selectWidth">
        <ng-container *ngIf="searchable">
          <div class="hse-multiple-select__search-input-wrapper">
            <input
              class="hse-multiple-select__search-input"
              id="{{ selectId + searchInputSuffix }}"
              type="text"
              [placeholder]="searchPlaceholder"
              (click)="stopPropagation($event)"
            />
            <hse-icon class="hse-multiple-select__search-input-icon" name="search"></hse-icon>
          </div>
        </ng-container>
        <div class="hse-multiple-select__option-list">
          <ng-container *ngIf="options.length; else notFoundDataTmpl">
            <ng-container *ngFor="let option of options">
              <button class="hse-multiple-select__option" (click)="stopPropagation($event)">
                <hse-checkbox
                  [disabled]="option.disabled || false"
                  [model]="selectedValues.includes(option.value)"
                  (modelChange)="onSelect(option)"
                >
                  <ng-container hseLabelTemplate [label]="option.label"></ng-container>
                </hse-checkbox>
              </button>
            </ng-container>
          </ng-container>
        </div>
      </div>
    </mat-menu>
    <ng-template #notFoundDataTmpl>
      <div class="hse-multiple-select__empty-option-list">
        <ng-container hseLabelTemplate [label]="notFoundData"></ng-container>
      </div>
    </ng-template>
  `,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => HseMultiSelectComponent),
      multi: true,
    },
  ],
})
export class HseMultiSelectComponent implements AfterViewInit, ControlValueAccessor, OnDestroy {
  @HostBinding('class.hse-multiple-select') private readonly baseCss: boolean = true;

  @Input() clearable: boolean = false;
  @Input() disabled: boolean = false;
  @Input() expandIcon: string = 'expandMore';
  @Input() hasError: boolean = false;
  @Input() label: string = '';
  @Input()
  set model(value: HseSelectOption<any>[]) {
    this._value = value;
    this._onChange(value);
  }
  @Input() notFoundData: string | TemplateRef<void> = 'Ничего не найдено';
  @Input() options: HseSelectOption<any>[] = [];
  @Input() placeholder: string = '';
  @Input() searchable: boolean = false;
  @Input() searchPlaceholder: string = 'Поиск';
  @Input() size: HseUiSizes = HseUiSizes.MEDIUM;

  @Output() modelChange: EventEmitter<HseSelectOption<any>[]> = new EventEmitter<HseSelectOption<any>[]>();
  @Output() onSearchOptions: EventEmitter<string> = new EventEmitter<string>();

  @ViewChild('selectEl', { static: true })
  private readonly _selectEl: ElementRef<HTMLElement> = new ElementRef<HTMLElement>(this._elRef.nativeElement);

  @HostBinding('class') get additionalClasses(): string {
    return `
      hse-multiple-select_${this._utilsService.getSizeClass(this.size)}
      ${this.disabled ? 'hse-multiple-select_disabled' : ''}
      ${this.hasError && !this.model.length ? 'hse-multiple-select_invalid' : ''}
    `;
  }

  @HostListener('window:resize')
  onResize() {
    this.updateSelectMenuProps();
  }

  get model(): HseSelectOption<any>[] {
    return this._value;
  }

  private readonly _indentWidth: number = 4;
  private readonly _optionalIconsWidth: number = 60;
  private readonly _selectedValueClassName: string = 'hse-multiple-select__selected-value';
  private _searchInputSubscription: Subscription = Subscription.EMPTY;
  private _value: HseSelectOption<any>[] = [];
  private _onChange: (value: HseSelectOption<any>[]) => void = () => {};
  private _onTouched: () => void = () => {};

  public readonly tagSize: HseUiSizes = HseUiSizes.SMALL;
  public readonly selectId: string = '';
  public readonly searchInputSuffix: string = '_searchInput';
  public hiddenTagsCount: number = 0;
  public selectedValues: any[] = [];
  public selectWidth: number = 0;

  constructor(
    private readonly _cdRef: ChangeDetectorRef,
    private readonly _elRef: ElementRef,
    private readonly _utilsService: UtilsService
  ) {
    this.selectId = this._utilsService.generateUniqId();
  }

  ngAfterViewInit(): void {
    this.updateSelectMenuProps();
  }

  ngOnDestroy(): void {
    this._searchInputSubscription.unsubscribe();
  }

  writeValue(value: Array<HseSelectOption<any>>): void {
    this._value = value;
    this.selectedValues = this._value.map((option: HseSelectOption<any>) => option.value);
    setTimeout(() => this.updateTagsCount(), 0);
  }

  registerOnChange(fn: any): void {
    this._onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this._onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  onSelect(option: HseSelectOption<any>): void {
    const optionIdx: number = this._value.findIndex((opt: HseSelectOption<any>) => opt.value === option.value);
    const newValue =
      optionIdx !== -1
        ? this._value.filter((opt: HseSelectOption<any>) => opt.value !== option.value)
        : [...this._value, option];

    this.writeValue(newValue);
    this._onChange(newValue);
    this._onTouched();
    this.modelChange.emit(this._value);
  }

  onClear(event: MouseEvent): void {
    event.stopPropagation();

    if (!this.disabled) {
      this.writeValue([]);
      this._onChange([]);
      this._onTouched();
      this.hiddenTagsCount = 0;
      this.modelChange.emit(this._value);
    }
  }

  updateSelectMenuProps(): void {
    this.selectWidth = this._selectEl.nativeElement.offsetWidth;
    this.updateTagsCount();
  }

  stopPropagation(event: MouseEvent): void {
    event.stopPropagation();
  }

  menuOpenedHandle(): void {
    this.checkSearchSubscription();
  }

  menuClosedHandle(): void {
    this.resetSearchSettings();
  }

  private checkSearchSubscription(): void {
    if (this.searchable && this._searchInputSubscription.closed) {
      const searchInput: HTMLElement | null = document.getElementById(`${this.selectId}${this.searchInputSuffix}`);

      if (searchInput) {
        this._searchInputSubscription = fromEvent(searchInput, 'input')
          .pipe(
            map((e: Event) => (e.target as HTMLInputElement).value),
            debounceTime(500),
            distinctUntilChanged()
          )
          .subscribe((value: string) => this.onSearchOptions.emit(value || ''));
      }
    }
  }

  private resetSearchSettings(): void {
    const searchInput: HTMLElement | null = document.getElementById(`${this.selectId}${this.searchInputSuffix}`);

    if (searchInput) {
      (searchInput as HTMLInputElement).value = '';
      this.onSearchOptions.emit('');
    }
  }

  private updateTagsCount(): void {
    const selectedValueWrapper: Element | null = this._selectEl.nativeElement.children.item(0);
    let tagsWrapper: Element | null = null;

    if (selectedValueWrapper) {
      tagsWrapper = selectedValueWrapper?.children.item(0);

      if (tagsWrapper && tagsWrapper.classList.contains(this._selectedValueClassName)) {
        const selectedValueWrapperWidth: number = parseFloat(getComputedStyle(selectedValueWrapper).width);
        let tagsWidthWithIndent: number = 0;
        let tagIdx: number = 0;

        while (tagsWidthWithIndent < selectedValueWrapperWidth && tagIdx < this.model.length) {
          tagsWidthWithIndent += parseFloat(getComputedStyle(tagsWrapper.children[tagIdx]).width) + this._indentWidth;
          tagIdx += 1;
        }

        const includedIconsWidth = this.hiddenTagsCount === 0 ? this._optionalIconsWidth : 0;

        this.hiddenTagsCount =
          tagsWidthWithIndent + includedIconsWidth > selectedValueWrapperWidth ? this.model.length - tagIdx + 1 : 0;
        this._cdRef.detectChanges();
      }
    }
  }
}
