import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HseSelectComponent } from './select.component';

describe('HseSelectComponent', () => {
  let component: HseSelectComponent;
  let fixture: ComponentFixture<HseSelectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HseSelectComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(HseSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
