import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  HostBinding,
  HostListener,
  Input,
  OnDestroy,
  Output,
  TemplateRef,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { fromEvent, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { HseSelectOption } from '../../select.models';
import { HseUiSizes } from '../../../@common';
import { UtilsService } from '../../../@utils';

@Component({
  selector: 'hse-select',
  template: `
    <ng-container *ngIf="label">
      <label class="hse-select__label" for="{{ selectId }}">{{ label }}</label>
    </ng-container>
    <div
      #selectEl
      class="hse-select__select"
      [id]="selectId"
      [matMenuTriggerFor]="!disabled ? optionList : null"
      (menuOpened)="menuOpenedHandle()"
      (menuClosed)="menuClosedHandle()"
    >
      <div class="hse-select__selected-value-wrapper">
        <ng-container *ngIf="placeholder && !model">
          <span class="hse-select__placeholder">{{ placeholder }}</span>
        </ng-container>
        <ng-container *ngIf="model">
          <span class="hse-select__selected-value">
            <ng-container hseLabelTemplate [label]="model.label"></ng-container>
          </span>
        </ng-container>
      </div>
      <div class="hse-select__icons">
        <ng-container *ngIf="clearable && model">
          <hse-icon class="hse-select__icon clear-icon" name="clear" (click)="onClear($event)"></hse-icon>
        </ng-container>
        <hse-icon class="hse-select__icon" name="expandMore"></hse-icon>
      </div>
    </div>
    <mat-menu class="hse-select__menu-override" #optionList="matMenu" [overlapTrigger]="false">
      <div [style.width.px]="selectWidth">
        <ng-container *ngIf="searchable">
          <div class="hse-select__search-input-wrapper">
            <input
              class="hse-select__search-input"
              id="{{ selectId + searchInputSuffix }}"
              type="text"
              [placeholder]="searchPlaceholder"
              (click)="stopPropagation($event)"
            />
            <hse-icon class="hse-select__search-input-icon" name="search"></hse-icon>
          </div>
        </ng-container>
        <div class="hse-select__option-list">
          <ng-container *ngIf="options.length; else notFoundDataTmpl">
            <ng-container *ngFor="let option of options">
              <button
                class="hse-select__option"
                [class.selected]="option.value === model?.value"
                [disabled]="option.disabled"
                (click)="onSelect(option)"
              >
                <ng-container hseLabelTemplate [label]="option.label"></ng-container>
              </button>
            </ng-container>
          </ng-container>
        </div>
      </div>
    </mat-menu>
    <ng-template #notFoundDataTmpl>
      <div class="hse-select__empty-option-list">
        <ng-container hseLabelTemplate [label]="notFoundData"></ng-container>
      </div>
    </ng-template>
  `,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => HseSelectComponent),
      multi: true,
    },
  ],
})
export class HseSelectComponent implements AfterViewInit, ControlValueAccessor, OnDestroy {
  @HostBinding('class.hse-select') private readonly baseCss: boolean = true;

  @Input() clearable: boolean = false;
  @Input() disabled: boolean = false;
  @Input() hasError: boolean = false;
  @Input() label: string | TemplateRef<void> = '';
  @Input()
  set model(value: HseSelectOption<any> | null) {
    this._value = value;
    this._onChange(value);
  }
  @Input() notFoundData: string | TemplateRef<void> = 'Ничего не найдено';
  @Input() options: HseSelectOption<any>[] = [];
  @Input() placeholder: string = '';
  @Input() searchable: boolean = false;
  @Input() searchPlaceholder: string = 'Поиск';
  @Input() size: HseUiSizes = HseUiSizes.MEDIUM;

  @Output() modelChange: EventEmitter<HseSelectOption<any> | null> = new EventEmitter<HseSelectOption<any> | null>();
  @Output() onSearchOptions: EventEmitter<string> = new EventEmitter<string>();

  @ViewChild('selectEl', { static: true })
  private readonly _selectEl: ElementRef<HTMLElement> = new ElementRef<HTMLElement>(this._elRef.nativeElement);

  @HostBinding('class') get additionalClasses(): string {
    return `
      hse-select_${this._utilsService.getSizeClass(this.size)}
      ${this.disabled ? 'hse-select_disabled' : ''}
      ${this.hasError && !this.model ? 'hse-select_invalid' : ''}
    `;
  }

  @HostListener('window:resize')
  onResize() {
    this.updateSelectMenuProps();
  }

  get model(): HseSelectOption<any> | null {
    return this._value;
  }

  private _searchInputSubscription: Subscription = Subscription.EMPTY;
  private _value: HseSelectOption<any> | null = null;
  private _onChange: (value: HseSelectOption<any> | null) => void = () => {};
  private _onTouched: () => void = () => {};

  public readonly selectId: string = '';
  public readonly searchInputSuffix: string = '_searchInput';
  public selectWidth: number = 0;

  constructor(private readonly _elRef: ElementRef, private readonly _utilsService: UtilsService) {
    this.selectId = this._utilsService.generateUniqId();
  }

  ngOnInit(): void {

  }

  ngAfterViewInit(): void {
    this.updateSelectMenuProps();
  }

  ngOnDestroy(): void {
    this._searchInputSubscription.unsubscribe();
  }

  writeValue(value: HseSelectOption<any> | null): void {
    this._value = value;
  }

  registerOnChange(fn: any): void {
    this._onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this._onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  onSelect(option: HseSelectOption<any>): void {
    this.writeValue(option);
    this._onChange(option);
    this._onTouched();
    this.resetSearchSettings();
    this.modelChange.emit(this._value);
  }

  onClear(event: MouseEvent): void {
    event.stopPropagation();

    if (!this.disabled) {
      this.writeValue(null);
      this._onChange(null);
      this._onTouched();
      this.modelChange.emit(this._value);
    }
  }

  stopPropagation(event: MouseEvent): void {
    event.stopPropagation();
  }

  menuOpenedHandle(): void {
    this.updateSelectMenuProps();
    this.checkSearchSubscription();
  }

  menuClosedHandle(): void {
    this.resetSearchSettings();
  }

  updateSelectMenuProps(): void {
    this.selectWidth = this._selectEl.nativeElement.offsetWidth;
  }

  private checkSearchSubscription(): void {
    if (this.searchable && this._searchInputSubscription.closed) {
      const searchInput: HTMLElement | null = document.getElementById(`${this.selectId}${this.searchInputSuffix}`);

      if (searchInput) {
        this._searchInputSubscription = fromEvent(searchInput, 'input')
          .pipe(
            map((e: Event) => (e.target as HTMLInputElement).value),
            debounceTime(500),
            distinctUntilChanged()
          )
          .subscribe((value: string) => this.onSearchOptions.emit(value || ''));
      }
    }
  }

  private resetSearchSettings(): void {
    const searchInput: HTMLElement | null = document.getElementById(`${this.selectId}${this.searchInputSuffix}`);

    if (searchInput) {
      (searchInput as HTMLInputElement).value = '';
      this.onSearchOptions.emit('');
    }
  }
}
