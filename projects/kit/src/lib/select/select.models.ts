import { TemplateRef } from '@angular/core';

export type HseSelectOption<T> = {
  selected?: boolean;
  disabled?: boolean;
  label: TemplateRef<void> | string;
  value: T;
};
