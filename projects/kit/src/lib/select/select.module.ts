import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatMenuModule } from '@angular/material/menu';
import { HseCommonUiModule } from '../@common';
import { HseCheckboxModule } from '../checkbox';
import { HseIconModule } from '../icon';
import { HseTagsModule } from '../tags';

import { HseSelectComponent } from './components/select/select.component';
import { HseMultiSelectComponent } from './components/multi-select/multi-select.component';

const declarations: any[] = [HseMultiSelectComponent, HseSelectComponent];

@NgModule({
  declarations,
  imports: [
    CommonModule,
    HseCommonUiModule,
    FormsModule,
    MatMenuModule,
    HseCheckboxModule,
    HseIconModule,
    HseTagsModule,
    ReactiveFormsModule,
  ],
  exports: [...declarations],
})
export class HseSelectModule {}
