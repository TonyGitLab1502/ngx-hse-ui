import {ChangeDetectionStrategy, Component, Input, OnInit, ViewEncapsulation } from "@angular/core";
import {HseTableColumnConfig} from "../../table.models";
import {HseRadioGroup} from "../../../radio-group";
import {HseDirectionTypes} from "../../../@common";

@Component({
  selector: 'hse-table-column-settings',
  template: `
    <section class="hsth__column__settings" [class.is-open]="isSettingsOpen">
      <hse-icon (menuOpened)="menuOpenedHandle()" (menuClosed)="menuClosedHandle()"
                [matMenuTriggerFor]="optionList" name="dots"></hse-icon>
      <!--SETTINGS MENU-->
      <mat-menu class="hsth__column__settings__menu-override"
                #optionList="matMenu" [overlapTrigger]="false">
        <section class="hse-table-column-settings">
          <div *ngIf="column.settings?.inputField" (click)="$event.stopPropagation()">
            <!--TEXT INPUT-->
            <hse-text-input class="hse-table-column-settings__option"
                            [placeholder]="column.settings?.inputField?.placeholder || ''"
                            [(model)]="textInputFieldValue"
                            (onEnter)="column.settings?.inputField?.onEnter(textInputFieldValue)"
                            *ngIf="column.settings?.inputField?.type === 'string'">
            </hse-text-input>
            <!--/TEXT INPUT-->

            <!--NUMBER INPUT-->
            <hse-number-input class="hse-table-column-settings__option"
                              [placeholder]="column.settings?.inputField?.placeholder || ''"
                              [(model)]="numberInputFieldValue"
                              (onEnter)="column.settings?.inputField?.onEnter(numberInputFieldValue)"
                              *ngIf="column.settings?.inputField?.type === 'number'">
            </hse-number-input>
            <!--NUMBER INPUT-->
          </div>

          <!--ORDER-->
          <hse-radio-group class="hse-table-column-settings__option"
                           *ngIf="column.settings?.onOrder"
                           [direction]="HseDirectionTypes.COLUMN"
                           [collection]="orderDirections"
                           [model]="orderDirection"
                           (modelChange)="orderColumn($event)"
          ></hse-radio-group>
          <!--/ORDER-->

          <!-- FILTERS -->
          <div class="hse-table-column-settings__option" *ngIf="column.settings?.filter"
               (click)="$event.stopPropagation()">
            <div mat-menu-item [matMenuTriggerFor]="filtersList">
              <div>Быстрый фильтр</div>
              <hse-icon name="chevronRight"></hse-icon>
            </div>
          </div>
          <!-- /FILTERS -->

        </section>
      </mat-menu>
      <!--/SETTINGS MENU-->
    </section>

    <!-- FILTERS CONTENT -->
    <mat-menu class="hsth__column__settings__menu-override" #filtersList tabindex="-1" [overlapTrigger]="false">
      <section class="hsth__column__settings__filters" (click)="$event.stopPropagation()"
               [style.width.px]="filter?.width">
        <div *ngIf="filter?.type === 'select'">
          <hse-select
            (modelChange)="filter.onFilter(column, $event)"
            [(model)]="filter.value"
            [options]="filter.collection || []"
            [placeholder]="filter.placeholder || ''">
          </hse-select>
        </div>
        <div *ngIf="filter?.type === 'combo-box'">
          <!-- TODO combo-box-->
        </div>
        <div *ngIf="filter?.type === 'radio'">
          <hse-radio-group [direction]="HseDirectionTypes.COLUMN"
                           [collection]="filter.collection || []"
                           [model]="filter.value"
                           (modelChange)="filter.onFilter(column, $event)"
          ></hse-radio-group>
        </div>
        <div *ngIf="filter?.type === 'template'">
          <ng-template *ngTemplateOutlet="filter.template || null; context: {$implicit: {filter: filter} }"></ng-template>
        </div>
      </section>
    </mat-menu>
    <!-- /FILTERS CONTENT -->
  `,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TableColumnSettingsComponent implements OnInit {

  protected readonly HseDirectionTypes = HseDirectionTypes;

  @Input() column!: HseTableColumnConfig<any>;

  isSettingsOpen = false;
  textInputFieldValue: string = '';
  numberInputFieldValue: number | null = null;

  orderDirection: string = '';

  orderDirections: HseRadioGroup<string> = [
    { label: 'По убыванию', value: 'asc', disabled: false },
    { label: 'По возрастанию', value: 'desc', disabled: false },
  ];

  filter: any;

  ngOnInit() {
    this.filter = this.column.settings?.filter;
  }

  menuOpenedHandle() {
    this.isSettingsOpen = true;
  }

  menuClosedHandle() {
    this.isSettingsOpen = false;
  }

  orderColumn($event: string) {
    if (this.column.settings?.onOrder !== undefined) {
      this.column.settings.onOrder(this.column, $event as 'asc' | 'desc' | null);
    }
  }
}
