import {
  ChangeDetectionStrategy, Component,
  QueryList,
  Input, OnInit, ViewChildren, ViewContainerRef, ViewEncapsulation, ElementRef
} from '@angular/core';
import {HseTableColumnConfig, HseTableStickyHeaderConfig} from "../../table.models";
import {HseTableService} from "../../table.service";

@Component({
  selector: 'hse-table-header',
  template: `
    <section class="hsth"
             [class.grid]="nested"
             [style.grid-template-columns]="nested ? gridTemplateColumns: ''">
      <div class="hsth__column" *ngFor="let column of columns" [class.has-nested-child]="column.childColumns">

        <!--CHECKBOX-->
        <hse-checkbox *ngIf="column.enableCheck" class="hsth__column__checkbox"
                      (modelChange)="columnChecked($event, column)"></hse-checkbox>
        <!--/CHECKBOX-->

        <!--КОНТЕНТ-->
        <span class="hsth__column__value">{{column.title}}</span>
        <!--/КОНТЕНТ-->

        <!--SETTINGS-->
        <div *ngIf="column.settings !== undefined">
          <hse-table-column-settings [column]="column"></hse-table-column-settings>
        </div>
        <!--/SETTINGS-->

        <!--ДОЧЕРНИЕ КОЛОНКИ -->
        <!--<ng-template #columnContainer></ng-template>-->
        <hse-table-header [nested]="true"
                          *ngIf="column.childColumns"
                          [columns]="column.childColumns"></hse-table-header>
        <!--/ДОЧЕРНИЕ КОЛОНКИ -->
      </div>
    </section>
  `,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TableHeaderComponent implements OnInit {

  @Input() columns: HseTableColumnConfig<any>[] = [];
  @Input() nested?: boolean = false;
  @Input() sticky?: HseTableStickyHeaderConfig | true | undefined

  @ViewChildren('columnContainer', { read: ViewContainerRef })
  columnContainers: QueryList<ViewContainerRef> | undefined;

  gridTemplateColumns: string = '';

  constructor(public base: HseTableService, public elementRef: ElementRef) { }

  ngOnInit() {
    this.gridTemplateColumns = this.base.setGrid(this.columns);
  }

  columnChecked($event: boolean, column: HseTableColumnConfig<any>) {
    if(column.onColumnChecked) {
      column.onColumnChecked($event);
    } else {
      throw new Error(`Callback onColumnChecked for column ${column.id} is not defined`)
    }
  }
}
