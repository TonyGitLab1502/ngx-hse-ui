import {ChangeDetectionStrategy, Component, ElementRef, Input, OnInit, QueryList, ViewChildren,
  ViewContainerRef, ViewEncapsulation } from '@angular/core';
import {HseTableColumnConfig} from "../../table.models";
import {HseTableService} from "../../table.service";

@Component({
  selector: 'hse-table-row',
  template: `
  <section class="hstr"
           [class.grid]="nested"
           [style.grid-template-columns]="nested ? gridTemplateColumns: ''">
    <div class="hstr__cell" [class.has-nested-child]="column.childColumns" *ngFor="let column of columns">

      <!-- CONTENT -->
      <span *ngIf="!column.childColumns && !column.template; else childCells" class="hstr__cell__value">{{row[column.id]}}</span>

      <ng-template *ngTemplateOutlet="column.template || null; context: {$implicit: {cell: row[column.id], row: row }}"></ng-template>
      <!-- /CONTENT -->

      <!-- CHILD COLUMNS -->
      <ng-template #childCells>
        <hse-table-row *ngIf="column.childColumns" [nested]="true" [row]="row[column.id]" [columns]="column.childColumns || []"></hse-table-row>
      </ng-template>
      <!-- CHILD COLUMNS -->
    </div>
  </section>
  `,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TableRowComponent implements OnInit {

  @Input() row: any;
  @Input() columns: HseTableColumnConfig<any>[] = [];
  @Input() nested?: boolean = false;

  @ViewChildren('columnContainer', { read: ViewContainerRef })
  columnContainers!: QueryList<ViewContainerRef>;

  gridTemplateColumns: string = '';

  constructor(private base: HseTableService, public elementRef: ElementRef) { }

  ngOnInit() {
    if (this.nested) {
      this.gridTemplateColumns = this.base.setGrid(this.columns);
    }
  }
}
