import {AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, Input, OnDestroy, OnInit,
  QueryList, ViewChild, ViewChildren, ViewEncapsulation} from '@angular/core';
import {HseTableColumnConfig, HseTableColumnWidthConfig, HseTableConfig} from "../../table.models";
import {TableHeaderComponent} from "../table-header/table-header.component";
import {TableRowComponent} from "../table-row/table-row.component";
import {HseTableService} from "../../table.service";
import {UtilsService} from "../../../@utils";
import {debounceTime, fromEvent, Subject, Subscription } from 'rxjs';

@Component({
  selector: 'hse-table',
  templateUrl: 'hse-table.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HseTableComponent implements OnInit, AfterViewInit, OnDestroy {

  @Input() config!: HseTableConfig<any>;
  @Input() rowsData: any[] = [];
  @ViewChild('table', {static: true})
  public readonly table!: ElementRef<HTMLElement>;

  gridTemplateColumns: string = '';
  fixedColumns: HseTableColumnConfig<any>[] = [];
  fixedColumnsWidth = 0;
  gridTemplateFixedColumns: string = '';

  @ViewChildren(TableRowComponent)
  rowsElements!: QueryList<TableRowComponent>;
  @ViewChildren(TableHeaderComponent)
  headerElements!: QueryList<TableHeaderComponent>;

  gridRowsHeader: string = '';
  gridRowsBody: string = '';

  shiftIndex = 0;
  scrollLeft = 0;
  columnElements!: NodeListOf<Element>;

  private resizeSubscription!: Subscription;
  private scrollendSubscription!: Subscription;

  constructor(public base: HseTableService, private utils: UtilsService, private cdr: ChangeDetectorRef) { }

  get isOverflow() {
    const {nativeElement} = this.table;
    return nativeElement.scrollWidth > nativeElement.clientWidth;
  }

  get shiftRightAvailable() {
    const {nativeElement} = this.table;
    return (nativeElement.clientWidth + nativeElement.scrollLeft) < nativeElement.scrollWidth;
  }

  ngOnInit() {
    this.configureTable();
  }

  configureTable() {
    if (this.table && this.config) {
      // достаем фиксированные колонки
      this.fixedColumns = this.config.columns.filter((column)=> {
        return column.fixed;
      });

      if (this.fixedColumns.length > 0) {
        // вычисляем ширину фиксированного контента по сумме фиксированных колонок
        const fixedColumnWidths = this.fixedColumns.map((column) => {
          if (this.utils.isObject(column.width)) {
            const widthConfig = column.width as HseTableColumnWidthConfig;
            return widthConfig.value as number;
          } else if(this.utils.isNumber(column.width)) {
            return column.width as number;
          }

          throw new Error('Incorrect width value for fixed column')
        })
        this.fixedColumnsWidth = this.utils.sum(fixedColumnWidths);

        // рассчитываем css grid для фиксированных колонок
        this.gridTemplateFixedColumns = this.base.setGrid(this.fixedColumns);
      }

      // рассчитываем css grid для всех колонок
      this.gridTemplateColumns = this.base.setGrid(this.config.columns);
    }
  }

  recalculateGridRows() {
    this.gridRowsBody = '';
    this.gridRowsHeader = '';
    this.cdr.detectChanges();
    setTimeout(()=> {
      this.gridRowsBody = this.rowsData.map((row)=> {
        return row.$targetElements
          ?  Math.max(...row.$targetElements.map((element: any) => element.getBoundingClientRect().height)) + 'px'
          : '';
      }).join(' ')

      this.gridRowsHeader = Math.max(...this.headerElements.map((item, ix)=> {
        const element = item.elementRef.nativeElement.querySelector('.hsth__column');

        return element.getBoundingClientRect().height
      })) + 'px';

      this.cdr.detectChanges();
    });
  }

  ngAfterViewInit() {
    this.columnElements = this.table.nativeElement.querySelectorAll('.hsth:not(.grid) > .hsth__column');

    this.rowsElements.forEach(item=> {
      const element = item.elementRef.nativeElement.querySelector('.hstr__cell');
      if (!item.row.$targetElements) {
        item.row.$targetElements = [element]
      } else {
        item.row.$targetElements.push(element)
      }
    });

    this.recalculateGridRows();


    const $resize = new Subject<null>();
    this.resizeSubscription = $resize.pipe(debounceTime(200)).subscribe(()=> {
      this.recalculateGridRows();
      if (!this.isOverflow) {
        this.shiftIndex = 0;
        this.scrollLeft = 0;
      }
    })

    const tableResizeObserver = new ResizeObserver(() => {
      $resize.next(null);
    });
    tableResizeObserver.observe(this.table.nativeElement);


    this.scrollendSubscription = fromEvent(this.table.nativeElement, 'scrollend')
      .subscribe(() => {
        this.cdr.detectChanges();
      });
  }

  ngOnDestroy() {
    this.resizeSubscription?.unsubscribe();
    this.scrollendSubscription?.unsubscribe();
  }

  shiftLeft() {
    this.shiftIndex -= 1;
    this.shiftTable(-1);
  }

  shiftRight() {
    this.shiftTable(1);
    this.shiftIndex += 1;
  }

  shiftTable(direction: 1 | -1) {
    const shiftColumn = this.columnElements[this.shiftIndex + this.fixedColumns.length];
    const {nativeElement} = this.table;

    this.scrollLeft = this.scrollLeft + (shiftColumn.clientWidth * direction);
    nativeElement.scrollTo({left: this.scrollLeft, behavior: 'smooth'});
  }
}
