import {AfterViewInit, Directive, ElementRef, Input } from "@angular/core";

@Directive({
  selector: '[hseTableSticky]'
})
export class HseTableStickyDirective implements AfterViewInit {

  @Input() stickyEnable: boolean = false;
  @Input() stickyTarget!: Element | null | undefined;

  constructor(private elementRef: ElementRef<HTMLElement>) { }

  ngAfterViewInit() {
    if(!this.stickyEnable) {
      return
    }

    this.initStickyTop();
  }

  get targetOffsetTop() {
    return this.stickyTarget ? this.stickyTarget.getBoundingClientRect().top : 0;
  }

  initStickyTop() {
    const stickyElement = this.elementRef.nativeElement;
    const scrollableContainer = this.stickyTarget || window;
    const anchor = this.createAnchorElement(stickyElement.parentElement || document.body, stickyElement);
    let offsetTop = stickyElement.getBoundingClientRect().top - this.targetOffsetTop;

    scrollableContainer.addEventListener('scroll', (ev) => {
      offsetTop = anchor.getBoundingClientRect().top - this.targetOffsetTop;

      if (offsetTop < 0) {
        stickyElement.style.top = `${-offsetTop}px`;
        stickyElement.style.zIndex = '99';
      } else {
        stickyElement.style.top = '0px';
        stickyElement.style.zIndex = '0';
      }
    });
  }

  createAnchorElement(parent: HTMLElement, stickyElement: HTMLElement) {
    const anchorElement = document.createElement('div');
    anchorElement.classList.add('hse-sticky-anchor')
    parent.insertBefore(anchorElement, stickyElement);

    return anchorElement;
  }
}
