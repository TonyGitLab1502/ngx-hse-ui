import { TemplateRef } from "@angular/core"
import {HseSelectOption} from "../select";

export interface HseTableConfig<ColumnIds> {
  name: string,
  columns: HseTableColumnConfig<ColumnIds>[],
  stickyHeader?: HseTableStickyHeaderConfig // конфиг липкой шапки таблицы
  shiftButtons?: boolean  // флаг для скрытия горизонтального скролл-бара и отображения кнопок слайда колонок по горизонтали
}

export interface HseTableStickyHeaderConfig {
  scrollableParent?: Element  // window
}

/**
 * Конфиг колонки таблицы
 * */
export interface HseTableColumnConfig<ColumnIds> {
  id: ColumnIds
  title: string
  fixed?: boolean // фиксированная колонка
  template?: TemplateRef<any> // шаблон
  width?: HseTableColumnWidthConfig | HseTableColumnWidthValue // конфиг ширины или значение ширины
  childColumns?: HseTableColumnConfig<ColumnIds>[] // дочерние колонки
  enableCheck?: boolean,
  onColumnChecked?: (value: boolean) => void,
  settings?: {
    inputField?: {
      type: 'string' | 'number',
      placeholder: string,
      onEnter: (value: string | number | null) => void
    },
    filter?: {
      type: 'combo-box' | 'select' | 'radio' | 'template',
      value?: any,
      width?: number,
      placeholder?: string,
      collection: HseSelectOption<any>[],
      onFilter: (column: HseTableColumnConfig<ColumnIds>, value: any) => void,
      template?: TemplateRef<any>,
    },
    onOrder?: (column: HseTableColumnConfig<ColumnIds>, direction: 'asc' | 'desc' | null) => void
  }
}

/**
 * Конфиг ширины колонки
 * */
export interface HseTableColumnWidthConfig {
  value?: HseTableColumnWidthValue, // значение ширины
  fixed?: boolean // фиксированная ширина
}

/**
 * Значение ширины колонки
 * 1) number - ширина в px
 * 2) string - строковое css-значение (px, fr, %, ...)
 * 3) [number | string, number | string] - [min, max]
 * */
export type HseTableColumnWidthValue = number | string | [number | string, number | string];

