import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HseTableComponent } from './components/table/hse-table.component';
import { TableRowComponent } from './components/table-row/table-row.component';
import { TableHeaderComponent } from './components/table-header/table-header.component';
import { FormsModule } from '@angular/forms';
import {HseTableService} from "./table.service";
import {HseTableStickyDirective} from "./hse-table-sticky.directive";
import {HseCheckboxModule} from "../checkbox";
import {HseIconModule} from "../icon";
import { MatMenuModule } from '@angular/material/menu';
import {TableColumnSettingsComponent} from "./components/table-column-settings/table-column-settings.component";
import {HseInputModule} from "../input";
import {HseRadioGroupModule} from "../radio-group";
import {HseSelectModule} from "../select";

@NgModule({
  declarations: [
    HseTableComponent,
    TableRowComponent,
    TableHeaderComponent,
    HseTableStickyDirective,
    TableColumnSettingsComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    HseCheckboxModule,
    HseIconModule,
    MatMenuModule,
    HseInputModule,
    HseRadioGroupModule,
    HseSelectModule
  ],
  exports: [HseTableComponent],
  entryComponents: [TableHeaderComponent],
  providers: [HseTableService]
})
export class HseTableModule {}
