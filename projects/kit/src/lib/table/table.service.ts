import {Injectable} from "@angular/core";
import {HseTableColumnConfig, HseTableColumnWidthConfig, HseTableColumnWidthValue} from "./table.models";
import {UtilsService} from "../@utils";

@Injectable()
export class HseTableService {
  // tableHeaders: TableHeaderComponent[] = [];
  // tableRows: TableRowComponent[] = [];
  // tableElement: Element | undefined;

  constructor(private utils: UtilsService) {}

  setGrid(columns: HseTableColumnConfig<any>[]) {
    if (columns) {
      return columns.map((columnConfig) => {
          const {width, childColumns} = columnConfig;
          let widthValue: HseTableColumnWidthValue | undefined;
          let isWidthFixed: boolean | undefined = columnConfig.fixed; // ширина фиксированных колонок всегда фиксирована

          if (this.utils.isObject(width)) {
            const widthConfig = width as HseTableColumnWidthConfig;
            widthValue = widthConfig.value;
            isWidthFixed = widthConfig.fixed;
          } else {
            widthValue = width as HseTableColumnWidthValue;
          }

          if (this.utils.isArray(widthValue)) {
            const result = (widthValue as Array<any>).map(value => {
              return typeof value === 'number' ? `${value}px` : value
            });
            return `minmax(${result[0]}, ${result[1]})`;
          } else if (typeof widthValue === 'number') {
            return `minmax(${widthValue}px, ${isWidthFixed ? `${widthValue}px` : '1fr'})`;
          } else if (typeof widthValue === 'string') {
            return widthValue;
          }

          return `1fr`
        }).join(' ') || '';
    } else {
      throw new Error('HseTableAdaptivityPlugin config error');
    }
  }
}
