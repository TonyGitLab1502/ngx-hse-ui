import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HseTabsComponent } from './tabs.component';

describe('HseTabsComponent', () => {
  let component: HseTabsComponent;
  let fixture: ComponentFixture<HseTabsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HseTabsComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(HseTabsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
