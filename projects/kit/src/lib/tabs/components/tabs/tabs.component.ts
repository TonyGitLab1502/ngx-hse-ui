import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  HostBinding,
  Input,
  OnInit,
  Output,
  ViewEncapsulation,
} from '@angular/core';
import { HseTabItem, HseTabsVariants } from '../../tabs.models';
import { HseUiSizes } from '../../../@common';
import { UtilsService } from '../../../@utils';

@Component({
  selector: 'hse-tabs',
  template: `
    <ng-container *ngFor="let tab of tabs">
      <button
        class="hse-tab-item"
        [class.hse-tab-item_active]="tab.selected"
        [class.hse-tab-item_invalid]="tab.hasError"
        [disabled]="tab.disabled"
        (click)="onSelectTab(tab)"
      >
        <span class="hse-tab-item__label">
          <ng-container hseLabelTemplate [label]="tab.label"></ng-container>
        </span>
        <ng-container *ngIf="tab.counter">
          <span class="hse-tab-item__counter">{{ tab.counter }}</span>
        </ng-container>
      </button>
    </ng-container>
  `,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HseTabsComponent implements OnInit {
  @HostBinding('class.hse-tabs') private readonly baseCss: boolean = true;

  @Input() tabs: HseTabItem[] = [];
  @Input() variant: HseTabsVariants = HseTabsVariants.LINE;
  @Input() size: HseUiSizes = HseUiSizes.MEDIUM;

  @Output() onChangeTab: EventEmitter<HseTabItem> = new EventEmitter<HseTabItem>();

  @HostBinding('class') get additionalClasses(): string {
    return `
      hse-tabs_${this.variant}
      hse-tabs_${this._utilsService.getSizeClass(this.size)}
    `;
  }

  private _activeTab: HseTabItem | null = null;

  constructor(private readonly _utilsService: UtilsService) {}

  ngOnInit(): void {
    this._activeTab = this.tabs.find((tab: HseTabItem) => tab.selected) || null;
  }

  onSelectTab(tab: HseTabItem): void {
    if (tab.id === this._activeTab?.id) {
      return;
    }

    this._activeTab = tab;
    this.tabs.forEach((tabItem: HseTabItem) => (tabItem.selected = tabItem.id === tab.id));
    this.onChangeTab.emit(this._activeTab);
  }
}
