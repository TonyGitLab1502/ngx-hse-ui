import { TemplateRef } from '@angular/core';

export type HseTabItem = {
  id: number | string;
  label: string | TemplateRef<void>;
  selected: boolean;
  disabled?: boolean;
  hasError?: boolean;
  counter?: number;
  optimizedMaxWidth?: number;
};

export enum HseTabsVariants {
  LINE = 'line',
  LINE_OUTLINED = 'line-outlined',
  LINE_FILLED = 'line-filled',
  SQUARE = 'square',
  ROUND = 'round',
}
