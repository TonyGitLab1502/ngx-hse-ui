import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HseTabsComponent } from './components/tabs/tabs.component';
import { HseCommonUiModule } from '../@common';
import { HseIconModule } from '../icon';

const declarations: any[] = [HseTabsComponent];

@NgModule({
  declarations,
  imports: [CommonModule, HseCommonUiModule, HseIconModule],
  exports: [...declarations],
})
export class HseTabsModule {}
