import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HseTagComponent } from './tag.component';

describe('HseTagComponent', () => {
  let component: HseTagComponent;
  let fixture: ComponentFixture<HseTagComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HseTagComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(HseTagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
