import {
  AfterContentInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  HostBinding,
  HostListener,
  Input,
  Output,
  Renderer2,
  ViewEncapsulation,
} from '@angular/core';
import { HseUiBaseVariants, HseUiSizes, HseUiVariants } from '../../../@common';
import { UtilsService } from '../../../@utils';

@Component({
  selector: 'hse-tag',
  template: `
    <ng-container *ngIf="leftIcon">
      <hse-icon class="hse-tag__left-icon" [name]="leftIcon"></hse-icon>
    </ng-container>
    <div class="hse-tag__label">
      <ng-content></ng-content>
    </div>
    <ng-container *ngIf="rightIcon">
      <hse-icon class="hse-tag__right-icon" [name]="rightIcon"></hse-icon>
    </ng-container>
  `,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HseTagComponent implements AfterContentInit {
  @HostBinding('class.hse-tag') private readonly baseCss: boolean = true;

  @Input() clickable: boolean = false;
  @Input() disabled: boolean = false;
  @Input() leftIcon: string = '';
  @Input() maxWidth: number = 120;
  @Input() rightIcon: string = '';
  @Input() selected: boolean = false;
  @Input() size: HseUiSizes = HseUiSizes.MEDIUM;
  @Input() variant: HseUiBaseVariants = HseUiVariants.PRIMARY;

  @Output() onClick: EventEmitter<void> = new EventEmitter<void>();

  @HostBinding('class') get additionalClasses(): string {
    return `
      hse-tag_${this._utilsService.getSizeClass(this.size)}
      hse-tag_${this.variant}
      ${this.selected ? 'selected' : ''}
      ${this.disabled ? 'hse-tag_disabled' : ''}
      ${this.clickable ? 'hse-tag_clickable' : ''}
    `;
  }

  @HostListener('click', ['$event.target'])
  onClickHandle() {
    if (this.clickable && !this.disabled) {
      this.selected = !this.selected;
      this.onClick.emit();
    }
  }

  constructor(
    private readonly _elRef: ElementRef,
    private readonly _renderer: Renderer2,
    private readonly _utilsService: UtilsService
  ) {}

  ngAfterContentInit(): void {
    this.setTagMaxWidth();
  }

  private setTagMaxWidth(): void {
    this._renderer.setStyle(this._elRef.nativeElement, 'max-width', `${this.maxWidth}px`);
  }
}
