export type HseTags = Array<HseTag>;
export type HseTag = { disabled: boolean; label: string; leftIcon: string; rightIcon: string };
