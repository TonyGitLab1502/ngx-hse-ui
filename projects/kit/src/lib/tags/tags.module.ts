import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HseTagComponent } from './components/tag/tag.component';
import { HseIconModule } from '../icon';

const declarations: any[] = [HseTagComponent];

@NgModule({
  declarations,
  imports: [CommonModule, HseIconModule],
  exports: [...declarations],
})
export class HseTagsModule {}
