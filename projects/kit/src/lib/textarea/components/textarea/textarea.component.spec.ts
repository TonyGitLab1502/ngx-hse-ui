import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HseTextareaComponent } from './textarea.component';

describe('HseTextareaComponent', () => {
  let component: HseTextareaComponent;
  let fixture: ComponentFixture<HseTextareaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HseTextareaComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(HseTextareaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
