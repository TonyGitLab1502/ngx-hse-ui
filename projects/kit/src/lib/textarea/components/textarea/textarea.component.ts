import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  HostBinding,
  Input,
  OnInit,
  Output,
  Renderer2,
  TemplateRef,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { UtilsService } from 'projects/kit/src/lib/@utils';

@Component({
  selector: 'hse-textarea',
  template: `
    <ng-container *ngIf="label">
      <label class="hse-textarea__label" for="{{ textareaId }}">
        <ng-container hseLabelTemplate [label]="label"></ng-container>
      </label>
    </ng-container>
    <div class="hse-textarea__textarea-wrapper">
      <textarea
        #textarea
        hseTextareaAutosize
        class="hse-textarea__textarea"
        [class.hse-textarea__textarea_with-counter]="maxLength"
        (blur)="onBlur()"
        [disabled]="disabled"
        [id]="textareaId"
        [enableAutosize]="autosize"
        [minRows]="minRows"
        [maxRows]="maxRows"
        [ngModel]="model"
        (ngModelChange)="onChange($event)"
        [placeholder]="placeholder"
      ></textarea>
      <ng-container *ngIf="maxLength">
        <div class="hse-textarea__characters">{{ textarea.value.length }} / {{ maxLength }}</div>
      </ng-container>
    </div>
  `,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => HseTextareaComponent),
      multi: true,
    },
  ],
})
export class HseTextareaComponent implements ControlValueAccessor, OnInit {
  @HostBinding('class.hse-textarea') private readonly baseCss: boolean = true;

  @Input() autosize: boolean = false;
  @Input() disabled: boolean = false;
  @Input() hasError: boolean = false;
  @Input() label: string | TemplateRef<void> = '';
  @Input()
  set model(value: string) {
    this._value = value;
    this._onChange(value);
  }
  @Input() maxLength: number | null = null;
  @Input() maxRows: number = 5;
  @Input() minRows: number = 2;
  @Input() placeholder: string = '';

  @Output() modelChange: EventEmitter<string> = new EventEmitter<string>();
  @ViewChild('textarea', { static: true })
  private _textarea: ElementRef<HTMLTextAreaElement> = new ElementRef<HTMLTextAreaElement>(this._elRef.nativeElement);

  @HostBinding('class') get additionalClasses(): string {
    return `${this.hasError ? 'hse-textarea_invalid' : ''}`;
  }

  get model(): string {
    return this._value;
  }

  private _value: string = '';
  private _onChange: (value: string) => void = () => {};
  private _onTouched: () => void = () => {};

  public readonly textareaId: string = '';

  constructor(
    private readonly _elRef: ElementRef,
    private readonly _renderer: Renderer2,
    private readonly _utilsService: UtilsService
  ) {
    this.textareaId = this._utilsService.generateUniqId();
  }

  ngOnInit(): void {
    if (this.maxLength) {
      this._renderer.setAttribute(this._textarea.nativeElement, 'maxlength', this.maxLength.toString());
    }
  }

  writeValue(value: string): void {
    this._value = value;
  }

  registerOnChange(fn: any): void {
    this._onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this._onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  onChange(value: string): void {
    this.writeValue(value);
    this._onChange(this._value);
    this._onTouched();

    this.modelChange.emit(this._value);
  }

  onBlur(): void {
    this._onTouched();
  }
}
