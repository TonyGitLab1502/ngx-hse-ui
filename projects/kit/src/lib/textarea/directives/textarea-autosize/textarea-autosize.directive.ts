import { Directive, ElementRef, HostListener, Input, OnInit } from '@angular/core';

@Directive({
  selector: '[hseTextareaAutosize]',
})
export class HseTextareaAutosizeDirective implements OnInit {
  @Input() enableAutosize: boolean = false;
  @Input() minRows: number = 2;
  @Input() maxRows: number = 5;

  private readonly _avgLineHeight: number = 20;

  constructor(private readonly _elRef: ElementRef<HTMLTextAreaElement>) {}

  @HostListener('input')
  onInput(): void {
    this.resize();
  }

  ngOnInit(): void {
    if (this._elRef.nativeElement.scrollHeight) {
      setTimeout(() => this.resize(), 0);
    }
  }

  private resize(): void {
    if (!this.enableAutosize) {
      return;
    }

    this._elRef.nativeElement.rows = this.minRows;

    const rows: number = Math.floor(
      (this._elRef.nativeElement.scrollHeight - this._elRef.nativeElement.clientHeight) / this._avgLineHeight
    );
    const rowsCount: number = this.minRows + rows;

    this._elRef.nativeElement.rows = rowsCount > this.maxRows ? this.maxRows : rowsCount;
  }
}
