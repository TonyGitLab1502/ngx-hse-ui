export * from './textarea.module';
export * from './components/textarea/textarea.component';
export * from './directives/textarea-autosize/textarea-autosize.directive';
