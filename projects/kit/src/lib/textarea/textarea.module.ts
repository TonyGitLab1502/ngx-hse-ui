import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HseCommonUiModule } from '../@common';
import { HseTextareaAutosizeDirective } from './directives/textarea-autosize/textarea-autosize.directive';
import { HseTextareaComponent } from './components/textarea/textarea.component';

const declarations: any[] = [HseTextareaAutosizeDirective, HseTextareaComponent];

@NgModule({
  declarations,
  imports: [CommonModule, FormsModule, HseCommonUiModule, ReactiveFormsModule],
  exports: [...declarations],
})
export class HseTextareaModule {}
