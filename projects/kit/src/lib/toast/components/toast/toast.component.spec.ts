import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HseToastComponent } from './toast.component';

describe('HseToastComponent', () => {
  let component: HseToastComponent;
  let fixture: ComponentFixture<HseToastComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HseToastComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(HseToastComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
