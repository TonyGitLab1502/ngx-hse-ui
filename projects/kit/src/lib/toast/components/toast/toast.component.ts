import { animate, style, transition, trigger } from '@angular/animations';
import { ChangeDetectionStrategy, Component, HostBinding, ViewEncapsulation } from '@angular/core';
import { HseToastItem, HseToastService } from '../../toast.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'hse-toast',
  template: `
    <ng-container *ngIf="toast$ | async as toast">
      <div class="hse-toast__content-wrapper" [@slideInOut]>
        <div class="hse-toast__icon-wrapper">
          <hse-icon class="hse-toast__icon" [name]="toast.iconName"></hse-icon>
        </div>
        <div
          class="hse-toast__content"
          [ngClass]="toast.description ? 'hse-toast__content_column' : 'hse-toast__content_row'"
        >
          <div class="hse-toast__title">{{ toast.title }}</div>
          <div class="hse-toast__description">
            <ng-container hseLabelTemplate [label]="toast.description"></ng-container>
          </div>
          <div class="hse-toast__actions">
            <button class="hse-toast__action" (click)="onApprove(toast)">{{ toast.approveButtonText }}</button>
            <button class="hse-toast__action" (click)="onReject()">{{ toast.rejectButtonText }}</button>
          </div>
        </div>
        <div class="hse-toast__icon-wrapper">
          <hse-icon class="hse-toast__close-icon" name="clear" (click)="onReject()"></hse-icon>
        </div>
      </div>
    </ng-container>
  `,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('slideInOut', [
      transition(':enter', [style({ bottom: '-100%' }), animate(`700ms ease-in`, style({ bottom: 0 }))]),
      transition(':leave', [animate(`500ms ease-out`, style({ bottom: '-100%' }))]),
    ]),
  ],
})
export class HseToastComponent {
  @HostBinding('class.hse-toast') private readonly baseCss: boolean = true;
  @HostBinding('class') get additionalClasses(): string {
    return `${this._toastService.toast$.getValue() ? 'hse-toast_showing' : ''}`;
  }

  get toast$(): Observable<HseToastItem | null> {
    return this._toastService.toast$;
  }

  constructor(private readonly _toastService: HseToastService) {}

  onApprove(toast: HseToastItem): void {
    if (toast.approveCallback) {
      toast.approveCallback();
    }

    this.onReject();
  }

  onReject(): void {
    this._toastService.removeToast();
  }
}
