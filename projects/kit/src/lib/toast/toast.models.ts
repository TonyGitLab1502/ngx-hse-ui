import { TemplateRef } from '@angular/core';

export type HseToastItemProps = {
  title: string;
  description?: string | TemplateRef<void>;
  iconName?: string;
  approveCallback?: () => void;
  approveButtonText?: string;
  rejectButtonText?: string;
};
