import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HseToastComponent } from './components/toast/toast.component';
import { HseToastService } from './toast.service';
import { HseCommonUiModule } from '../@common';
import { HseIconModule } from '../icon';

const declarations: any[] = [HseToastComponent];

@NgModule({
  declarations,
  imports: [CommonModule, HseCommonUiModule, HseIconModule],
  providers: [HseToastService],
  exports: [...declarations],
})
export class HseToastModule {}
