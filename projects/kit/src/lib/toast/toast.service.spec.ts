import { TestBed } from '@angular/core/testing';

import { HseToastService } from './toast.service';

describe('HseToastService', () => {
  let service: HseToastService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HseToastService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
