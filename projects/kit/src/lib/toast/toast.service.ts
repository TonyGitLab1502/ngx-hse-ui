import { Injectable, TemplateRef } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { HseToastItemProps } from './toast.models';

export class HseToastItem {
  constructor(
    public title: string,
    public description: string | TemplateRef<void>,
    public iconName: string,
    public approveButtonText: string,
    public approveCallback: (() => void) | undefined,
    public rejectButtonText: string
  ) {
    if (!title) {
      throw new Error('Ошибка! Не передан обязательный параметр title');
    }

    this.title = title;
    this.description = description;
    this.iconName = iconName;
    this.approveButtonText = approveButtonText;
    this.approveCallback = approveCallback;
    this.rejectButtonText = rejectButtonText;
  }
}

@Injectable({
  providedIn: 'root',
})
export class HseToastService {
  public toast$: BehaviorSubject<HseToastItem | null> = new BehaviorSubject<HseToastItem | null>(null);

  addToast(toastProps: HseToastItemProps): void {
    const { title, description, iconName, approveButtonText, approveCallback, rejectButtonText } = toastProps;
    const toastItem = new HseToastItem(
      title,
      description || '',
      iconName || 'info',
      approveButtonText || 'Принять',
      approveCallback || undefined,
      rejectButtonText || 'Отклонить'
    );

    this.toast$.next(toastItem);
  }

  removeToast(): void {
    this.toast$.next(null);
  }
}
