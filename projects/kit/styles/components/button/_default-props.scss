$-button-default-props: (
  padding: (
    sm: 0.375rem 1.5rem,
    md: 0.625rem 1.5rem,
    lg: 0.875rem 1.5rem,
  ),
  sizes: (
    sm: 2rem,
    md: 2.5rem,
    lg: 3rem,
  ),
  variants: (
    primary: (
      bg: hse-color(brand, brand-1),
      bg-hover: hse-color(brand, dark),
      border: hse-color(brand, brand-1),
      border-hover: hse-color(brand, dark),
      color: hse-color(base, white),
    ),
    secondary: (
      bg: hse-color(base, white),
      bg-hover: hse-color(base, white),
      border: hse-color(gray, morn-1),
      border-hover: hse-color(brand, brand-1),
      color: hse-color(gray, dusk-1),
    ),
    tertiary: (
      bg: hse-color(base, white),
      bg-hover: hse-color(gray, dusk-5),
      border: hse-color(base, white),
      border-hover: hse-color(gray, dusk-5),
      color: hse-color(gray, dusk-1),
    ),
  ),
  inactive: (
    bg: hse-color(gray, dusk-5),
    bg-hover: hse-color(gray, dusk-5),
    border: hse-color(gray, dusk-5),
    border-hover: hse-color(gray, dusk-5),
    color: hse-color(gray, dusk-4),
  ),
);

$-button-group-default-props: (
  default: (
    bg: hse-color(base, white),
    bg-hover: hse-color(base, white),
    border: hse-color(gray, morn-1),
    border-hover: hse-color(brand, brand-1),
    color: hse-color(gray, dusk-1),
  ),
  active: (
    bg: hse-color(brand, brand-1),
    bg-hover: hse-color(brand, dark),
    border: hse-color(brand, brand-1),
    border-hover: hse-color(brand, dark),
    color: hse-color(base, white),
  ),
);

$-icon-button-default-props: (
  border-radius: (
    sm: 0.25rem,
    md: 0.5rem,
    lg: 0.75rem,
  ),
  padding: (
    sm: 0.125rem,
    md: 0.25rem,
    lg: 0.5rem,
  ),
  sizes: (
    sm: 2rem,
    md: 2.5rem,
    lg: 3rem,
  ),
  variants: (
    primary: (
      bg: hse-color(brand, brand-1),
      bg-hover: hse-color(brand, dark),
      border: hse-color(brand, brand-1),
      border-hover: hse-color(brand, dark),
      color: hse-color(base, white),
    ),
    secondary: (
      bg: hse-color(base, white),
      bg-hover: hse-color(base, white),
      border: hse-color(gray, morn-1),
      border-hover: hse-color(brand, brand-1),
      color: hse-color(gray, dusk-1),
    ),
    tertiary: (
      bg: hse-color(base, white),
      bg-hover: hse-color(gray, dusk-5),
      border: hse-color(base, white),
      border-hover: hse-color(gray, dusk-5),
      color: hse-color(gray, dusk-1),
    ),
  ),
  inactive: (
    bg: hse-color(gray, dusk-5),
    bg-hover: hse-color(gray, dusk-5),
    border: hse-color(gray, dusk-5),
    border-hover: hse-color(gray, dusk-5),
    color: hse-color(gray, dusk-4),
  ),
);
